'use strict';

module.exports = function(environment) {
  const ENV = {
    modulePrefix: 'help-button',
    environment,
    rootURL: '/',
    locationType: 'auto',
    namespace: '/api/v1',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false,
      },
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    // contentSecurityPolicy: {
    //   'default-src': "'none'",
    //   'script-src': "'self' 'unsafe-inline' 'unsafe-eval'",
    //   'font-src': "'self'",
    //   'connect-src': "'self'",
    //   'img-src': "'self'",
    //   'report-uri':"'localhost'",
    //   'style-src': "'self' 'unsafe-inline'",
    //   'frame-src': "'none'"
    // }
  };

  ENV['google-apis'] = {
    'key': 'AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM',
  };

  ENV['place-autocomplete'] = {
    // exclude: true,
    key: 'AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM',
    // client: 'gme-myclientid',
  };

  ENV['ember-cli-notifications'] =  {
    includeFontAwesome: true,
  };

  ENV['ember-simple-auth'] = {
    authenticationRoute: 'index',
    routeAfterAuthentication: 'index',
    routeIfAlreadyAuthenticated: 'index',
  };

  ENV.torii = {
    sessionServiceName: 'session',
    allowUnsafeRedirect: true,
    providers: {
      'facebook-oauth2': {
        apiKey: '2167841139913071',
        redirectUri: 'http://localhost:4200',
        tokenExchangeUri: '/api/v1/token',
      },
      'twitter': {
        requestTokenUri: '/api/v1/auth/twitter',
      },
      'google-oauth2': {
        apiKey: '400715726399-k9kfli1mmh5f4qlnquhk5mgel2gka74a.apps.googleusercontent.com',
        redirectUri: 'http://localhost:4200/oauth2callback',
        tokenExchangeUri: '/api/v1/token',
        // secretIdClient: 'K6dhKBZrr3nijfFM8U6WwNCm'
      },
      'instagram-oauth2': {
        apiKey: '048f2666744742afb86ace1011d7cf05',
        redirectUri: 'http://localhost:4200',
        tokenExchangeUri: '/api/v1/token',
      },
    },
  };

  if (environment === 'development') {
    ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;

    ENV['ember-cli-mirage'] = {
      enabled: false,
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    const domainUrl = process.deployEnv.deployTarget === 'staging' ?
      'help-buttons-staging.herokuapp.com' : '';

    ENV.torii.providers['facebook-oauth2'].redirectUri = `https://${domainUrl}`;
    ENV.torii.providers.twitter.redirectUri = `https://${domainUrl}/api/v1/auth/twitter`;
    ENV.torii.providers['google-oauth2'].redirectUri = `https://${domainUrl}/oauth2callback`;
    ENV.torii.providers['instagram-oauth2'].redirectUri = `https://${domainUrl}`;
  }

  return ENV;
};
