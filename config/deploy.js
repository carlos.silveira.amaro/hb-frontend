const VALID_DEPLOY_TARGETS = [ // update these to match what you call your deployment targets
  'staging',
  'production',
];

const SECRETS = require('./secrets');

module.exports = function(deployTarget) {
  const ENV = {
    'revision-data': {
      type: 'file-hash',
    },
    build: {},
    redis: {
      allowOverwrite: true,
      keyPrefix: 'help-buttons-app:index',
    },
    s3: {
      prefix: 'statics',
      filePattern: '**/*.{js,css,png,gif,ico,jpg,map,xml,txt,svg,swf,eot,ttf,woff,woff2,otf,json,pdf}',
    },
  };

  if (VALID_DEPLOY_TARGETS.indexOf(deployTarget) === -1) {
    throw new Error('Invalid deployTarget ' + deployTarget);
  }

  if (deployTarget === 'staging') {
    // configure other plugins for staging deploy target here
    /* protected secrets variables */
    ENV.redis.url = process.env.STAGING_REDIS_URL || SECRETS.staging.redis.url;

    ENV.s3.accessKeyId = process.env.STAGING_AWS_ACCESS_KEY_ID || SECRETS.staging.s3.accessKeyId;
    ENV.s3.secretAccessKey = process.env.STAGING_AWS_SECRET_ACCESS_KEY || SECRETS.staging.s3.secretAccessKey;
    ENV.s3.bucket = process.env.STAGING_S3_BUCKET || SECRETS.staging.s3.bucket;
    ENV.s3.region = process.env.STAGING_AWS_DEFAULT_REGION || SECRETS.staging.s3.region;

    /* variables hardcoded*/
    ENV.build.environment = process.env.NODE_ENV || 'production';
    ENV.enableLog = true;
    ENV.enableTracking = true;
  }

  if (deployTarget === 'production') {
    // configure other plugins for production deploy target here
    ENV['revision-data'].type = 'git-tag-commit';

    /* protected secrets variables */
    ENV.redis.url = process.env.PRODUCTION_REDIS_URL || SECRETS.production.redis.url;

    ENV.s3.accessKeyId = process.env.PRODUCTION_AWS_ACCESS_KEY_ID || SECRETS.production.s3.accessKeyId;
    ENV.s3.secretAccessKey = process.env.PRODUCTION_AWS_SSECRET_ACCESS_KEY || SECRETS.production.s3.secretAccessKey;
    ENV.s3.bucket = process.env.PRODUCTION_S3_BUCKET || SECRETS.production.s3.bucket;
    ENV.s3.region = process.env.PRODUCTION_AWS_DEFAULT_REGION || SECRETS.production.s3.region;

    /* variables hardcoded*/
    // ENV.localeStrategy = process.env.LOCALE_STRATEGY || 'domain';
    ENV.build.environment = process.env.NODE_ENV || 'production';
  }

  ENV.deployTarget = deployTarget;

  process.deployEnv = ENV;

  return ENV;

  /* Note: a synchronous return is shown above, but ember-cli-deploy
   * does support returning a promise, in case you need to get any of
   * your configuration asynchronously. e.g.
   *
   *    var Promise = require('ember-cli/lib/ext/promise');
   *    return new Promise(function(resolve, reject){
   *      var exec = require('child_process').exec;
   *      var command = 'heroku config:get REDISTOGO_URL --app my-app-' + deployTarget;
   *      exec(command, function (error, stdout, stderr) {
   *        ENV.redis.url = stdout.replace(/\n/, '').replace(/\/\/redistogo:/, '//:');
   *        if (error) {
   *          reject(error);
   *        } else {
   *          resolve(ENV);
   *        }
   *      });
   *    });
   *
   */
};
