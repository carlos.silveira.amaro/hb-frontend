import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | button-net/index/index/new-button/date', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:button-net/index/index/new-button/date');
    assert.ok(route);
  });
});
