import currentUser from 'help-button/utils/current-user';
import { module, test } from 'qunit';

module('Unit | Utility | current user', function() {
  // Replace this with your real tests.
  test('it is a function', function(assert) {
    assert.equal(typeof currentUser, 'function', 'it is a function');
  });
});
