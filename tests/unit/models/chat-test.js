import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  belongsToRelationship,
  hasManyRelationship,
} from 'help-button/tests/helpers/model-helpers';

import { run } from '@ember/runloop';

module('Unit | Model | chat', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    const model = run(() => this.owner.lookup('service:store').createRecord('chat'));
    // const store = this.store();
    assert.ok(!!model);
  });

  belongsToRelationship('chat', 'button');

  hasManyRelationship('chat', 'users', {
    type: 'user',
  });

  hasManyRelationship('chat', 'messages', {
    type: 'message',
  });
});
