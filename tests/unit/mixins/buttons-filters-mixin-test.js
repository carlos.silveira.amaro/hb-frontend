import EmberObject from '@ember/object';
import ButtonsFiltersMixinMixin from 'help-button/mixins/buttons-filters-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | buttons-filters-mixin', function() {
  // Replace this with your real tests.
  test('it works', function(assert) {
    const ButtonsFiltersMixinObject = EmberObject.extend(ButtonsFiltersMixinMixin);
    const subject = ButtonsFiltersMixinObject.create();
    assert.ok(subject);
  });
});
