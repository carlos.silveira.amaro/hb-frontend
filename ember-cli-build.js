'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

const stagingPrependURL = 'https://s3-eu-west-3.amazonaws.com/help-buttons-staging/statics/';
const productionPrependURL = '';

module.exports = function(defaults) {
  const fingerprintOptions = {
    extensions: ['js', 'css', 'png', 'jpg', 'jpeg', 'gif', 'svg', 'png', 'pdf', 'json'],
    replaceExtensions: ['html', 'js', 'css', 'json'],
    exclude: [
    ],
  };

  const deployTarget = process.env.DEPLOY_TARGET;
  process.env.PREPEND_URL = '/';

  switch (deployTarget) {
    case 'staging':
      fingerprintOptions.prepend = stagingPrependURL;
      fingerprintOptions.enabled = true;
      process.env.PREPEND_URL = stagingPrependURL;
      break;
    case 'production':
      fingerprintOptions.enabled = true;
      fingerprintOptions.prepend = productionPrependURL;
      process.env.PREPEND_URL = productionPrependURL;
      break;
    default:
      break;
  }

  const app = new EmberApp(defaults, {

    fingerprint: fingerprintOptions,

    emberCLIDeploy: {
      runOnPostBuild: false, // returns the deployTarget
      configFile: 'config/deploy.js', // optionally specifiy a different config file
      shouldActivate: false, // optionally call the activate hook on deploy
    },

    'ember-cli-babel': {
      includePolyfill: true,
    },

    'ember-bootstrap': {
      'bootstrapVersion': 4,
      'importBootstrapFont': false,
      'importBootstrapCSS': false,
      'whitelist': [],
    },
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  app.import('node_modules/leaflet-routing-machine/dist/leaflet-routing-machine.js');
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.
  return app.toTree();
};
