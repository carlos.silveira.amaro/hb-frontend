import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    const button = this.modelFor('button-net.buttons.new');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('button-net.buttons.new');
    }
  },
});
