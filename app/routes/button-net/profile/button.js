import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';
import { hash } from 'rsvp';
import { set, get } from '@ember/object';

export default Route.extend({
  model(model) {
    let button = this.store.peekRecord('button', model.button_id);
    if (isEmpty(button)) {
      button = this.store.findRecord('button', model.button_id);
    }
    return hash({
      button,
      chats: button.chats,
    });
  },

  afterModel(model) {
    this.loadTagsAndType(model).then((response) => {
      this.setHeadTags(model, response.buttonType, response.buttonTags);
    });
  },

  loadTagsAndType(model) {
    let buttonTags = '';
    let buttonType = model.button.buttonType;
    if (buttonType === 'offer') {
      buttonType = 'ofrece';
      return model.button.offerTags.then((offerTags) => {
        for (let i = 0; i < offerTags.length; i++) {
          buttonTags = buttonTags + ' #' + offerTags.toArray()[i].name;
        }
        return {buttonTags, buttonType};
      });
    } else if (buttonType === 'need') {
      buttonType = 'necesita';
      return model.button.neededTags.then((needTags) => {
        for (let i = 0; i < needTags.length; i++) {
          buttonTags = buttonTags + ' #' + needTags.toArray()[i].name;
        }
        return {buttonTags, buttonType};
      });
    }
    buttonType = 'intercambia';
    return model.button.offerTags.then((offerTags) => {
      for (let i = 0; i < offerTags.length; i++) {
        buttonTags = buttonTags + ' #' + offerTags.toArray()[i].name;
      }
      return model.button.neededTags.then((needTags) => {
        for (let i = 0; i < needTags.length; i++) {
          buttonTags = buttonTags + ' #' + needTags.toArray()[i].name;
        }
        return {buttonTags, buttonType};
      });
    });
  },

  setHeadTags(model, buttonType, buttonTags) {
    const modelImage = get(model, 'button.image').then((image) => {
      if (isEmpty(image)) {
        return 'https://www.dropbox.com/s/jxb5jqtjxvty6m4/imagen_redes_por_defecto.png?dl=0';
      }
      return image.url;
    });
    const headTags = [{
      type: 'meta',
      tagId: 'meta-twitter-card',
      attrs: {
        name: 'twitter:card',
        content: '#helpbuttons ' + 'summary_large_image',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-site',
      attrs: {
        name: 'twitter:site',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-creator',
      attrs: {
        name: 'twitter:creator',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-title',
      attrs: {
        name: 'twitter:title',
        content: get(model.button, 'creator.nickname') + '' +
        buttonType + ',' + buttonTags,
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-description',
      attrs: {
        name: 'twitter:description',
        content: model.button.description,
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-image',
      attrs: {
        name: 'twitter:image',
        content: modelImage,
      },
    },
    ];

    set(this, 'headTags', headTags);
  },
});
