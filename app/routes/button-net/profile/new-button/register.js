import Route from '@ember/routing/route';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import NewButtonOnAuthMixin from '../../../../mixins/new-button-on-auth-mixin';

export default Route.extend(
  UnauthenticatedRouteMixin,
  NewButtonOnAuthMixin,
  {
    headTags: [{
      type: 'meta',
      tagId: 'meta-twitter-card',
      attrs: {
        name: 'twitter:card',
        content: 'summary_large_image',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-site',
      attrs: {
        name: 'twitter:site',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-creator',
      attrs: {
        name: 'twitter:creator',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-title',
      attrs: {
        name: 'twitter:title',
        content: 'HelpButtons - Regístrate',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-description',
      attrs: {
        name: 'twitter:description',
        content: 'Registrate ahora para acceder a Helpbuttons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-image',
      attrs: {
        name: 'twitter:image',
        content: 'https://www.dropbox.com/s/xni2lreczd587fo/imagen_pordefecto_otros.png?dl=0',
      },
    },
    ],

    modelRoute: 'profile.new-button',

    model() {
      return this.store.createRecord('user');
    },
  }
);
