import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';
import { set, get } from '@ember/object';
import { hash } from 'rsvp';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 4000,
  cssClasses: 'notifications-index',
};

export const POLL_INTERVAL = 10000;

let REFRESH = true;

export default Route.extend(
  AuthenticatedRouteMixin,
  {
    authenticationRoute: 'button-net.index.index.button.register',

    notifications: service('notification-messages'),

    queryParams: {
      isButton: {
        refreshModel: true,
        as: 'isButton',
        replace: true,
      },
      id: {
        refreshModel: true,
        as: 'id',
        replace: true,
      },
    },


    _findOrCreateChat(button) {
      if (isEmpty(button)) {
        this.notifications.error(
          'No se ha encontrado el chat',
          NOTIFICATION_OPTIONS,
        );
      }
      return button.chats.then((chat) => {
        if (REFRESH) {
          REFRESH = false;
          return this.modelFor('button-net.index.index.button').chats.reload().then((chat) => {
            if (isEmpty(chat.firstObject)) {
              return this.store.createRecord('chat', {
                button: button,
              }).save()
                .then((newChat) => {
                  return hash({
                    chat: newChat,
                    button,
                    messages: newChat.messages,
                    currentUser: get(this, 'session.currentUser'),
                  });
                });
            }
            return hash({
              chat: chat.firstObject,
              button: button,
              messages: chat.firstObject.messages,
              currentUser: get(this, 'session.currentUser'),
            });
          });
        }
        if (isEmpty(chat.firstObject)) {
          return this.store.createRecord('chat', {
            button: button,
          }).save()
            .then((newChat) => {
              return hash({
                chat: newChat,
                button,
                messages: newChat.messages,
                currentUser: get(this, 'session.currentUser'),
              });
            });
        }
        return hash({
          chat: chat.firstObject,
          button: button,
          messages: chat.firstObject.messages,
          currentUser: get(this, 'session.currentUser'),
        });
      });
    },
    _findChatButton(chat) {
      if (isEmpty(chat)) {
        this.notifications.error(
          'No se ha encontrado el chat',
          NOTIFICATION_OPTIONS,
        );
      }
      return chat.button.then((button) => {
        return hash({
          chat: this.chat,
          button: button,
          messages: this.chat.messages,
          currentUser: get(this, 'session.currentUser'),
        });
      });
    },

    model(params) {
      let paramId = params.id;
      if (isEmpty(params.id)) {
        paramId = this.controllerFor('button-net.index.index.button').routing.router.url.split('/')[3];
      }
      if (params.isButton === 'true' || isEmpty(params.isButton)) {
        const button = this.store.peekRecord('button', paramId);
        if (isEmpty(button)) {
          return this.store.findRecord('button', paramId).then((buttonResponse) => {
            set(this, 'button', buttonResponse);
            return this._findOrCreateChat(buttonResponse);
          })
            .catch(()=>{
              this.notifications.error('No se ha encontrado el chat porque el boton no existe', NOTIFICATION_OPTIONS);
              this.transitionTo('button-net.index.index');
            });
        }
        return this._findOrCreateChat(button);
      }
      const chat = this.store.peekRecord('chat', paramId);
      if (isEmpty(chat)) {
        return this.store.findRecord('chat', paramId).then((chatResponse) => {
          set(this, 'chat', chatResponse);
          return this._findChatButton(chatResponse);
        })
          .catch(()=>{
            this.notifications.error('No se ha encontrado el chat', NOTIFICATION_OPTIONS);
            this.transitionTo('button-net.index.index');
          });
      }
      set(this, 'chat', chat);
      return this._findChatButton(chat);
    },

    afterModel(model) {
      if (!isEmpty(model) && !isEmpty(model.chat)) {
        let poller = this.chatPoller;

        if (poller) {
          this.pollboy.remove(poller);
        }

        poller = this.pollboy.add(this, () => model.messages.reload(), POLL_INTERVAL);

        this.set('chatPoller', poller);
        if (!isEmpty(model.currentUser)) {
          model.currentUser.reload();
        }
        model.button.reload();
        return model.messages.reload();
      }
      return null;
    },
    // openLinkTelegram (username) {
    //     appWindow = window.open("https://t.me/"+username,"_blank");
    //     // setTimeout( function () {if (appWindow) {
    //     //     appWindow.location ="http://www.ourdomain.com/buyourapp";
    //     //         }
    //     //         },1000);
    // },
    // openLinkWhatsapp (userphone) {
    //     appWindow = window.open("https://wa.me/"+"34"+userphone,"_blank");
    //     // setTimeout( function () {if (appWindow) {
    //     //     appWindow.location ="http://www.ourdomain.com/buyourapp";
    //     //         }
    //     //         },1000);
    // },
    // openWhatsaaporTelegram(){
    //   if(user.useExternalConv)
    //   openLinkTelegram(user.userTelegram);
    //
    //   if(user.useWhatsapp)
    //   openLinkWhatsapp (user.phoe);
    //
    // },
    deactivate() {
      set(this, 'controller.otherMessages', null);
      if (this.controller.model.chat.isNew) {
        this.controller.model.chat.destroyRecord();
      }
      const poller = this.chatPoller;

      if (poller) {
        this.pollboy.remove(poller);
      }
    },

    actions: {
      refreshModel: function() {
        this.refresh();
      },
    },
  });
