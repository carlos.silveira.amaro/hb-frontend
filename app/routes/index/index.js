import Route from '@ember/routing/route';
import { get, set, setProperties } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';

export const POLL_INTERVAL = 1000;


export default Route.extend({
  geolocation: service('geolocation'),

  headTags() {
    const controller = this.controllerFor(this.routeName);
    return [{
      type: 'meta',
      tagId: 'meta-twitter-card',
      attrs: {
        name: 'twitter:card',
        content: 'summary_large_image',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-site',
      attrs: {
        name: 'twitter:site',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-creator',
      attrs: {
        name: 'twitter:creator',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-title',
      attrs: {
        name: 'twitter:title',
        content: controller.get('buttonsDisplay') === 'list' ?
          'HelpButtons - Listado de botones' : 'HelpButtons - BotonesDeAyuda',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-description',
      attrs: {
        name: 'twitter:description',
        content: 'Helpbuttons es tu herramienta colaborativa personalizable mediante #tags',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-image',
      attrs: {
        name: 'twitter:image',
        content: 'https://www.dropbox.com/s/jxb5jqtjxvty6m4/imagen_redes_por_defecto.png?dl=0',
      },
    },
    ];
  },


  getUserLocation() {
    const geolocation = get(this, 'geolocation');
    return geolocation.getCurrentPosition()
      .then((coordinates) => {
        const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);
        set(this, 'userPosition', userPosition);
        return userPosition;
      });
  },

  onPoll() {
    const indexController = this.controllerFor('index.index');
    if(indexController.get('userMarker')) {
      const geolocation = get(this, 'geolocation');
      return geolocation.getCurrentPosition()
        .then((coordinates) => {
          const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);
          if (isEmpty(get(indexController, 'lastUserLat')) || isEmpty(get(indexController, 'lastUserLng'))) {
            set(indexController, 'lastUserLat', userPosition.lat);
            set(indexController, 'lastUserLng', userPosition.lng);
            const queryParams = {
              latitude: userPosition.lat,
              longitude: userPosition.lng,
              address: null,
              northEastLat: null,
              northEastLng: null,
              southWestLat: null,
              southWestLng: null,
              userMarker: true,
            };
            this.transitionTo('index', { queryParams });
          } else {
            set(indexController, 'userPosition', userPosition);
            const maxPosLat = userPosition.lat > get(indexController, 'lastUserLat') ?
              userPosition.lat : get(indexController, 'lastUserLat');
            const minPosLat = userPosition.lat > get(indexController, 'lastUserLat') ?
              get(indexController, 'lastUserLat') : userPosition.lat;

            const maxPosLng = userPosition.lng > get(indexController, 'lastUserLng') ?
              userPosition.lng : get(indexController, 'lastUserLng');
            const minPosLng = userPosition.lng > get(indexController, 'lastUserLng') ?
              get(indexController, 'lastUserLng') : userPosition.lng;
            if ((maxPosLat - minPosLat > 0.0002) || (maxPosLng - minPosLng > 0.0002)) {
              set(indexController, 'lastUserLat', userPosition.lat);
              set(indexController, 'lastUserLng', userPosition.lng);
              const queryParams = {
                latitude: get(indexController, 'lastUserLat'),
                longitude: get(indexController, 'lastUserLng'),
                address: null,
                northEastLat: null,
                northEastLng: null,
                southWestLat: null,
                southWestLng: null,
                userMarker: true,
              };
              this.transitionTo('index', { queryParams });
            }
          }
        });
    }
    return Promise.resolve();
  },

  afterModel() {
    let usersPoller = this.get('usersPoller');

    // Make sure we only create one poller instance. Without this every time onPoll
    // is called afterModel would create a new poller causing us to have a growing list
    // of pollers all polling the same thing (which would result in more frequent polling).
    if (!usersPoller) {
      usersPoller = this.get('pollboy').add(this, this.onPoll, POLL_INTERVAL);
      this.set('usersPoller', usersPoller);
    }
  },

  deactivate() {
    const usersPoller = this.get('usersPoller');
    this.get('pollboy').remove(usersPoller);
  },

  centerLeafletMapOnMarker(map, marker) {
    const latLngs = [ marker.getLatLng() ];
    const markerBounds = L.latLngBounds(latLngs);
    map.fitBounds(markerBounds);
  },

  removeLocalStorage() {
    window.localStorage.clear();
    window.location.reload();
  },

  setupController(controller, model, transition) {
    this._super(...arguments);
    const indexModel = this.controllerFor('index').get('model');
    const meta = indexModel.buttons.meta;
    const transitionType = get(controller, 'transitionType');
    // Check if stored transition type for setting received bounds
    // from server inside the meta response
    if (isEmpty(this.controller.get('allowMoveMap')) || this.controller.get('allowMoveMap')) {
      if(isEmpty(transitionType) ||
        transitionType === 'pointType' ||
        transitionType === 'addressType') {
        if ((isEmpty(meta.swl) && !isEmpty(meta.lat)) ||
          (get(this.controllerFor('index'), 'reduceZoomNeed') && !isEmpty(meta.lat))) {
          const leftCorner = L.latLng(meta.lat + 1, meta.lng - 1);
          const rightCorner = L.latLng(meta.lat - 1, meta.lng + 1);
          const bounds = L.latLngBounds(leftCorner, rightCorner);
          setProperties(controller, {
            bounds,
            disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
          });
        } else {
          const leftCorner = L.latLng(meta.swl, meta.swlng);
          const rightCorner = L.latLng(meta.nel, meta.nelng);
          const bounds = L.latLngBounds(leftCorner, rightCorner);
          setProperties(controller, {
            bounds,
            disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
          });
        }
        set(this.controllerFor('index'), 'reduceZoomNeed', false);
        if(!isEmpty(transition.queryParams.hideWelcome)) {
          set(this, 'hideWelcome', transition.queryParams.hideWelcome);
        }
        if(!isEmpty(transition.queryParams.nel)) {
          const leftCorner = L.latLng(meta.swl, meta.swlng);
          const rightCorner = L.latLng(meta.nel, meta.nelng);
          const bounds = L.latLngBounds(leftCorner, rightCorner);
          setProperties(controller, {
            bounds,
            disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
          });
        }
      }
    }
  },
});
