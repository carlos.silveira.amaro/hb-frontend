import Route from '@ember/routing/route';
import InfinityRoute from "ember-infinity/mixins/route";
import { inject as service } from '@ember/service';
import { get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { hash } from 'rsvp';
import currentUser from 'help-button/utils/current-user';
import { readOnly } from '@ember/object/computed';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};


export default Route.extend(InfinityRoute,
// export default Route.extend(
// export default class  extends Route {
// export default class InfinityRoute extends Route
  {
    infinity: service(),
    geolocation: service('geolocation'),
    notifications: service('notification-messages'),
    session: service('session'),
    isAuthenticated: readOnly('session.isAuthenticated'),
    currentUser: currentUser('session'),
    controllerVar: null,

    queryParams: {
      latitude: {
        refreshModel: true,
        as: 'lat',
      },
      longitude: {
        refreshModel: true,
        as: 'lng',
      },
      northEastLat: {
        refreshModel: true,
        as: 'nel',
      },
      northEastLng: {
        refreshModel: true,
        as: 'nelng',
      },
      southWestLat: {
        refreshModel: true,
        as: 'swl',
      },
      southWestLng: {
        refreshModel: true,
        as: 'swlng',
      },
      address: {
        refreshModel: true,
        as: 'adr',
      },
      userMarker: {
        refreshModel: false,
        as: 'user',
      },
      hideWelcome: {
        refreshModel: false,
        as: 'hideWelcome',
      },
      buttonsDisplay: {
        refreshModel: true,
        as: 'buttonsDisplay',
      },
      searchTags: {
        refreshModel: true,
        as: 'searchTags',
      },
      // otherNets: {
      //   refreshModel: true,
      //   as: 'otherNets',
      // },
    },

    hideWelcome: true,

    lastResults: null,

    urlTargetName: null,

    searchTrendingTags: null,
    searchZoneAddress: null,
    dif: null,
    dif2: null,
    reduceZoomNeed: false,

    /**
     * Checks if passed query params have all neccesary properties
     * to perform anyone search properly
   * @method _checkQueryParams
   * @param  {Object}          queryParams
   * @return {Boolean}
   */
    _checkQueryParams(queryParams) {
      const hasPointCoords = !isEmpty(queryParams) &&
        !isEmpty(queryParams.lat) &&
        !isEmpty(queryParams.lng);

      const hasBoundCoords = !isEmpty(queryParams.nel) &&
        !isEmpty(queryParams.nelng) &&
        !isEmpty(queryParams.swl) &&
        !isEmpty(queryParams.swlng);

      const hasAddressParam = !isEmpty(queryParams.adr);

      return hasPointCoords || hasBoundCoords || hasAddressParam;
    },

    savedHref: null,
    // buttonNetName: null,
    notSearched: true,
    deactivated: false,
    beforeModel(transition) {
      this._super(...arguments);
      if (get(this, 'deactivated')) {
        transition.abort();
      }
      if (window.location.href.includes('#') && get(this, 'notSearched')) {
        set(this, 'savedHref', window.location.href);
        get(this, 'savedHref').substr(get(this, 'savedHref').indexOf('#') + 1);
        transition.abort();
        set(this, 'notSearched', false);
        const queryParams = {
          latitude: null,
          longitude: null,
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          reload: true,
          searchTags: get(this, 'savedHref').substr(get(this, 'savedHref').indexOf('#') + 1),
          // buttonNet: transition.params['button-net'].button_net_name,
        };
        this.transitionTo('index', { queryParams });
      } else {
        set(this, 'savedHref', null);
      }
      set(this, 'urlTargetName', transition.targetName);
      const indexController = this.controllerFor('index/index');
      /*
       * If map is loaded, save current transition and abort the previous
       * (see '_replaceTransition' method in index controller)
       */
      indexController._replaceTransition(transition);
      // Add check if force close
        set(this, 'hideWelcome', true);
      
      /*
     * if the QP aren't enought to perform a search its made
     * an user geolocation then we make a transition to this same route again
     * but with the geolocation coordinates as query params
     */
      if(!this._checkQueryParams(transition.queryParams)) {
        return this.geolocation
          .getCurrentPosition()
          .then((coordinates) => {
            const queryParams = {
              latitude: coordinates.latitude,
              longitude: coordinates.longitude,
              northEastLat: null,
              northEastLng: null,
              southWestLat: null,
              southWestLng: null,
              hideWelcome: this.hideWelcome,
              // buttonNet: transition.params['button-net'].button_net_name,
            };
            set(this, 'reduceZoomNeed', true);
            this.transitionTo(this.urlTargetName, { queryParams });
          });
      }
      return null;
    },

    lastParam: null,
    model(params) {
      // window.history.replaceState({} , 'HelpButtons', this._router.url.slice(0, -1));
      params.perPage = 2;
      params.totalPagesParam = 'meta.total-pages';
      const queryParams = {};
      const {
        latitude,
        longitude,
        northEastLat,
        northEastLng,
        southWestLat,
        southWestLng,
        address,
        hideWelcome,
        buttonsDisplay,
        searchTags,
        reload,
        offerFilter,
        needFilter,
        perPage,
        totalPagesParam,
        // otherNets,
      } = params;
      set(this, 'lastParam', params);
      /*
      * We check each QP to send on search the QP with value only
      */
      if(!isEmpty(latitude)) { queryParams.lat = latitude; }
      if(!isEmpty(longitude)) { queryParams.lng = longitude; }
      if(!isEmpty(northEastLat)) { queryParams.nel = northEastLat; }
      if(!isEmpty(northEastLng)) { queryParams.nelng = northEastLng; }
      if(!isEmpty(southWestLat)) { queryParams.swl = southWestLat; }
      if(!isEmpty(southWestLng)) { queryParams.swlng = southWestLng; }
      if(!isEmpty(address)) { queryParams.adr = address; }
      if(!isEmpty(this.hideWelcome)) { queryParams.hideWelcome = hideWelcome; }
      if(!isEmpty(buttonsDisplay)) { queryParams.buttonsDisplay = buttonsDisplay; }
      if(!isEmpty(searchTags)) { queryParams.searchTags = searchTags; }
      if(!isEmpty(perPage)) { queryParams.perPage = perPage; }
      if(!isEmpty(totalPagesParam)) { queryParams.totalPagesParam = totalPagesParam; }
      if(!isEmpty(searchTags)) { queryParams.offerFilter = offerFilter; }
      if(!isEmpty(searchTags)) { queryParams.needFilter = needFilter; }

      // if(!isEmpty(buttonNet)) { queryParams.buttonNet = buttonNet; }
      // if(!isEmpty(otherNets)) { queryParams.otherNets = otherNets; }

      // Buttons search
      // return this.infinity.model('product');
      // const buttons = this.store.query('button', queryParams)
      let buttons;
      if (queryParams.buttonsDisplay === 'list') {
        buttons = this.infinity.model('button', queryParams);
      } else {
        buttons = this.store.query('button', queryParams);
      }
        buttons.then((buttonsResponse) => {
          // Check if buttons response has empty Meta
          const emptyMeta = Object.keys(buttonsResponse.meta).length === 0 &&
          typeof buttonsResponse.meta === 'object';
          // Check if is meta is empty & the query param address is passed
          if(emptyMeta && !isEmpty(queryParams.adr)) {
            const indexController = this.controllerFor('index/index');
            // RESTORE
            this.notifications.error(
              'Disculpe, no fué posible encontrar la dirección',
              NOTIFICATION_OPTIONS
            );

            // If exists, restore a previous successfull meta
            if(!isEmpty(indexController) && get(indexController, 'loadedMap')) {
              return this.lastResults;
            }
            queryParams.adr = null;
            // If there is no previous metas, reset queryParams and reload page
            return this.transitionTo(this.urlTargetName, { queryParams });
          }
          set(this, 'lastResults', buttonsResponse);
          const searchTrendingTagsArray = [];
          const trendingTagsKeys = Object.keys(buttonsResponse.meta['tags-counter']);
          for (let i = 0; i < trendingTagsKeys.length; i++) {
            const tagVar = this.store.peekRecord('tag', trendingTagsKeys[i]);
            if (!isEmpty(tagVar)) {
              searchTrendingTagsArray.push(tagVar);
            } else {
              this.store.findRecord('tag', trendingTagsKeys[i]).then((resultTag) => {
                searchTrendingTagsArray.push(resultTag);
                set(this, 'searchTrendingTags', searchTrendingTagsArray);
              });
            }
          }
          set(this, 'searchZoneAddress', queryParams.adr);
          if (isEmpty(get(this, 'searchZoneAddress'))) {
            let dif;
            let dif2;
            if (!isEmpty(queryParams.lat) && !isEmpty(queryParams.lng)) {
              dif = queryParams.lat;
              dif2 = queryParams.lng;
            } else {
              if (queryParams.nel > queryParams.swl) {
                dif = queryParams.nel - (queryParams.nel - queryParams.swl) / 2;
                if (queryParams.nelng > queryParams.swlng) {
                  dif2 = queryParams.nelng - (queryParams.nelng - queryParams.swlng) / 2;
                } else {
                  dif2 = queryParams.swlng - (queryParams.swlng - queryParams.nelng) / 2;
                }
              } else {
                dif = queryParams.swl - (queryParams.swl - queryParams.nel) / 2;
                if (queryParams.nelng > queryParams.swlng) {
                  dif2 = queryParams.nelng - (queryParams.nelng - queryParams.swlng) / 2;
                } else {
                  dif2 = queryParams.swlng - (queryParams.swlng - queryParams.nelng) / 2;
                }
              }
            }
            set(this, 'dif', dif);
            set(this, 'dif2', dif2);
          }
          // set(this, 'searchTrendingTags', buttonsResponse.meta["tags-counter"]);
          set(this, 'searchTrendingTags', searchTrendingTagsArray);
          return buttonsResponse;
        });
      return hash({
        buttons,
        currentUser: this.currentUser,
      });
    },

    setupController(controller, model) {
      this._super(controller, model);
      set(controller, 'buttonsFiltered', null);
      set(controller, 'searchTrendingTags', get(this, 'searchTrendingTags'));
      set(controller, 'searchZoneAddress', get(this, 'searchZoneAddress'));
      if (isEmpty(get(this, 'searchZoneAddress'))) {
        const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
          get(this, 'dif') + ',' + get(this, 'dif2') + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
        $.getJSON(geocodingAPI).then((json) => {
          if (json.status === 'OK') {
            const result = json.results[0];
            let locationName = result.formatted_address.split(',');
            locationName = locationName.reverse();
            if (locationName.length > 1 ) {
              locationName = locationName[1].replace(/[0-9]/g, '') + ',' + locationName[0];
            }
            // IDEA:
            set(controller, 'searchZoneAddress', locationName);
          }
        });
      }
      set(controller, 'reduceZoomNeed', get(this, 'reduceZoomNeed'));
      set(this, 'controllerVar', controller);
      set(this, 'reduceZoomNeed', false);
    },

    deactivate() {
      const controller = this.controllerFor('index');
      if (!isEmpty(get(controller, 'contextTransition'))) {
        get(controller, 'contextTransition').abort();
      }
      // Deactivate transitions for 0.01 second
      set(this, 'deactivated', true);
      const context = this;
      setTimeout(function() {
        set(context, 'deactivated', false);
      }, 10);
    },

    contextTransition: null,
    actions: {
      loading(transition) {
        const controller = this.controllerFor('index');
        controller.set('loading', true);
        set(controller, 'contextTransition', transition);
        transition.promise.finally(()=>{
          controller.set('loading', false);
        });
      },
    },
  }
);
