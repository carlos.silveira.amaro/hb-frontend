import Route from '@ember/routing/route';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import { get } from '@ember/object';


export default Route.extend(
  UnauthenticatedRouteMixin,
  {
    headTags: [{
      type: 'meta',
      tagId: 'meta-twitter-card',
      attrs: {
        name: 'twitter:card',
        content: 'summary_large_image',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-site',
      attrs: {
        name: 'twitter:site',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-creator',
      attrs: {
        name: 'twitter:creator',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-title',
      attrs: {
        name: 'twitter:title',
        content: 'HelpButtons - Inicia sesión',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-description',
      attrs: {
        name: 'twitter:description',
        content: 'Inicia sesión para acceder a Helpbuttons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-image',
      attrs: {
        name: 'twitter:image',
        content: 'https://www.dropbox.com/s/jxb5jqtjxvty6m4/imagen_redes_por_defecto.png?dl=0',
      },
    },
    ],
    model: function(params) {
      const vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
      this.controllerFor('application').set('searchInput', params.search_value);

      return this.store.createRecord('user');
    },

    queryParams: {email: {refreshModel: true}},


    setupController(controller, model) {
      this.controllerFor('login').get('email');
      this.set('queryParams', get(model, 'queryParams'));
    },
  }
);
