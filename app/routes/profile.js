import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { get, set } from '@ember/object';
import { resolve } from 'rsvp';


export default Route.extend(
  AuthenticatedRouteMixin,
  {
    session: service(),

    queryParams: {
      searchTags: {
        refreshModel: true,
        as: 'searchTags',
      },
    },


    model() {
      const currentUser = this.session.currentUser;
      const name = currentUser.id;

      return currentUser.constructor.toString() === 'DS.PromiseObject' ?
        currentUser : resolve(currentUser);
    },

    deactivate() {
      set(this.controller, 'selectedButton', null);
      set(this.controller, 'showProfile', null);
    },
  }
);
