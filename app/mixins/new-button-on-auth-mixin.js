import Mixin from '@ember/object/mixin';
import { get, getProperties } from '@ember/object';
import { inject as service } from '@ember/service';
import { hash, resolve } from 'rsvp';
import { isEmpty } from '@ember/utils';

export default Mixin.create({
  session: service(),
  modelRoute: '',

  actions: {
    willTransition(transition) {
      const button = this.modelFor(get(this, 'modelRoute'));

      if (get(this, 'session.isAuthenticated') && get(button, 'isNew')) {
        transition.abort();

        const { offerTags, neededTags } = getProperties(button, 'offerTags', 'neededTags');
        button.save()
          .then((savedButton) => {
            return hash({
              offerTags: isEmpty(offerTags) ? resolve() : savedButton.updateRelationship('offerTags'),
              neededTags: isEmpty(neededTags) ? resolve() : savedButton.updateRelationship('neededTags'),
            });
          })
          .then(() => {
            transition.retry();
          });
      }
    },
  },
});
