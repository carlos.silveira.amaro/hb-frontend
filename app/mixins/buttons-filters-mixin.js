import Mixin from '@ember/object/mixin';
import { computed, set, get, getProperties } from '@ember/object';
import { readOnly, or } from '@ember/object/computed';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';

export default Mixin.create({
  routing: service('-routing'),
  /**
   * This prop must be overwrited from each controller where this mixin will be imported
   * @type {Array}
   */
  buttons: null,
  buttonsFiltered: null,

  session: service('session'),
  currentUser: readOnly('session.currentUser'),

  selectedTags: computed(
    'routing.router.currentURL',
    {
      get() {
        const tags = new Array();
        if (!isEmpty(this.routing)) {
          let currentURL = decodeURI(this.routing.router.currentURL);
          if (!isEmpty(currentURL) && currentURL.includes('searchTags')) {
            const tagsIndex = currentURL.indexOf('searchTags');
            let lastTagIndex = currentURL.length;
            let tagUrlParams = currentURL.substring(tagsIndex + 11, lastTagIndex);
            if (tagUrlParams.indexOf('&') !== -1) {
              lastTagIndex = tagUrlParams.indexOf('&');
              tagUrlParams = tagUrlParams.substring(0, lastTagIndex);
            }
            const context = this;
            for (let i = 0; tagUrlParams.includes('%2C'); i++) {
              let fixedTerm = tagUrlParams.substring(0, tagUrlParams.indexOf('%2C'));
              fixedTerm=fixedTerm.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
              fixedTerm=fixedTerm.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");
              tags[i] = this.store.query('tag', {
                'filter[name]': fixedTerm})
                .then((tagsResponse) => {
                  return isEmpty(tagsResponse.firstObject) ? null : tagsResponse.firstObject;
                });
              tagUrlParams = tagUrlParams.replace(tagUrlParams.substring(0, tagUrlParams.indexOf('%2C') + 3), '');
            }

            if (!isEmpty(tagUrlParams)) {
              let fixedTerm = tagUrlParams.substring(0, tagUrlParams.length);
              fixedTerm=fixedTerm.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
              fixedTerm=fixedTerm.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");
              tags[tags.length] = this.store.query(
                'tag', { 'filter[name]': fixedTerm})
                .then((tagsResponse) => {
                  return isEmpty(tagsResponse.firstObject) ? null : tagsResponse.firstObject;
                });
              tagUrlParams = tagUrlParams.replace(tagUrlParams.substring(0, tagUrlParams.length), '');
            }
            Promise.all(tags).then(values => {
              currentURL = decodeURI(window.location.href);
              // if (currentURL.includes('searchTags') || !isEmpty(values[0])) {
                isEmpty(values[0]) ? null : set(this, 'selectedTags', values);
                this.send('updateUrl');
              // }
            });
          }
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),


  prevButtonsType: false,

  ownedButtons: computed(
    'buttons.[]',
    'buttonsFiltered.[]',
    'selectedTags.[]',
    'buttons.@each',
    'buttonsFiltered.@each',
    'selectedTags.@each',
    'showPulsedButtons',
    'currentUser',
    'currentUser.{buttons.[],ownedButtons.[]}',
    {
      get() {
        const currentUser = get(this, 'currentUser');
        if(!isEmpty(currentUser) && get(currentUser, 'admin')) {
          const relationshipName = 'ownedButtons';
          get(currentUser, relationshipName)
             .then((buttonsResp) => {
               set(this, 'ownedButtons', buttonsResp);
           });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  lastButtonsCached: null,

  buttonsToShowCached: computed('buttonsToShow', function() {
    let merge;
    if (!isEmpty(get(this, 'lastButtonsCached.length')) || get(this, 'lastButtonsCached.length') > 0) {
      var ids = new Set(get(this, 'lastButtonsCached').map(d => d.id));
      merge = [...get(this, 'lastButtonsCached').toArray(), ...get(this, 'buttonsToShow').toArray().filter(d => !ids.has(d.id))];
    } else {
      merge = get(this, 'buttonsToShow');
    }
    set(this, 'lastButtonsCached', merge);
    return merge;
  }),


  buttonsToShow: computed(
    'buttons.[]',
    'buttonsFiltered.[]',
    'selectedTags.[]',
    'buttons.@each',
    'buttonsFiltered.@each',
    'selectedTags.@each',
    'showPulsedButtons',
    'currentUser',
    'currentUser.{buttons.[],ownedButtons.[]}',
    'ownedButtons',
     function() {
        const currentUser = get(this, 'currentUser');
        const { buttons, buttonsFiltered, selectedTags } = getProperties(this,
          'buttons', 'buttonsFiltered', 'selectedTags');

        if (this.prevButtonsType !== this.showPulsedButtons) {
          set(this, 'prevButtonsType', this.showPulsedButtons);
          set(this, 'buttonsFiltered', null);
          set(this, 'resetFilters', true);
        }
        let buttonsToShow = buttonsFiltered === null ? buttons : buttonsFiltered;

        if((!isEmpty(currentUser) && get(currentUser, 'admin') && !this.routing.currentPath.includes('profile')) || (!isEmpty(currentUser) && get(currentUser, 'superAdmin') && !this.routing.currentPath.includes('profile'))) {
         const relationshipName = 'ownedButtons';
         // FILTER BY TAGS
         if(!isEmpty(selectedTags) && !isEmpty(selectedTags[0])) {
           const selectedTagIds = selectedTags.getEach('id').uniq();
           const btnshow = buttonsToShow.filter((button) => {
             const buttonTagIds = [
               ...button.hasMany('neededTags').ids(),
               ...button.hasMany('offerTags').ids(),
             ].uniq();
             const hasTags = selectedTagIds.every((value) => {
               return buttonTagIds.includes(value);
             });
             return hasTags;
           });
           buttonsToShow = btnshow;
         }
         if (this.routing.currentPath.includes('profile') && !get(this, 'showPulsedButtons')) {
           return buttonsToShow;
         } else if (this.routing.currentPath.includes('profile') && get(this, 'showPulsedButtons')) {
           return buttonsToShow;
         } else {
           if (!isEmpty(get(this, 'ownedButtons')) && get(this, 'ownedButtons.length') > 0) {
             const arr = [];
             if (!isEmpty(get(this, 'ownedButtons'))) {
               for (let i = 0; i < get(this, 'ownedButtons').length; i++) {
                 arr.push(get(this, 'ownedButtons').objectAt(i));
               }
             }
             if (!isEmpty(buttonsToShow)) {
               for (let i = 0; i < buttonsToShow.length; i++) {
                 arr.push(buttonsToShow.objectAt(i));
               }
             }
             return arr;
           } else {
             return buttonsToShow;
           }
         }
       } else {
         // FILTER BY TAGS
         if(!isEmpty(selectedTags) && !isEmpty(selectedTags[0])) {
           const selectedTagIds = selectedTags.getEach('id').uniq();
           const btnshow = buttonsToShow.filter((button) => {
             const buttonTagIds = [
               ...button.hasMany('neededTags').ids(),
               ...button.hasMany('offerTags').ids(),
             ].uniq();
             const hasTags = selectedTagIds.every((value) => {
               return buttonTagIds.includes(value);
             });
             return hasTags;
           });
           return btnshow;
         }
         return buttonsToShow;
       }
    }
  ),

  lastSearchUrl: null,
  updateUrl: true,
  isFirstLoad: true,

  actions: {
    updateSelectedTags(selectedTags) {
      set(this, 'selectedTags', selectedTags);
      this.send('updateUrl');
    },

    toggleSelectedTag(tag) {
      if(isEmpty(this.selectedTags)) {
        set(this, 'selectedTags', new Array());
        this.selectedTags.pushObject(tag);
      } else {
        set(this, 'tagFiltered', false);
        if (this.selectedTags.includes(tag)) {
          this.selectedTags.removeObject(tag);
          if (isEmpty(this.selectedTags)) {
            set(this, 'selectedTags', null);
          }
          this.send('updateUrl');
        } else {
          this.selectedTags.pushObject(tag);
          this.send('updateUrl');
        }
      }
    },
    updateUrl() {
      const urlParams = new URLSearchParams(window.location.search);
      let updateUrl = get(this, 'updateUrl');
      let newUrl = '?';

      if (!isEmpty(this.hideWelcome)) {
        newUrl += 'hideWelcome=' + this.hideWelcome + '&'
      }
      if (!isEmpty(this.needFilter)) {
        newUrl += 'needFilter=' + this.needFilter + '&'
      }
      if (!isEmpty(this.offerFilter)) {
        newUrl += 'offerFilter=' + this.offerFilter + '&'
      }
      if (!isEmpty(this.northEastLat)) {
        newUrl += 'nel=' + this.northEastLat + '&'
      }
      if (!isEmpty(this.northEastLng)) {
        newUrl += 'nelng=' + this.northEastLng + '&'
      }
      if (!isEmpty(this.southWestLat)) {
        newUrl += 'swl=' + this.southWestLat + '&'
      }
      if (!isEmpty(this.southWestLng)) {
        newUrl += 'swlng=' + this.southWestLng + '&'
      }
      if (!isEmpty(this.latitude)) {
        newUrl += 'lat=' + this.latitude + '&'
      }
      if (!isEmpty(this.longitude)) {
        newUrl += 'lng=' + this.longitude + '&'
      }
      if (!isEmpty(this.address)) {
        newUrl += 'adr=' + this.address + '&'
      }
      if (!isEmpty(this.userMarker)) {
        newUrl += 'userMarker=' + this.userMarker + '&'
      }
      if (!isEmpty(this.buttonsDisplay)) {
        newUrl += 'buttonsDisplay=' + this.buttonsDisplay + '&'
      }
      let selectedTagsArray;
      if (!isEmpty(this.selectedTags) && !isEmpty(this.selectedTags[0])) {
        selectedTagsArray = this.selectedTags.map(function(item) {
          return !isEmpty(item) ? item['name'] : null;
        });
        updateUrl = true;
        if (!isEmpty(this.selectedTags)) {
          newUrl += 'searchTags=' + selectedTagsArray.join('%2C') + '&'
        }
      } else {
        if (!isEmpty(urlParams.get('searchTags')) && get(this, 'isFirstLoad')) {
          newUrl += 'searchTags=' + urlParams.get('searchTags') + '&'
        }
      }
      if (!get(this, 'tagFiltered')) {
        set(this, 'tagFiltered', true);
        if (newUrl == '?') {
            this.transitionToRoute(this.routing.router.currentPath, { queryParams: {searchTags: ''}});
        } else {
          const currentPath = this.routing.router.currentPath === 'index.index.index' ? '' : this.routing.router.currentPath;
          this.transitionToRoute(currentPath + '/' + newUrl.slice(0, -1))
        }
      } else {
        window.history.pushState({} , 'HelpButtons', newUrl.slice(0, -1));
      }
      set(this, 'isFirstLoad', false);
    },
  },
});
