import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Model.extend({
  lastMessageDate: attr('date'),
  user: belongsTo('user'),
  messages: hasMany('message'),
  button: belongsTo('button'),
  buttonCreator: belongsTo('user'),
  unreadMessages: attr('number'),

  lastMessageDateFormated: computed('lastMessageDate', function() {
    if (!isEmpty(this.lastMessageDate)) {
      const oneDay = 24 * 60 * 60 * 1000;
      const firstDate = new Date();
      const secondDate = this.lastMessageDate;

      const diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
      if (diffDays === 0) {
        return 'hoy';
      } else if (diffDays === 1) {
        return '1 día';
      }
      return diffDays + 'días';
    }
    return 'No hay mensajes';
  }),
});
