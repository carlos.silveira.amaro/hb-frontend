import Model from 'ember-data/model';
import ModelMixin from 'ember-data-updating-json-api-relationships/mixins/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import {  computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Model.extend(
  ModelMixin,
  {
    email: attr('string'),
    nickname: attr('string', { defaultValue: 'Sin nombre' }),
    internalId: attr('number'),

    location: attr('string'),
    description: attr('string'),
    ownedButtonsCounter: attr('number'),
    // Vitual attribute so we can authenticate when registering
    randomPassword: attr('string'),

    // Attribute when changing password
    currentPassword: attr('string'),
    password: attr('string'),
    passwordConfirmation: attr('string'),
    name: attr('string'),
    phone: attr('string'),
    showPhone: attr('boolean', { defaultValue: false }),
    appLanguage: attr('string'),
    active: attr('boolean'),
    useExternalConv: attr('boolean', { defaultValue: false }),
    useWhatsapp: attr('boolean', { defaultValue: false }),
    useTelegram: attr('boolean', { defaultValue: false }),
    userTelegram: attr('string'),
    deliverInterests: attr('boolean', { defaultValue: false }),
    tagsLongitude: attr('number'),
    tagsLatitude: attr('number'),
    tagsDistance: attr('number', { defaultValue: 10000 }),
    blocked: attr(),
    blockedBy: attr(),
    liked: attr(),
    actionEvent: attr('string'),
    likes: attr('number'),
    otherUserId: attr('number'),

    chats: hasMany('chat', { inverse: null }),

    admin: attr('boolean'),
    superAdmin: attr('boolean'),

    avatar: belongsTo('image', { inverse: null }),

    ownedButtons: hasMany('button', { inverse: null }),
    buttons: hasMany('button'),
    buttonNets: hasMany('buttonNet', { inverse: null }),
  
    userTags: hasMany('userTag', { inverse: null }),
    languages: hasMany('language', { inverse: null }),

    hasAvatar: computed(
      'avatar',
      function() {
        return !isEmpty(this.belongsTo('avatar').id());
      }
    ),

    notificationsNumber: attr('number'),

    hasNotifications: computed(
      'notificationsNumber',
      function() {
          return this.notificationsNumber > 0;
      },
    ),

    capitalizedName: computed(
      'nickname',
      function() {
        return this.nickname.capitalize();
      },
    ),
  }
);
