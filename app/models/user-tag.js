import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed, set, get } from '@ember/object';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { isEmpty } from '@ember/utils'


export default Model.extend({
  name: computed( 'tag', {
    get() {
      get(this, 'tag').then((tagResp) => {
        !isEmpty(tagResp) ? set(this, 'name', tagResp.name) : set(this, 'name', '');
      })
    },
    set(key, value) {
      return value;
    },
  }),
  activeButtonsCounter: computed( 'tag', {
    get() {
      get(this, 'tag').then((tagResp) => {
        !isEmpty(tagResp) ? set(this, 'activeButtonsCounter', tagResp.activeButtonsCounter) : set(this, 'activeButtonsCounter', '');
      })
    },
    set(key, value) {
      return value;
    },
  }),
  tagType: attr('number', { defaultValue: 2 }),

  user: belongsTo('user'),
  tag: belongsTo('tag'),

  needType: computed( 'tagType', function() {
    if (this.tagType==0 || this.tagType==3) {
      return false;
    }
    return true;
  }),

  offerType: computed( 'tagType', function() {
    if (this.tagType==1 || this.tagType==3) {
      return false;
    }
    return true;
  }),

  setType: computed( 'needType','offerType', {
    get() {
      set(this,'hasDirtyAttributes', true);
      if (this.needType && this.offerType) {
        set(this,'tagType',2);
      } else {
        if (this.needType) {
          set(this,'tagType',1);
        }
        if (this.offerType) {
          set(this,'tagType',0);
        }
        if (!this.needType && !this.offerType) {
          set(this,'tagType',3);
        }
      }
      return null;
    },
    set(key, value) {
      return value;
    }
  }),

  buttonNets: attr(),
});
