import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  url: attr('string'),
  thumb: attr('string'),
  smallThumb: attr('string'),
  tinyThumb: attr('string'),
});
