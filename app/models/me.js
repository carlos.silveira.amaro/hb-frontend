import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  email: attr('string'),
  name: attr('string'),
  showPhone: attr('boolean', { defaultValue: false }),
  useExternalConv: attr('boolean', { defaultValue: false }),
  useTelegram: attr('boolean', { defaultValue: false }),
  useWhatsapp: attr('boolean', { defaultValue: false }),
  userTelegram: attr('boolean', { defaultValue: false }),
  deliverInterests: attr('boolean', { defaultValue: false }),
  likes: attr('number', { defaultValue: 0 }),
  liked: attr(),
  blocked: attr(),
  blockedBy: attr(),
  actionEvent: attr(),

  admin: attr('boolean'),
  superAdmin: attr('boolean'),

  // Attribute when changing password
  password: attr('string'),
  passwordConfirmation: attr('string'),

  avatar: belongsTo('image', { inverse: null }),

  buttons: hasMany('button', { inverse: 'creator' }),

  chats: hasMany('chat', { inverse: null }),
});
