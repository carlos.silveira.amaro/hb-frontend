import Model from 'ember-data/model';
import ModelMixin from 'ember-data-updating-json-api-relationships/mixins/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { inject as service } from '@ember/service';
import { A } from '@ember/array';
import { computed, get, set, getProperties } from '@ember/object';
import { readOnly, equal } from '@ember/object/computed';
import { resolve } from 'rsvp';
import { isEmpty } from '@ember/utils';

export const NEED = 'need';
export const OFFER = 'offer';
export const CHANGE = 'change';

export const BUTTON_TYPES = { NEED, OFFER, CHANGE };

export default Model.extend(
  ModelMixin,
  {
    //  SERVICES
    session: service('session'),

    // MODEL ATTRIBUTES
    description: attr('string'),
    internalId: attr('number'),
    name: attr('string'),
    urlName: attr('string'),
    netUrl: attr('string'),
    actionEvent: attr('string'),
    creatorId: attr('number'),
    active: attr('boolean'),
    btnCount: attr('number'),
    latitude: attr('number'),
    longitude: attr('number'),
    createdAt: attr('date'),
    locationName: attr('string'),
    privacy: attr('boolean'),
    netOptions: attr(),
    imgUrl: attr('string'),
    adminUsers: attr(),
    blockedUsers: attr(),
    allowedUsers: attr(),
    otherNetIds: attr(),

    //DISPLAY OPTIONS
    display: attr('boolean', { defaultValue: false }),

    // MODEL RELATIONSHIPS
    creator: belongsTo('user', { inverse: null }),
    icon: belongsTo('image', { inverse: null }),
    buttons: hasMany('button', { inverse: null }),
    tags: hasMany('tag', { inverse: null }),//array?

    // adminUsers: hasMany('user', { inverse: null }),//array?
    // blockedUsers: hasMany('user', { inverse: null }),//array?
    // acceptedUsers: hasMany('user', { inverse: null }),//array?


    // NET_OPTIONS ARRAY OF BOOLEANS MEANING  [0-PRICE_MODULE, 1-CAUSE_MODULE, 2-TRANSPORT_MODULE, 3-NO_MAP_MODULE]
    netOptions: attr(''),

    // COMPUTED PROPERTIES
    isAuthenticated: readOnly('session.isAuthenticated'),
    currentUser: readOnly('session.currentUser'),

    isMine: computed(
      'isAuthenticated',
      'currentUser.internalId',
      'creator.id',
      {
        get() {
          if(get(this, 'isAuthenticated')) {
            if (!isEmpty(this.currentUser)) {
              const currentUserPromise = get(this, 'currentUser').constructor.toString() === 'DS.PromiseObject' ?
                get(this, 'currentUser') : resolve(get(this, 'currentUser'));
              currentUserPromise.then((currentUser) => {
                if (isEmpty(currentUser)) {
                  set(this, 'isMine', false);
                } else {
                  const isMine = currentUser.internalId.toString() === this.belongsTo('creator').id();
                  set(this, 'isMine', isMine);
                }
              });
            }
          }
          return false;
        },
        set(key, value) { return value; },
      }
    ),

    notificationsNumber: attr('number'),

    hasNotifications: computed( //if net has new buttons
      'notificationsNumber',
      'chatUnread',
      function() {
        return this.notificationsNumber > 0 || this.chatUnread;
      },
    ),

    location: computed(
      'latitude',
      'longitude',
      function() {
        let { latitude, longitude } = getProperties(this, 'latitude', 'longitude');
        if (isEmpty(latitude)) {
          latitude = 0;
        }
        if (isEmpty(longitude)) {
          longitude = 0;
        }
        return [ latitude, longitude ];
      }
    ),

  }
);
