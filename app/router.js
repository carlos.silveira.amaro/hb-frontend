import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL,
});

Router.map(function() {
  this.route('login');
  this.route('dashboard');
  this.route('signup');

  this.route('buttons', function() {
    this.route('button', { path: 'button/:button_id'}, function() {
      this.route('my-chat', { path: 'chat/:chat_id'});
      this.route('chat', { path: 'chat'});
    });
    this.route('new', { path: '/:button_id'}, function() {
      this.route('description');
      this.route('location');
      this.route('activate-button');
      this.route('share');
      this.route('login');
      this.route('register');
      this.route('date');
    });
  });

  this.route('profile', function() {
    this.route('button', { path: 'button/:button_id'}, function() {
      this.route('my-chat', { path: 'chat/:chat_id'});
      this.route('chat', { path: 'chat'});
    });
    this.route('new-button', { path: '/:button_id'}, function() {
      this.route('description');
      this.route('location');
      this.route('activate-button');
      this.route('login');
      this.route('register');
      this.route('share');
      this.route('date');
    });
  });
  this.route('components-repo');
  this.route('register');
  this.route('recovery');
  this.route('index', { path: '' }, function() {
    this.route('index', { path: '' }, function() {
      this.route('button', { path: 'button/:button_id'}, function() {
        this.route('chat', { path: 'chat'});
        this.route('login');
        this.route('register');
        this.route('my-chat', { path: 'chat/:chat_id'});
      });
      this.route('new-button', { path: '/:button_id'}, function() {
        this.route('description');
        this.route('location');
        this.route('activate-button');
        this.route('login');
        this.route('register');
        this.route('share');
        this.route('date');
      });

      this.route('new', function() {});
    });
  });
  this.route('user', { path: 'user/:user_id'}, function() {
    this.route('buttons', function() {
      this.route('chat', { path: 'chat/:chat_id'});
    });
  });
  this.route('profile-edition');
  this.route('others');
  this.route('button-net', { path: '/:button_net_name'}, function() {
    this.route('login');
    this.route('others');
    this.route('profile-edition');
    this.route('buttons', function() {
      this.route('button', { path: 'button/:button_id'}, function() {
        this.route('my-chat', { path: 'chat/:chat_id'});
        this.route('chat', { path: 'chat'});
      });
      this.route('new', { path: '/:button_id'}, function() {
        this.route('description');
        this.route('location');
        this.route('activate-button');
        this.route('share');
        this.route('login');
        this.route('register');
        this.route('date');
      });
    });
    this.route('profile', function() {
      this.route('button', { path: 'button/:button_id'}, function() {
        this.route('my-chat', { path: 'chat/:chat_id'});
        this.route('chat', { path: 'chat'});
      });
      this.route('new-button', { path: '/:button_id'}, function() {
        this.route('description');
        this.route('location');
        this.route('activate-button');
        this.route('login');
        this.route('register');
        this.route('share');
        this.route('date');
      });
    });
    this.route('recovery');
    this.route('register');
    this.route('user', { path: 'user/:user_id'}, function() {
      this.route('buttons', function() {
        this.route('chat');
      });
    });
    this.route('index', { path: '' }, function() {
      this.route('index', { path: '' }, function() {
        this.route('button', { path: 'button/:button_id'}, function() {
          this.route('chat', { path: 'chat'});
          this.route('login');
          this.route('register');
          this.route('my-chat', { path: 'chat/:chat_id'});
        });
        this.route('new-button', { path: '/:button_id'}, function() {
          this.route('description');
          this.route('location');
          this.route('activate-button');
          this.route('login');
          this.route('register');
          this.route('share');
          this.route('date');
        });

        this.route('new', function() {});
        //
        // this.route('new', function() {});
      });
    });
  });

  this.route('404', { path: '/*path' });
  this.route('error');
});


export default Router;
