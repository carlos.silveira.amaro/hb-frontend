import { helper } from '@ember/component/helper';
import { isEmpty } from '@ember/utils';
import { htmlSafe } from '@ember/string';

/**
  * this helper is used to generate the div icons for each marker of the leaflet map.
  * It takes two parameters, a number, relevant data for the estate, and a string
  * the URL of the logo to show in the div icon
  *
  * @param estate {DS.model.Estate} the estate to show its data in the map
  *
  * @return {HtmlSafeString} Html img element in string format
  */
export function buttonIcon([ button, avatar, icon]) {
  const avatarThumbUrl = isEmpty(avatar) || isEmpty(avatar.content) ? null : avatar.content.tinyThumb;
  const imgUrl = isEmpty(avatarThumbUrl) ? '/assets/svg/logo/icon_marker.svg' : avatarThumbUrl;
  const imgUrlnet = isEmpty(avatar) ? '/assets/svg/logo/icon_marker.svg' : 'https://help-buttons-staging.s3.eu-west-3.amazonaws.com/statics/assets/categories/' + avatar.toString() + '.png';
  const imgCreate = '/assets/svg/logo/cross.svg';
  var buttonTag = '';
  var colorTag = '';
    if(button.isNeedButton && !isEmpty(button.neededTags.firstObject)){
        buttonTag=button.neededTags.firstObject.name;
        colorTag='red'
    };
    if(button.isOfferButton && !isEmpty(button.offerTags.firstObject)){
        buttonTag=button.offerTags.firstObject.name;
        colorTag='green'
    }
    if(button.isChangeButton && !isEmpty(button.offerTags.firstObject) && !isEmpty(button.neededTags.firstObject)){
        buttonTag=button.offerTags.firstObject.name + ' x #' + button.neededTags.firstObject.name;
        colorTag='change'
    }
  const notificationClass = button.chatUnread ? 'notif-marker' : '';
  let htmlOutput;
  switch (icon) {
    case 'desde':
      // htmlOutput = `
      //   <figure class="${notificationClass} button-icon button-icon--offer">
      //     <img src=${imgUrl} class="button-icon__image "/>
      //     <span class="button-icon__arrow"></span>
      //   </figure>
      // `;
      htmlOutput = `
        <figure class="${notificationClass} button-icon button-icon--offer">
          <img src=${imgUrl} class="button-icon__image "/>
          <span class="button-icon__arrow"></span>
        </figure>
      `;
      break;
    case 'hasta':
      htmlOutput = `
        <figure class="button-icon button-icon--neutro">
          <img src=${imgUrl} class="button-icon__image "/>
          <span class="button-icon__arrow"></span>
        </figure>
      `;
      break;
      case 'neutro':
        htmlOutput = `
          <figure class="button-icon button-icon--neutro">
            <img src=${imgUrl} class="button-icon__image "/>
            <span class="button-icon__arrow"></span>
          </figure>
        `;
        break;
      case 'net':
        htmlOutput = `
          <figure class="button-icon button-icon--net">
            <img src=${imgUrlnet} class="button-icon__image "/>
            <div class="button-icon__tags button-icon__tags--${colorTag}">
                <div class="button-icon__link-tag">
                  Red ${button.name}
                </div>
            </div>
          </figure>
        `;
      break;
      case 'create':
        htmlOutput = `
          <figure class="button-icon button-icon--create">
            <img src=${imgCreate} class="button-icon__image "/>
          </figure>
        `;
      break;
    default:
      if (button.isChangeType) {
        if (icon == 'grey') {
          htmlOutput = `
            <figure class="button-icon button-icon--grey button-icon--neutro">
              <img src=${imgUrl} class="button-icon__image "/>
              <span class="button-icon__arrow"></span>
            </figure>
          `;
        } else {
          htmlOutput = `
            <figure class="button-icon button-icon--neutro">
              <img src=${imgUrl} class="button-icon__image "/>
              <span class="button-icon__arrow"></span>
            </figure>
          `;
        }
      } else {
        if (icon == 'grey') {
          htmlOutput = `
            <figure class="button-icon button-icon--grey button-icon--${button.buttonType}">
              <img src=${imgUrl} class="button-icon__image "/>
              <span class="button-icon__arrow"></span>
            </figure>
          `;
        } else {
          htmlOutput = `
            <figure id='buttonIcon' class="button-icon button-icon--${button.buttonType}">
              <img src=${imgUrl} class="button-icon__image "/>
              <span class="button-icon__arrow"></span>
              <div class="button-icon__tags button-icon__tags--${colorTag}">
                  <div class="button-icon__link-tag">
                    #${buttonTag}
                  </div>
              </div>
            </figure>
          `;
        }
       }
  }
  return htmlSafe(htmlOutput);
}



export default helper(buttonIcon);
