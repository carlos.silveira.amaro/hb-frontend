import JSONAPIAdapter from 'ember-data-updating-json-api-relationships/adapters/adapter';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import { inject as service } from '@ember/service';
import config from '../config/environment';

export default JSONAPIAdapter.extend(
  DataAdapterMixin,
  {
    session: service(),
    namespace: config.namespace,

    authorize(xhr) {
      const sessionData = this.get('session.data.authenticated');
      xhr.setRequestHeader('X-USER-EMAIL', `${sessionData.email}`);
      xhr.setRequestHeader('X-USER-TOKEN', `${sessionData.token}`);
    },
  }
);
