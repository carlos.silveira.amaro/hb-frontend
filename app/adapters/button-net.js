import ApplicationAdapter from './application';
import { isEmpty } from '@ember/utils';
import { set } from '@ember/object';

export default ApplicationAdapter.extend({
  urlForUpdateRecord(id, modelName, snapshot) {
    const buttonNet = snapshot.record;
    if (buttonNet.actionEvent === 'allowMe') {
      set(buttonNet, 'actionEvent', null);
      return `/api/v1/button-nets/${buttonNet.id}/allow`;
    }

    if (!isEmpty(snapshot.adapterOptions) && !isEmpty(snapshot.adapterOptions.relationshipToUpdate)) {
      return `/api/v1/button-nets/${buttonNet.id}/relationships/` + snapshot.adapterOptions.relationshipToUpdate;
    }
    return `/api/v1/button-nets/${buttonNet.id}`;
  },
});
