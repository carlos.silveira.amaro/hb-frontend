import MediaQueriesService from 'ember-cli-media-queries/services/media-queries';

export default MediaQueriesService.extend({
  media: {
    mobile: '(max-width: 575px)',
    tablet: '(min-width: 576px) and (max-width: 991px)',
    desktop: '(min-width: 992px) and (max-width: 1199px)',
    jumbo: '(min-width: 1200px)',
  },
});
