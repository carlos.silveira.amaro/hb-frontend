import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { computed, set } from '@ember/object';
import RSVP from 'rsvp';
import config from '../config/environment';
import $ from 'jquery';

export default Service.extend({
  ajax: service(),

  currentPosition: null,

  geolocationEnabled: computed(
    function() {
      return navigator.geolocation;
    }
  ),


  getCurrentPosition() {
    return new RSVP.Promise((resolve, reject)=>{
      const options = {
        enableHighAccuracy: true,
        timeout: 4000,
        maximumAge: 0,
      };
      const errorCallback = this.locationError(resolve, reject);
      navigator.geolocation.getCurrentPosition(this.locationSuccess(resolve), errorCallback, options);
    });
  },

  locationSuccess(resolve) {
    return (position) => {
      set(this, 'currentPosition', position.coords);
      resolve(position.coords);
    };
  },

  locationError(resolve, reject) {
    return ()=>{
      const { 'google-apis': {key}  } = config;

      $.ajax({
        type: 'POST',
        url: `https://www.googleapis.com/geolocation/v1/geolocate?key=${key}`,
      })
        .then((position)=>{
          const coordinates = {
            latitude: position.location.lat,
            longitude: position.location.lng,
          };
          resolve(coordinates);
        })
        .catch((reason)=>{
          const msg = 'Error: ' + reason.message + ' Code: ' + reason.code;
          const error = {
            message: msg,
          };
          reject(error);
        });
    };
  },
});
