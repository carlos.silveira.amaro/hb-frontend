import SessionService from 'ember-simple-auth/services/session';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { reject } from 'rsvp';
import { set } from '@ember/object';

export default SessionService.extend({
  store: service(),

  currentUser: computed(
    'isAuthenticated',
    {
      get() {
        return this.isAuthenticated ?
          this.store.findRecord('user', 'current')
            .then((currentUser) => {
              set(this, 'currentUser', currentUser);
            })
          : reject();
      },
      set(key, value) {
        return value;
      },
    }
  ),


});
