import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';

export default Controller.extend({
  currentUser: alias('model.currentUser'),
  avatar: alias('model.avatar'),

  actions: {
    goHome() {
      this.transitionToRoute('button-net.index.index');
    },
  },
});
