import Controller, { inject as controller } from '@ember/controller';
import { set, get, computed } from '@ember/object';


export default Controller.extend({
  button: null,
  buttonNetController: controller('button-net'),

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),

  actions: {
    nextStep() {
      this.transitionToRoute('button-net.buttons.new.date');
    },
  },
});
