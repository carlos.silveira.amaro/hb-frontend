import Controller, { inject as controller } from '@ember/controller';
import { inject as service } from '@ember/service';
import { or } from '@ember/object/computed';
import { isEmpty } from '@ember/utils'
import { resolve } from 'rsvp';
import { get } from '@ember/object'


const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 5000,
  cssClasses: 'notifications-index',
};

export default Controller.extend({
  mediaQueries: service(),
  notifications: service('notification-messages'),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  buttonNet: null,
  buttonNetController: controller('button-net'),

  init() {
    this._super(...arguments);
    const context = this;
    setTimeout(function () {
      if (get(context, 'buttonNetController.model.privacy')) {
        context.notifications.warning(
          'Esta red es privada, inicia sesión para acceder',
          NOTIFICATION_OPTIONS,
        );
      }
    }, 1000);
  },

  email: null,
  model: null,

  actions: {
    backAction() {
      window.history.back();
    },
    closeAction() {
      this.transitionToRoute('button-net.index.index');
    },
    searchByAddress(address, tag) {
      const urlParams = new URLSearchParams(window.location.search);
      let newTags = "";
      if (!isEmpty(urlParams.get('searchTags'))) {
        newTags = 'searchTags=' + urlParams.get('searchTags') + '&';
      }
      if (!isEmpty(tag)) {
        newTags += tag;
      }


      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
        searchTags: newTags,
      };
      return resolve().then(() => {
        this.transitionToRoute('button-net.button-net.index', { queryParams });
      });
    },
  },
});
