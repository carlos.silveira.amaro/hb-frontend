import Controller from '@ember/controller';
import { set, get } from '@ember/object';
import { computed } from '@ember/object'

export default Controller.extend({
  button: null,

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),

  actions: {
    nextStep() {
      this.transitionToRoute('profile.new-button.location');
    },
    closeAction() {
      this.transitionToRoute('profile');
    },
  },
});
