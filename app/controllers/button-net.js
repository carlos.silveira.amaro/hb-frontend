import Controller, { inject as controller } from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed, get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { readOnly } from '@ember/object/computed';

export default Controller.extend({
  model: null,
  session: service('session'),
  currentUser: readOnly('session.currentUser'),
  showAllowModal: false,
  indexController: controller('button-net.index'),
  showAllowModalComp: readOnly('indexController.showAllowModalComp'),

  isCurrentNetMine: computed('model', {
    get() {
      if (!isEmpty(get(this, 'model'))) {
        const context = this;
        get(this, 'model.creator').then((creator) => {
          if (get(context, 'currentUser.internalId') === Number(creator.id)) {
            set(context, 'isCurrentNetMine', true);
          } else {
            set(context, 'isCurrentNetMine', false);
          }
        })
      }
    },
    set(key, value) {
      return value;
    }
  }),

  actions: {
    toggleAllowModal() {
      set(this, 'showAllowModal', false);
      set(this, 'model.actionEvent', 'allowMe');
      get(this, 'model').save();
    }
  }
});
