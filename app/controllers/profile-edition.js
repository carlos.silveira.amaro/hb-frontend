import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed, set, get, setProperties } from '@ember/object';
import { alias, notEmpty, union } from '@ember/object/computed';
import { isEmpty } from '@ember/utils';
import { htmlSafe } from '@ember/string';
import { resolve, hash } from 'rsvp';

const languageOptions = ['Español', 'Ingles'];

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Controller.extend({
  //SERVICES
  session: service(),
  geolocation: service('geolocation'),
  store: service(),


///MODELS
  currentUser: alias('model.currentUserLet'),
  avatar: alias('model.avatar'),
  selectedInterests: alias('model.userTags'),
  selectedNetTags: alias('model.tags'),
  languages: alias('model.languages'),
  selectedLanguages: alias('model.selectedLanguages'),
  notifications: service('notification-messages'),
  blockedUsers: alias('model.blockedUsers'),
  bannedUsers: alias('model.bannedUsers'),

  sliderValue: computed('currentUser.tagsDistance', function() {
    switch (get(this, 'currentUser.tagsDistance')) {
       case 1:
         return 0;
       case 2:
         return 1;
       case 3:
         return 2;
       case 4:
         return 3;
       case 5:
         return 4;
       case 10:
         return 5;
       case 20:
         return 6;
       case 30:
         return 7;
       case 50:
         return 8;
       case 100:
         return 9;
       case 200:
         return 10;
       case 500:
         return 11;
       case 1000:
         return 12;
       case 2000:
         return 13;
       case 3000:
         return 14;
      case 10000:
         return 15;
       default:
          return 0;
     }
  }),

  interestsSaved: true,
  changedBlocked: true,

  lastName: '',
  lastUrlname: '',
  lastDesc: '',
  lastUrl: '',

  changedNet: computed('selectedNet.name',
  'selectedNet.urlName',
  'selectedNet.description',
  'selectedNet.netUrl',
  function() {
    if (get(this, 'selectedNet.name') !== get(this, 'lastName') || get(this, 'selectedNet.urlName') !== get(this, 'lastUrlname') || get(this, 'selectedNet.description') !== get(this, 'lastDesc') || get(this, 'selectedNet.netUrl') !== get(this, 'lastUrl')) {
      set(this, 'selectedNet.hasDirtyAttributes', true);
      return true;
    } else {
      set(this, 'selectedNet.hasDirtyAttributes', false);
      return false;
    }
  }),

  changedInterests: computed('interestsSaved',
    'selectedInterests',
    'currentUser.userTags',
    'currentUser.userTags.[].hasDirtyAttributes',
    'currentUser.userTags.@each.hasDirtyAttributes',
    'currentUser.userTags.[].buttonNets.length',
    'currentUser.userTags.@each.buttonNets.length',
    function() {
      if (get(this, 'interestsSaved')) {
        return false;
      }
      return true;
  }),

  languagesSaved: true,
  changedLanguages: computed('languagesSaved', 'selectedLanguages', 'languages', function() {
    if (this.languagesSaved) {
      set(this, 'languagesSaved', false);
      return false;
    }
    return true;
  }),

  userButtonNetsAdmin: computed(
    'currentUser.internalId',
    {
      get() {
        get(this, 'store').query('buttonNet', { 'filter[admin]': true }).then((buttonNets) => {
          set(this, 'userButtonNetsAdmin', buttonNets);
        });
      },
      set(key, value) { return value; },
    }
  ),

  userButtonNetsUser: computed(
    'currentUser.internalId',
    {
      get() {
        get(this, 'store').query('buttonNet', { 'creator_id': this.currentUser.internalId }).then((buttonNets) => {
          set(this, 'userButtonNetsAdmin', buttonNets);
        });
        // get(this, 'currentUser.buttonNets').then((buttonNets) => {
        //   set(this, 'userButtonNetsUser', buttonNets);
        // });
      },
      set(key, value) { return value; },
    }
  ),

  isUserCreator: computed(
    'currentUser.internalId',
    'selectedNet.creatorId',
    function()
    {
      if(this.selectedNet != null && this.currentUser.internalId == this.selectedNet.creatorId)
      {
        // console.log(this.currentUser.internalId +''+ this.selectedNet.creatorId);
        return true;
      } else {
        // console.log(this.currentUser.internalId +''+ this.selectedNet.creatorId);
        return false;
      }

    }),

  userButtonNets: union('userButtonNetsUser', 'userButtonNetsAdmin'),


  ///Buttonnets pending confirrmation from superadmin
    inactiveButtonNets: computed(
      {
        get() {
          this.store.query('buttonNet', {})
            .then((buttonNet) => {
              set(this, 'inactiveButtonNets', buttonNet);
            });
          return null;
        },
        set(key, value) { return value; },
      }
    ),


  isShowingModal: false,
  hideCreateNet:true,

  showProfileEdition: false,
  showConversationEdition: false,
  showPasswordEdition: false,
  showNetsEdition: false,
  showInterestEdition: false,
  showSecurityAndPrivacity: false,
  showNotificationsSettings: false,
  showNetsModeration: false,
  showUserModeration:false,
  showInterest:false,

  showNetExample: true,
  showIconSelector: false,

  icons: ['1_terrestre_one', '2_tren_Artboard','3_maritim', '4_air', '5_non_motor', '6_tics_electronic_tech_&software', '7_games_table_all','8_home_and_garden_help', '9_objetos','10_cook2', '11_plastic_art', '12_compras', '13_crafts', '14_peluc_estectic','15_party', '16_meetings','17_drink3', '18_teach_ADVICE', '19_investigacion', '20_religion5', '21_general','22_chat78', '23_elder8','24_emergency', '25_addiction', '37_police_jail', '26_politics', '27_sustain_recicle', '28_cooperacion', '29_human_rights','30_enfado_denuncia', '31_nature90', '32_woman', '33_man', '34_minorias_inmigrac','35_paral', '36_shelter', '38_animal34', '39_plants','40_love3', '41_sanidad_farmacia','42_Babe_child', '43_construccion_bricolage_reformas', '44_agric_industria', '45_cleaning', '47_energy','46_peligro_danger','48_guide2', '50_storage', '37_police_jail', '38_animal34', '51_traffic2','52_depresion', '53_problemas_economicos','54_defunciones', '55_otras_causas', '56_security', '57_lies_fraud', '39_plants', ],


  distAvisos : 0,
  isTagAlertOn : true,

  languageOptions: languageOptions,

  updateNetDisabled: computed('selectedNet.hasDirtyAttributes','changedNet', function() {
    return !get(this, 'selectedNet.hasDirtyAttributes') || !get(this, 'changedNet');
  }),

  changeProperties: false,
  updateUserDisabled: computed(
    'currentUser.avatarFile',
    'changedInterests',
    'changedLanguages',
    'currentUser.hasDirtyAttributes',
    'changeProperties',
    function() {
      get(this, 'changeProperties');
      return (!this.currentUser.hasDirtyAttributes)
      && isEmpty(this.currentUser.avatarFile)
      && !this.changedInterests
      && !this.changedLanguages
      && !this.changeProperties;
    }
  ),

  changePasswordDisabled: computed(
    'currentUser.actualPassword',
    'currentUser.newPassword',
    'currentUser.passwordConfirmation',
    function() {
      return isEmpty(this.currentUser.currentPassword) ||
        isEmpty(this.currentUser.password) ||
        isEmpty(this.currentUser.passwordConfirmation);
    }
  ),

  WhatsappDisabled: computed(
    'currentUser.useWhatsapp',
    'currentUser.phone',
    function() {
      return isEmpty(this.currentUser.phone);
    }
  ),

  TelegramDisabled: computed(
    'currentUser.useTelegram',
    'currentUser.userTelegram',
    function() {
      return isEmpty(this.currentUser.userTelegram);
    }
  ),

  hasPendingAvatar: notEmpty('currentUser.avatarFile'),
  selectedNet: null,

  isSharingLocation: computed('currentUser', function() {
    return !isEmpty(this.currentUser.location);
    return true;
  }),

  useExternalConv: computed('currentUser', function() {
    if(!this.currentUser.useWhatsapp && !this.currentUser.useTelegram)
    {
        return false;
    }
    else{
      return true;
    }
  }),

  trendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'trendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  usersNotEmpty: notEmpty('users'),

  changedUsersToggle: false,
  trendingLanguages: computed(
    {
      get() {
        this.store.query('language', {})
          .then((trendingLanguages) => {
            set(this, 'trendingLanguages', trendingLanguages);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  users: computed(
    'currentUser.blocked',
    'currentUser.blocked.length',
    'currentUser.blocked.[]',
    'currentUser.blocked.@each',
    'changedUsersToggle',
    {
      get() {
        const array = [];
        if (!isEmpty(get(this, 'currentUser.blocked'))) {
          for (let i = 0; i < get(this, 'currentUser.blocked').length; i++) {
            if (!isEmpty(get(this, 'currentUser.blocked')[i])) {
              this.store.find('user', get(this, 'currentUser.blocked')[i])
                .then((users) => {
                  if (!isEmpty(users)) {
                    array.pushObject(users);
                    set(this, 'users', array);
                  }
                });
            }
          }
        }
      },
      set(key, value) { return value; },
    }
  ),

  selectedNetblockedUsers: computed(
    'selectedNet.blockedUsers',
    'selectedNet.blockedUsers.length',
    'selectedNet.blockedUsers.[]',
    'selectedNet.blockedUsers.@each',
    'changedSelectedNetUsersToggle',
    {
      get() {
        const array = [];
        for (let i = 0; i < get(this, 'selectedNet.blockedUsers').length; i++) {
          if (!isEmpty(get(this, 'selectedNet.blockedUsers')[i])) {
            this.store.find('user', get(this, 'selectedNet.blockedUsers')[i])
              .then((users) => {
                if (!isEmpty(users)) {
                  array.pushObject(users);
                  set(this, 'selectedNetblockedUsers', array);
                }
              });
          }
        }
      },
      set(key, value) { return value; },
    }
  ),

  selectedNetAdminUsers: computed(
    'selectedNet.adminUsers',
    'selectedNet.adminUsers.length',
    'selectedNet.adminUsers.[]',
    'selectedNet.adminUsers.@each',
    'changedSelectedNetUsersToggle',
    {
      get() {
        const array = [];
        for (let i = 0; i < get(this, 'selectedNet.adminUsers').length; i++) {
          if (!isEmpty(get(this, 'selectedNet.adminUsers')[i])) {
            this.store.find('user', get(this, 'selectedNet.adminUsers')[i])
              .then((users) => {
                if (!isEmpty(users)) {
                  array.pushObject(users);
                  set(this, 'selectedNetAdminUsers', array);
                }
              });
          }
        }
      },
      set(key, value) { return value; },
    }
  ),

  selectedNetacceptedUsers: computed(
    'selectedNet.allowedUsers',
    'selectedNet.allowedUsers.length',
    'selectedNet.allowedUsers.[]',
    'selectedNet.allowedUsers.@each',
    'changedSelectedNetUsersToggle',
    {
      get() {
        const array = [];
        for (var i = 0; i < get(this, 'selectedNet.allowedUsers').length; i++) {
          if (!isEmpty(get(this, 'selectedNet.allowedUsers')[i])) {
            this.store.find('user', get(this, 'selectedNet.allowedUsers')[i])
              .then((users) => {
                if (!isEmpty(users)) {
                  array.pushObject(users);
                  set(this, 'selectedNetacceptedUsers', array);
                }
              });
          }
        }
      },
      set(key, value) { return value; },
    }
  ),

  selectedAdminUsers: computed(
    'selectedNet.adminUsers',
    'selectedNet.adminUsers.length',
    'selectedNet.adminUsers.[]',
    'selectedNet.adminUsers.@each',
    'changedSelectedNetUsersToggle',
    {
      get() {
        const array = [];
        for (var i = 0; i < get(this, 'selectedNet.adminUsers').length; i++) {
          if (!isEmpty(get(this, 'selectedNet.adminUsers')[i])) {
            this.store.find('user', get(this, 'selectedNet.adminUsers')[i])
              .then((users) => {
                if (!isEmpty(users)) {
                  array.pushObject(users);
                  set(this, 'selectedNetacceptedUsers', array);
                }
              });
          }
        }
      },
      set(key, value) { return value; },
    }
  ),


  hasBounds: notEmpty('mapBounds'),

  searchAddressBounds: null,

  mapBounds: computed(
    'geolocation',
    'searchAddressBounds',
    'currentUser.tagsLatitude',
    'currentUser.tagsLongitude',
    {
      get() {
        if (!isEmpty(get(this, 'currentUser.tagsLatitude'))) {
          set(this, 'mapBounds', L.latLng(get(this, 'currentUser.tagsLatitude'), get(this, 'currentUser.tagsLongitude')).toBounds(500));
          set(this, 'draggableMarkerLocation', L.latLng(get(this, 'currentUser.tagsLatitude'), get(this, 'currentUser.tagsLongitude')));
          return L.latLng(get(this, 'currentUser.tagsLatitude'), get(this, 'currentUser.tagsLongitude')).toBounds(500);
        } else {
          if (isEmpty(this.searchAddressBounds)) {
            get(this, 'geolocation')
              .getCurrentPosition()
              .then((coordinates) => {
                set(this, 'mapBounds', L.latLng(coordinates.latitude, coordinates.longitude).toBounds(500));
                set(this, 'draggableMarkerLocation', L.latLng(coordinates.latitude, coordinates.longitude));
              });
          } else {
            return this.searchAddressBounds;
          }
        }
      },
      set(key, value) {
        return value;
      },
  }),

  deleteModal: false,
  showMap: false,
  draggableMarkerLocation: null,

  actions: {
    nameKeyUp(param) {
      set(this, 'selectedNet.urlName', param.replace(/ /g,"_"));
    },
    onMapLoad() {
      const center = this.mapBounds.getCenter();
      setProperties(this, {
        'currentUser.latitude': center.lat,
        'currentUser.longitude': center.lng,
        'draggableMarkerLocation': L.latLng(center.lat, center.lng),
      });
    },
    onDragEnd(marker) {
      setProperties(this, {
        'currentUser.latitude': marker.target._latlng.lat,
        'currentUser.longitude': marker.target._latlng.lng,
        'draggableMarkerLocation': L.latLng(marker.target._latlng.lat, marker.target._latlng.lng),
      });
    },
    placeChangedCallback(params) {
      if (!isEmpty(params.geometry)) {
        const viewport = params.geometry.viewport;
        const northEastCorner = L.latLng(viewport.getNorthEast().lat(), viewport.getNorthEast().lng());
        const southWestCorner = L.latLng(viewport.getSouthWest().lat(), viewport.getSouthWest().lng());
        setProperties(this, {
          'searchAddressBounds': L.latLngBounds(northEastCorner, southWestCorner),
          'searchZoom': true,
          'isSearchingAddress': true,
          'currentUser.tagsLatitude': params.geometry.location.lat(),
          'currentUser.tagsLongitude': params.geometry.location.lng(),
          'changeProperties': true,
          'draggableMarkerLocation': L.latLng(params.geometry.location.lat(), params.geometry.location.lng()),
        });
      }
    },
    acceptLocationTag() {
      set(this, 'currentUser.tagsLatitude', get(this,'draggableMarkerLocation.lat'));
      set(this, 'currentUser.tagsLongitude', get(this, 'draggableMarkerLocation.lng'));
      set(this, 'showMap', false);
    },
    showMap() {
      this.toggleProperty('showMap');
    },
    deleteProfile() {
      get(this, 'currentUser').destroyRecord().then(() => {
        get(this, 'session').invalidate();
      });
    },

    searchByAddress(address, tags) {
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
        searchTags: tags,
      };
      this.transitionToRoute('index.index', { queryParams });
    },

    acceptLocation(address) {
      set(this, 'selectedNet.latitude', address.geometry.location.lat());
      set(this, 'selectedNet.longitude', address.geometry.location.lng());
      set(this, 'selectedNet.locationName', address.formatted_address);
      set(this, 'selectedNet.hasDirtyAttributes', true);
    },


    toggleProperty(param) {
      if (param === 'showSecurityAndPrivacity' || param === 'showNotificationsSettings') {
        window.alert('Funcionalidad en desarrollo');
      } else {
        this.toggleProperty(param);
      }
    },

    updateUserProfile() {
      set(this, 'currentUser.userTags', this.selectedInterests);
      set(this, 'currentUser.languages', this.selectedLanguages);

      isEmpty(this.selectedLanguages) ? resolve() : this.currentUser.updateRelationship('languages');
      return get(this, 'currentUser').save().then((user) => {
        set(this, 'currentUser.hasDirtyAttributes', false);
        set(this, 'changeProperties', false);
        set(this, 'changedInterests', false);
        set(this, 'interestsSaved', false);
        let image = resolve();
        if (!isEmpty(user.avatarFile)) {
          const options = {};
          options.headers = {
            'Authorization': this.session.data.authenticated.token,
          };
          options.data = {
            name: user.avatarFile.name,
            file: user.avatarFile,
            'imageable-id': user.internalId,
            'imageable-type': 'user',
            'imageable-property': 'avatar',
          };
          image = user.avatarFile.upload('/api/v1/images', options)
            .then((data) => {
              const normalizedImageResponse = this.store.normalize('image', data.body.data);
              const newImage = this.store.push(normalizedImageResponse);
              setProperties(user, {
                'avatar': newImage,
                'avatarFile': null,
              });
              return newImage;
            });
        }
        get(user, 'userTags').then((interestsResp) => {
          const promiseArray = [];
          for (let i = 0; i < interestsResp.length; i++) {
            if (interestsResp.objectAt(i).hasDirtyAttributes) {
              promiseArray.push(interestsResp.objectAt(i).save());
            }
          }
          Promise.all(promiseArray).then(values => {
            set(this, 'interestsSaved', true);
          });
        })
        set(this, 'languagesSaved', true);
        return hash({
          image,
        });
      });
    },

    toggleShowPhone() {
      this.toggleProperty('currentUser.showPhone');
    },

    determineUserConvers() {

    },

    toggleUseWhatsapp() {
      this.toggleProperty('currentUser.useWhatsapp');
      if(this.currentUser.phone==null) {
        set(this, 'currentUser.useWhatsapp', false);
      }
      set(this, 'currentUser.useExternalConv', true);
      if(!this.currentUser.useWhatsapp && !this.currentUser.useTelegram) {
        set(this, 'currentUser.useExternalConv', false);
      }
    },

    toggleUseTelegram() {
      this.toggleProperty('currentUser.useTelegram');
      set(this, 'currentUser.useExternalConv', true);
      if(!this.currentUser.useWhatsapp && !this.currentUser.useTelegram) {
        set(this, 'currentUser.useExternalConv', false);
      }
    },

    searchTags(term) {
      return this.store.query('tag', { 'filter[name]': term })
        .then((tags) => {
          return tags.filter((tag) => !this.selectedInterests.includes(tag));
        });
    },

    searchNetTags(term) {
      return this.store.query('tag', { 'filter[name]': term })
        .then((tags) => {
          return tags.filter((tag) => !this.selectedNetTags.includes(tag));
        });
    },

    searchLanguages(term) {
      return this.store.query('language', { 'filter[name]': term })
        .then((language) => {
          return language.filter((language) => !this.selectedLanguages.includes(language));
        });
    },

    searchUsers(term) {
      return this.store.query('user', { 'filter[name]': term })
        .then((users) => {
          return users.filter((user) => !this.blockedUsers.includes(user));
        });
    },
    searchLanguages(term) {
      return this.store.query('language', { 'filter[name]': term })
        .then((languages) => {
          return languages.filter((lang) => !this.selectedLanguages.includes(lang));
        });
    },
    changePassword() {
      set(this, 'currentUser.actionEvent', 'updatePassword');
      this.currentUser.save().then(() => {
        this.notifications.success(
          'Contraseña cambiada.',
          NOTIFICATION_OPTIONS,
        );
      })
      .catch(() => {
        this.notifications.error(
          'Caracteres no válidos, comprueba que son iguales y 6 o más caracteres.',
          NOTIFICATION_OPTIONS,
        );
      });
    },
    shareLocation(isSharingLocation) {
      this.toggleProperty(isSharingLocation);
      if (this.isSharingLocation) {
        set(this, 'showSpinner1', true);
        this.geolocation
          .getCurrentPosition()
          .then((coordinates) => {
            const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
            coordinates.latitude + ',' + coordinates.longitude + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
            $.getJSON(geocodingAPI).then((json) => {
              if (json.status === 'OK') {
                const result = json.results[0];
                let state = '';
                for (let i = 0, len = result.address_components.length; i < len; i++) {
                  const ac = result.address_components[i];
                  if (ac.types.indexOf('administrative_area_level_1') >= 0) state = ac.short_name;
                }
                if (state !== '') {
                  set(this, 'currentUser.location', state);
                  set(this, 'showSpinner1', false);
                }
              }
            });
          });
      } else {
        set(this, 'currentUser.location', null);
      }
    },
    uploadImage(file) {
      set(this, 'currentUser.avatarFile', file);
      file.readAsDataURL().then(function(url) {
        file.set('url', url);
      });
    },
    updateButtonNet(buttonNet) {
      set(this, 'lastName', buttonNet.name);
      set(this, 'lastUrlname', buttonNet.urlName);
      set(this, 'lastDesc', buttonNet.description);
      set(this, 'lastUrl', buttonNet.netUrl);
      buttonNet.save().then(() => {
        set(this, 'selectedNet.hasDirtyAttributes', false);
      });
    },
    updateBlockedUsers() {
      set(this, 'changedBlocked', true);
      set(this, 'currentUser.actionEvent', 'unblock');
      get(this, 'currentUser').save();
    },
    selectNet(net) {
      if (get(this, 'selectedNet') === net) {
        set(this, 'selectedNet', null);
      } else {
        set(this, 'lastName', net.name);
        set(this, 'lastUrlname', net.urlName);
        set(this, 'lastDesc', net.description);
        set(this, 'lastUrl', net.netUrl);
        set(this, 'selectedNet', net);
        // set(this, 'selectedNet.hasDirtyAttributes', false);
      }
    },

    createNewTag(targetRelationship, tagName) {
      if (tagName.includes('##')) {
        tagName = tagName.replace('#', '');

      }
      if (tagName.includes('#')) {
        tagName = tagName.replace('#', '');
      }
      tagName=tagName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      tagName=tagName.toLowerCase();

      return get(this, 'store').createRecord('tag', {
        name: tagName,
      })
        .save()
        .then((tag) => {
          get(this, 'store').createRecord('userTag', {
            name: tagName,
            user: this.currentUser,
          })
          .save()
          .then((userTag) => {
            get(this, targetRelationship).pushObject(userTag);
            set(this, 'selectedNet.hasDirtyAttributes', true);
          })
        });
    },

    createNewNetTag(targetRelationship, tagName) {
      if (tagName.includes('##')) {
        tagName = tagName.replace('#', '');

      }
      if (tagName.includes('#')) {
        tagName = tagName.replace('#', '');
      }
      tagName=tagName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      tagName=tagName.toLowerCase();

      return get(this, 'store').createRecord('tag', {
        name: tagName,
      })
        .save()
          .then((tag) => {
            get(this, targetRelationship).pushObject(tag);
            set(this, 'selectedNet.hasDirtyAttributes', true);
          });
    },

    createNewLanguage(targetRelationship, tagName) {
      if (tagName.includes('##')) {
        tagName = tagName.replace('#', '');

      }
      if (tagName.includes('#')) {
        tagName = tagName.replace('#', '');
      }
      tagName=tagName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      tagName=tagName.toLowerCase();

      return get(this, 'store').createRecord('language', {
        name: tagName,
      })
        .save()
        .then((language) => {
          get(this, targetRelationship).pushObject(language);
        });
    },

    checkTagAlreadyExists(term) {
      const tags = this.tags;
      return !(!isEmpty(tags) && !isEmpty(tags.findBy('name', term)));
    },
    checkLanguageAlreadyExists(term) {
      const languages = this.languages;
      return !(!isEmpty(languages) && !isEmpty(languages.findBy('name', term)));
    },
    tagSuggestion(term) {
      return `Pulsa intro o aquí para crear ${term}`;
    },
    languageSuggestion(term) {
      return `Pulsa intro o aquí para crear ${term}`;
    },
    removeTag(targetRelationship, tag) {
      set(this,'changedInterests',true);
      const target = get(this, targetRelationship);
      target.removeObject(tag);
      tag.deleteRecord();
    },
    handleKeyDown(select, e) {
      if (e.keyCode === 8 && e.target.value === '' && select.selected.length > 0) {
        select.actions.select(select.selected.slice(-1));
        return false;
      }
    },
    sliderMoved(event) {
      set(this,'distAvisos', event.target.value); // the slider's value
    },
    sendTagAlert(){
      this.toggleProperty('currentUser.deliverInterests')
      set(this,'changedInterests',true);
    },
    checkUserAlreadyExists(term) {
      const users = this.users;
      return !(!isEmpty(users) && !isEmpty(users.findBy('name', term)));
    },
    removeBlockedUser(user) {
      const currentUser = get(this, 'currentUser');
      get(currentUser, 'blocked').splice(get(currentUser, 'blocked').indexOf(Number(user.id)), 1);
      this.toggleProperty('changedUsersToggle');
      set(this, 'changedBlocked', false);
    },
    removeSelectedNetBlockedUser(user) {
      const selectedNet = get(this, 'selectedNet');
      get(selectedNet, 'blockedUsers').splice(get(selectedNet, 'blockedUsers').indexOf(Number(user.id)), 1);
      this.toggleProperty('changedSelectedNetUsersToggle');
      set(this, 'selectedNet.hasDirtyAttributes', true);
    },
    removeSelectedNetAdminUser(user) {
      const selectedNet = get(this, 'selectedNet');
      get(selectedNet, 'adminUsers').splice(get(selectedNet, 'adminUsers').indexOf(Number(user.id)), 1);
      this.toggleProperty('changedSelectedNetUsersToggle');
      set(this, 'selectedNet.hasDirtyAttributes', true);
    },
    removeSelectedNetAllowedUser(user) {
      const selectedNet = get(this, 'selectedNet');
      get(selectedNet, 'allowedUsers').splice(get(selectedNet, 'allowedUsers').indexOf(Number(user.id)), 1);
      this.toggleProperty('changedSelectedNetUsersToggle');
      set(this, 'selectedNet.hasDirtyAttributes', true);
    },
    removeUser(targetRelationship, user) {
      const target = get(this, targetRelationship);
      target.removeObject(user);
    },
    sliderUpdate(sliderValue) {
      switch (sliderValue) {
         case "0":
           set(this, 'currentUser.tagsDistance', 1);
           break;
         case "1":
           set(this, 'currentUser.tagsDistance', 2);
           break;
         case "2":
           set(this, 'currentUser.tagsDistance', 3);
           break;
         case "3":
           set(this, 'currentUser.tagsDistance', 4);
           break;
         case "4":
           set(this, 'currentUser.tagsDistance', 5);
           break;
         case "5":
           set(this, 'currentUser.tagsDistance', 10);
           break;
         case "6":
           set(this, 'currentUser.tagsDistance', 20);
           break;
         case "7":
           set(this, 'currentUser.tagsDistance', 30);
           break;
         case "8":
           set(this, 'currentUser.tagsDistance', 50);
           break;
         case "9":
           set(this, 'currentUser.tagsDistance', 100);
           break;
         case "10":
           set(this, 'currentUser.tagsDistance', 200);
           break;
         case "11":
           set(this, 'currentUser.tagsDistance', 500);
           break;
         case "12":
           set(this, 'currentUser.tagsDistance', 1000);
           break;
         case "13":
           set(this, 'currentUser.tagsDistance', 2000);
           break;
         case "14":
           set(this, 'currentUser.tagsDistance', 3000);
           break;
        case "15":
           set(this, 'currentUser.tagsDistance', 10000);
           break;
        default:
          set(this, 'currentUser.tagsDistance', 0);
          break;
       }
       set(this, 'currentUser.hasDirtyAttributes', true);
    },
    changeInterests(param) {
      if (param.length > get(this,'selectedInterests.length')) {
        const newUserTag = this.store.createRecord('userTag');
        set(newUserTag, 'tag', param[param.length-1]);
        set(newUserTag, 'user', get(this, 'currentUser'));
        set(this,'changedInterests',true);
        newUserTag.save().then((tagResp) => {
          get(this, 'currentUser.userTags').pushObject(tagResp);
        });
      } else {
          set(this,'changedInterests',true);
          const d = get(this, 'currentUser.userTags').toArray();
          if (d[d.length-1] !== undefined) {
            d[d.length-1].deleteRecord();
            d.removeObject(d[d.length-1]);
            set(this, 'currentUser.userTags', d);
          }
      }
    },

    changeNetTags(param) {
      const selectedNet = get(this, 'selectedNet');
      set(selectedNet, 'tags', param);
      set(this, 'selectedNet.hasDirtyAttributes', true);
    },

    createNewLanguage(targetRelationship, tagName) {
      if (tagName.includes('##')) {
        tagName = tagName.replace('#', '');

      }
      if (tagName.includes('#')) {
        tagName = tagName.replace('#', '');
      }
      tagName=tagName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      tagName=tagName.toLowerCase();

      return get(this, 'store').createRecord('language', {
        name: tagName,
      })
        .save()
        .then((language) => {
          get(this, targetRelationship).pushObject(language);
        });
    },
    toggleAttrAction(tag, type) {
      tag.toggleProperty(type)

      if (tag.needType && tag.offerType) {
        set(tag,'tagType',2);
      } else {
        if (tag.needType) {
          set(tag,'tagType',1);
        }
        if (tag.offerType) {
          set(tag,'tagType',0);
        }
        if (!tag.needType && !tag.offerType) {
          set(tag,'tagType',3);
        }
      }
      set(this, 'currentUser.hasDirtyAttributes', true);
    },

    addIconToNet(param) {
      const selectedNet = get(this, 'selectedNet');
      set(selectedNet, 'imgUrl', param);
      set(this, 'selectedNet.hasDirtyAttributes', true);
    },

    setNetAdmin() {
      const userm=this.get('email');
      get(this, 'store').queryRecord('user', { 'email': userm, 'actionEvent': 'net_admin', 'net_id': get(this, 'selectedNet.id')}).then((user) => {
        set(this, 'selectedNet.adminUsers', user);
        set(this, 'button.actionEvent', 'button-transfered');
        get(this, 'selectedNet').save()
          .then(() => {
            this.notifications.success(
              'Añadido nuevo admin a tu red',
              NOTIFICATION_OPTIONS,
            );
          })
          .catch(() => {
            this.notifications.error(
              'Error al añadir admin',
              NOTIFICATION_OPTIONS,
            );
          });
      })
      .catch((error) => {
        if (error === 'User not found') {
          this.notifications.info(
            'Este email no tiene cuenta',
            NOTIFICATION_OPTIONS,
          );
        }
      });
    },

    acceptNet(net) {
      set(net,'active',true);
      net.save();
    },

    toggleShowIcons() {
      this.toggleProperty('showIconSelector');
    },
  },
});
