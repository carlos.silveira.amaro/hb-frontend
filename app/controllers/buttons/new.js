import Controller from '@ember/controller';
import { readOnly, equal } from '@ember/object/computed';
import { inject as service } from '@ember/service';

export default Controller.extend({
  routing: service('-routing'),

  button: readOnly('model'),

  activeButtonRouteClass: equal('routing.currentRouteName', 'buttons.new.activate-button'),

  actions: {
    closeAction() {
      this.transitionToRoute('index.index');
    },
  },
});
