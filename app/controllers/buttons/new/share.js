import Controller from '@ember/controller';
import { get } from '@ember/object'


export default Controller.extend({

  socialShareTransition: null,
  socialShareURL: null,

  actions: {
    finishCreation() {
      this.transitionToRoute('index.index', { queryParams: { hideWelcome: true, lat: get(this, 'model.latitude'), lng: get(this, 'model.longitude'),
      nel: null, nelng: null, swl: null, swlng: null}});
    },
    socialShareTransition() {
      switch (this.socialShareNetwork) {
        case 'facebook':
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
          break;
        case 'twitter':
          window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
          break;
        case 'linkedin':
          window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
          break;
        case 'whatsapp':
          window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
          break;
        default:
      }
    },
  },
});
