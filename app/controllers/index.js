import Controller, { inject as controller } from '@ember/controller';
import { inject as service } from '@ember/service';
import { or, alias } from '@ember/object/computed';
import { computed, get, set } from '@ember/object';
import { resolve } from 'rsvp';
import { htmlSafe } from '@ember/string';
import { isEmpty } from '@ember/utils';

import ButtonsFiltersMixin from '../mixins/buttons-filters-mixin';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Controller.extend(
  ButtonsFiltersMixin,
  {
    // SERVICES
    mediaQueries: service(),
    routing: service('-routing'),
    geolocation: service('geolocation'),
    session: service(),
    notifications: service('notification-messages'),


    // QUERY PARAMS
    queryParams: [
      'latitude',
      'longitude',
      'northEastLat',
      'northEastLng',
      'southWestLat',
      'southWestLng',
      'address',
      'hideWelcome',
      'userMarker',
      'buttonsDisplay',
      'searchTags',
      'needFilter',
      'offerFilter',
    ],

    /**
     * query param, used to performs searchs by point
     * @type {Number}
     * @property latitude & longitude
     */
    latitude: null,
    longitude: null,
    /**
     * query param, used to performs searchs by area
     * @type {Number}
     * @property northEastLat & northEastLng & southWestLat & southWestLng
     */
    northEastLat: null,
    northEastLng: null,
    southWestLat: null,
    southWestLng: null,
    address: null,
    userMarker: false,
    buttonsDisplay: null,
    searchTags: null,
    searchTrendingTags: null,
    searchZoneAddress:null,
    zoomMax: false,
    lastZoom: null,
    reduceZoomNeed: false,
    buttonNet: null,

    hideWelcome: computed(
      'isAuthenticated',
      {
        get() {
          if (get(this, 'isAuthenticated')) {
              set(this, 'hideWelcome', true);
          }
        },
        set(key, value) {
          return value;
        },
      }
    ),

    needFilter: false,
    offerFilter:false,

    // COMPUTED PROPERTIES
    isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

    isShowingMenuModal: false,
    contextTransition: null,
    showDatePicker:false,

    isShowingModal: false,

    /**
     * Contains all buttons without filters applied yet, this CP
     * is used from buttons-filters component and ButtonsFiltersMixin
     * @type {Array}
     */
    buttons: alias('model.buttons'),

    currentUser: alias('model.currentUser'),

    userPosition: null,

    lastUserLat: null,
    lastUserLng: null,

    isAuthenticated: computed(
      'session.isAuthenticated',
      function() {
        return !get(this, 'session.isAuthenticated');
      }
    ),

    isMapRoute: computed(
      'routing.router.currentURL',
      function() {
        return !get(this, 'routing.router.currentURL').includes('buttonsDisplay=list');
      }
    ),

    modalBackground: computed('isAuthenticated', function() {
      if (this.isAuthenticated) {
        return 'component-header__container-modal';
      }
      return 'p-2 bg-primary h-100 w-100';
    }),

    showOnlyCreate: computed(
      'isSmallDevice',
      'hideWelcome',
      function() {
        if (get(this, 'hideWelcome') === 'false') {
          return get(this, 'isSmallDevice');
        }
        return false;
      }
    ),

    asideStyles: computed(
      'selectedTags.[]',
      'buttonsDisplay',
      function() {
        get(this, 'tagsToApply');
        if (this.buttonsDisplay === 'list') {
          return '';
        }
        const headerHeight = document.getElementsByClassName('component-header')[0].clientHeight;
        const searchTopBarHeight = document.getElementsByClassName('search-topbar')[0].clientHeight;
        const filtersHeight = document.getElementById('buttonsFiltersId').clientHeight;
        const headerSectionHeight = headerHeight + searchTopBarHeight + filtersHeight + 70;
        const asideStyles = `height: ${window.innerHeight - headerSectionHeight}px;`;
        return htmlSafe(asideStyles);
      }
    ),

    asideBodyStyles: computed(
      'selectedTags.[]',
      'buttonsDisplay',
      function() {
        get(this, 'tagsToApply');
        if (this.buttonsDisplay === 'map') {
          return '';
        }
        const headerHeight = document.getElementsByClassName('component-header')[0].clientHeight;
        const searchTopBarHeight = document.getElementsByClassName('search-topbar')[0].clientHeight;
        const filtersHeight = document.getElementById('buttonsFiltersId').clientHeight;
        const headerSectionHeight = headerHeight + searchTopBarHeight + filtersHeight ;
        const asideStyles = `padding-top: ${headerSectionHeight-60}px;`;
        return htmlSafe(asideStyles);
      }
    ),

    leafletMapConst: null,

    loading: false,
    tagFiltered: true,

    actions: {
      reduceZoom(e) {
        if(!this.isSmallDevice){
        get(this, 'leafletMapConst').target.zoomOut(2);
        }
        else {
          get(this, 'leafletMapConst').target.setZoom(5);
        }

      },
      searchByAddress(address) {
        const urlParams = new URLSearchParams(window.location.search);
        let newTags = null;
        if (!isEmpty(urlParams.get('searchTags'))) {
          newTags = 'searchTags=' + urlParams.get('searchTags') + '&';
        }

        const queryParams = {
          latitude: null,
          longitude: null,
          address,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: null,
          searchTags: newTags,
          hideWelcome: true,
        };
        return resolve().then(() => {
          this.transitionToRoute('index', { queryParams });
        });
      },

      localizeUser() {
        if(get(this, 'userMarker')) {
          const queryParams = {
            latitude: get(this, 'userPosition.lat'),
            longitude: get(this, 'userPosition.lng'),
            address: null,
            northEastLat: null,
            northEastLng: null,
            southWestLat: null,
            southWestLng: null,
            userMarker: false,
          };
          this.transitionToRoute('index', { queryParams });
          return null;
        }
        const geolocation = get(this, 'geolocation');
        return geolocation.getCurrentPosition()
          .then((coordinates) => {
            const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);

            if (isEmpty(get(this, 'lastUserLat')) || isEmpty(get(this, 'lastUserLng'))) {
              set(this, 'lastUserLat', userPosition.lat);
              set(this, 'lastUserLng', userPosition.lng);
              const queryParams = {
                latitude: userPosition.lat,
                longitude: userPosition.lng,
                address: null,
                northEastLat: null,
                northEastLng: null,
                southWestLat: null,
                southWestLng: null,
                userMarker: true,
              };
              this.transitionToRoute('index', { queryParams });
            } else {
              set(this, 'userPosition', userPosition);
              const maxPosLat = userPosition.lat > get(this, 'lastUserLat') ?
                userPosition.lat : get(this, 'lastUserLat');
              const minPosLat = userPosition.lat > get(this, 'lastUserLat') ?
                get(this, 'lastUserLat') : userPosition.lat;

              const maxPosLng = userPosition.lng > get(this, 'lastUserLng') ?
                userPosition.lng : get(this, 'lastUserLng');
              const minPosLng = userPosition.lng > get(this, 'lastUserLng') ?
                get(this, 'lastUserLng') : userPosition.lng;
              if ((maxPosLat - minPosLat > 0.0005) || (maxPosLng - minPosLng > 0.0005)) {
                set(this, 'lastUserLat', userPosition.lat);
                set(this, 'lastUserLng', userPosition.lng);
                const queryParams = {
                  latitude: get(this, 'lastUserLat'),
                  longitude: get(this, 'lastUserLng'),
                  address: null,
                  northEastLat: null,
                  northEastLng: null,
                  southWestLat: null,
                  southWestLng: null,
                  userMarker: true,
                };
                this.transitionToRoute('index', { queryParams });
              }
            }
          });
      },

      mapZoomEnd(e) {
        const lastZoom = this.lastZoom;
        set(this, 'mapMoved', true);

        //18 is actual max zoom
        if(this.isSmallDevice)
        {
          set(get(this, 'indexController'), 'zoomMax', e.target.getZoom() > 5);
        } else {
          set(get(this, 'indexController'), 'zoomMax', e.target.getZoom() > 3);
        }

        if (e.target.getZoom() > 3) {
          set(get(this, 'indexController'), 'leafletMapConst', e);
        }

        if (!isEmpty(lastZoom)) {
          e.target.getZoom() > lastZoom ? set(this, 'zoomChanged', true) : set(this, 'zoomChanged', false);
        } else {
          set(this, 'lastZoom', e.target.getZoom());
        }
      },

      addInterest() {
        if(get(this, 'session.isAuthenticated')) {
          const currentUser = get(this, 'session.currentUser')
            get(currentUser, 'userTags').then((userTags) => {
              get(this,'selectedTags').forEach((item, i) => {
                let alreadyExists = false;
                for (let j = 0; j < userTags.length; j++) {
                  if (userTags.objectAt(j).name === item.name) {
                    alreadyExists = true
                  }
                }
                if (!alreadyExists) {
                  let tag = get(this, 'store').createRecord('userTag', {
                    name: item.name,
                    tag: item
                  });
                  tag.save().then((tagResp) => {
                    get(currentUser,'userTags').pushObject(tagResp);
                    currentUser.save();
                    this.notifications.success(
                      'Añadido #'+tagResp.name+' a tus intereses',
                      NOTIFICATION_OPTIONS,
                    );
                  });
                }
              });
            })
        } else {
          const queryParams = {
            latitude: get(this, 'lastUserLat'),
            longitude: get(this, 'lastUserLng'),
            address: null,
            northEastLat: null,
            northEastLng: null,
            southWestLat: null,
            southWestLng: null,
            userMarker: true,
          };
          this.notifications.error(
            'Escribe tu mail para poder subscribirte a los temas',
            NOTIFICATION_OPTIONS,
          );
          this.transitionToRoute('register', {queryParams} );
        }
      },

      loginTransition() {
        const queryParams = {
          latitude: null,
          longitude: null,
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
        };
        this.transitionToRoute('index', { queryParams });
      },
    },
  }
);
