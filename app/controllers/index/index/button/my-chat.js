import Controller from '@ember/controller';
import { alias, readOnly } from '@ember/object/computed';
import { set, computed } from '@ember/object';
import { resolve } from 'rsvp';
import { later } from '@ember/runloop';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';
import { htmlSafe } from '@ember/string';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
};

export default Controller.extend({
  notifications: service('notification-messages'),
  chat: alias('model.chat'),
  button: alias('model.button'),
  messages: alias('model.messages'),
  isMyButton: readOnly('button.isMine'),

  // QUERY PARAMS
  queryParams: [
    'isButton',
    'id',
  ],
  queryParams: ['id', 'isButton'],
  id: null,
  isButton: null,

  messagesHeightStyles: computed(
    'userNeededTags.[]',
    'userOfferTags.[]',
    function() {
      const headerTagsHeight = document.getElementById('chatTags').clientHeight;
      const chatContainerHeight = document.getElementById('chatContainer').clientHeight;
      const messagesHeightStyles = `height: ${chatContainerHeight - headerTagsHeight - 65}px;`;
      return htmlSafe(messagesHeightStyles);
    }
  ),



  userNeededTags: computed('button', function() {
    return this.button.neededTags;
  }),

  userOfferTags: computed('button', function() {
    return this.button.offerTags;
  }),

  otherUser: computed(
    'isMyButton',
    {
      get() {
        if (!isEmpty(this.chat) && !isEmpty(this.button) && !isEmpty(this.isMyButton)) {
          if (this.button.isMine) {
            this.chat.user.then((user) => {
              set(this, 'otherUser', user);
            });
          } else {
            this.chat.buttonCreator.then((user) => {
              set(this, 'otherUser', user);
            });
          }
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  currentUser: computed(
    'isMyButton',
    {
      get() {
        if (!isEmpty(this.chat) && !isEmpty(this.button) && !isEmpty(this.isMyButton)) {
          if (this.button.isMine) {
            this.chat.buttonCreator.then((user) => {
              set(this, 'currentUser', user);
            });
          } else {
            this.chat.user.then((user) => {
              set(this, 'currentUser', user);
            });
          }
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),
  otherMessages: null,

  canShowMessages: computed(
    'sortedMessages',
    'otherMessages',
    'otherUser',
    'currentUser',
    function() {
      return (!isEmpty(this.sortedMessages) || !isEmpty(this.otherMessages)) &&
       (!isEmpty(this.otherUser) && !isEmpty(this.currentUser));
    }),

  sortedMessages: computed(
    'messages.@each',
    {
      get() {
        let sortedMessages;
        if (!isEmpty(this.messages)) {
          sortedMessages = this.messages.sortBy('createdAt');
          if (sortedMessages.firstObject.isNew) {
            sortedMessages.push(sortedMessages.shift());
          }
        } else {
          sortedMessages = ' ';
        }
        set(this, 'otherMessages', sortedMessages);
        set(this, 'sortedMessages', sortedMessages);
      },
      set(key, value) {
        return value;
      },
    }
  ),

  newMessage: null,
  isAllowedUpdate: true,
  buttonCardClick: false,

  actions: {
    sendMessage(text) {
      if (!isEmpty(text)) {
        if(this.isAllowedUpdate) {
          set(this, 'isAllowedUpdate', false);
          return this.store.createRecord('message', {
            body: text,
            read: false,
            chat: this.chat,
          })
            .save()
            .then(() => {
              set(this, 'newMessage', null);
              this.send('scrollBottom');
            })
            .catch((error) => {
              this.messages.removeAt(this.messages.length - 1);
              this.notifications.error(
                error.errors[0].title + '. ' + error.errors[0].detail,
                NOTIFICATION_OPTIONS,
              );
            })
            .finally(() => {
              set(this, 'isAllowedUpdate', true);
            });
        }

        return resolve(later(this, () => {
          return this.send('sendMessage')(text);
        }, 500));
      }
      return resolve();
    },
    scrollBottom() {
      const chatConversation = document.getElementById('chatConversation');
      if(!isEmpty((chatConversation))) {
        chatConversation.scrollTop = chatConversation.scrollHeight;
      }
    },
    openProfile() {
      this.transitionToRoute('index.index.button', this.button.id);
    },
    backAction() {
      this.transitionToRoute('index.index.button', this.button.id);
    },
    closeButtonFile() {
      if (this.buttonCardClick) {
       this.transitionToRoute('index.index.button', this.button.id);
      }
      set(this, 'buttonCardClick', true);
    },
    cardClick() {
      set(this, 'buttonCardClick', false);
    },
  },
});
