import Controller, { inject as controller } from '@ember/controller';
import { readOnly } from '@ember/object/computed';
import { set, get } from '@ember/object';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';

export default Controller.extend({
  button: readOnly('model.button'),
  chats: readOnly('model.chats'),
  buttonController: controller('index.index.button'),
  accessDenied: readOnly('buttonController.accessDenied'),
  session: service(),

  selectedButton: null,
  socialShareNetwork: null,
  socialShareURL: null,

  init() {
    this._super(...arguments);
    // set(this, 'isNew', window.location.href.includes('new'));
  },

  // isNew: false,

  actions: {
    closeAction() {
      this.transitionToRoute('index.index');
    },
    openConversation() {
      if (get(this, 'button.creator.id') === get(this, 'currentUser.id') && get(this, 'currentUser.id') !== undefined) {
        this.transitionToRoute('index.index.button.my-chat', { queryParams: {id: this.button.id, isButton: true }});
      } else {
        this.transitionToRoute('index.index.button.chat', { queryParams: {id: this.button.id, isButton: true }});
      }
    },
    socialShareTransition() {
      switch (this.socialShareNetwork) {
        case 'facebook':
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
          break;
        case 'twitter':
          window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
          break;
        case 'linkedin':
          window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
          break;
        case 'whatsapp':
          window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
          break;
        default:
      }
    },
    removeButtonTransition() {
      set(this, 'selectedButton', null);
      this.transitionToRoute('index');
    },
  },
});
