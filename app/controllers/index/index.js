import Controller, { inject as controller } from '@ember/controller';
import { get, set, computed, setProperties } from '@ember/object';
import { debounce } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { readOnly, or, alias } from '@ember/object/computed';
import { htmlSafe } from '@ember/string';
import { resolve } from 'rsvp';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Controller.extend({
  // SERVICES
  routing: service('-routing'),
  session: service(),
  infinity: service(),
  router: service(),
  mediaQueries: service(),
  geolocation: service('geolocation'),
  notifications: service('notification-messages'),
  headTagsService: service('head-tags'),
  firstLoad: false,
  allowMoveMap: true,
  isListView: null,
  isList: null,

  queryParameters: {
    buttonsDisplay: '',
  },

  init() {
    this._super(...arguments);
    if (this.router._router.url.includes('buttonsDisplay=list')) {
      set(this, 'isListView', 'list');
      set(this, 'isList', true);
      // document.getElementsByClassName('.checkbox').checked = false;
    } else {
      set(this, 'isListView', 'map');
      set(this, 'isList', false);
      // document.getElementsByClassName('.checkbox').checked = true;

    }

    this.addObserver('buttonsDisplay', this, 'buttonsDisplayParamObserver');
  },

  buttonsDisplayParamObserver() {
    this.get('headTagsService').collectHeadTags();
    if (get(this, 'buttonsDisplay') !== 'map') {
      set(this, 'loadedMap', false);
    }
  },

  iconCreateFunction: function(cluster) {
    cluster.getChildCount();
    return L.divIcon({ html: '<div class="clusterIcon"><span>' + cluster.getChildCount() + '</span></div>' });
  },


  selectedSort: null,

  bounds: null,
  loadedMap: false,
  lastZoom: null,

  userMarkerClass: computed('userMarker', function() {
    return get(this, 'userMarker') ?
      'btn btn-link index-map__localize-user index-map__localize-user--selected' :
      'btn btn-link index-map__localize-user';
  }),

  disableQueryParamsSetting: false,

  storedTransition: null,

  zoomChanged: null,

  buttonsDisplay: readOnly('indexController.buttonsDisplay'),

  userMarker: readOnly('indexController.userMarker'),

  selectedTags: readOnly('indexController.selectedTags'),

  buttonsToShow: readOnly('indexController.buttonsToShowCached'),
  buttons: readOnly('indexController.buttons'),

  isShowingCreateNetModal: alias('indexController.isShowingCreateNetModal'),
  // isShowingCreateNetModal: false,

  userPosition: alias('indexController.userPosition'),

  currentUser: readOnly('model.currentUser'),

  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  mapMoved: true,

  buttonNetsToShow: computed(
    'store',
    {
      get(){
        this.store.findAll('buttonNet').then((net) => {
          set(this, 'buttonNetsToShow', net);
        });
      },
      set(key,value){
        return value;
      }
    }
  ),

  haveButtons: computed(
    'buttonsToShow',
    function() {
      return !isEmpty(get(this, 'buttonsToShow'));
    }
  ),

  searchMapStyles: computed(
    'selectedTags.[]',
    'isSmallDevice',
    function() {
      const headerHeight = document.getElementsByClassName('component-header')[0].clientHeight;
      const searchTopBarHeight = document.getElementsByClassName('search-topbar')[0].clientHeight;
      const headerSectionHeight = headerHeight + searchTopBarHeight;
      var searchMapStyles = `height: ${window.innerHeight - headerSectionHeight}px; top: ${headerSectionHeight}px;`;
      if (this.isSmallDevice)
      {
        searchMapStyles = `height: ${window.innerHeight - headerSectionHeight-64}px; top: ${headerSectionHeight}px;`;
      }
      return htmlSafe(searchMapStyles);
    }
  ),


  /**
   * Search type (search by area, point or address) according with transition query params
   * @type {String}
   * @property transitionType
   */
  transitionType: computed(
    'storedTransition.queryParams.{latitude,longitude,address,northEastLat,northEastLng,southWestLat,southWestLng}',
    function() {
      if(!isEmpty(this.storedTransition) && !isEmpty(this.storedTransition.queryParams)) {
        const queryParams = get(this, 'storedTransition.queryParams');
        if(!isEmpty(queryParams.nel) &&
          !isEmpty(queryParams.nelng) &&
          !isEmpty(queryParams.swl) &&
          !isEmpty(queryParams.swlng)) {
          return 'areaType';
        } else if(!isEmpty(queryParams.adr)) {
          return 'addressType';
        } else if(!isEmpty(queryParams.lat) &&
          !isEmpty(queryParams.lng)) {
          return 'pointType';
        }
        return 'errorType';
      }
      return null;
    }
  ),
  /**
   * Method to trigger a search by area, setting others QPs to null and sending bounds only,
   * @method _triggerSearchArea
   * @param  {L.LatLngBounds}           mapBounds
   * @return {Void}
   */
  _triggerSearchArea(mapBounds) {
    get(this, 'leafletMapConst').target.dragging.disable();
    set(this, 'allowMoveMap', false);
    let selectedTags;
    let isFirst = true;
    if (!isEmpty(this.selectedTags)) {
      for (let i = 0; i < this.selectedTags.length; i++) {
        if (isFirst) {
            selectedTags = this.selectedTags[i].name;
            isFirst = false;
        } else {
          selectedTags = ',' + this.selectedTags[i].name;
        }
      }
    }
    let queryParams;
      if (this.firstLoad) {
        queryParams = {
          latitude: null,
          longitude: null,
          address: null,
          northEastLat: mapBounds._northEast.lat,
          northEastLng: mapBounds._northEast.lng,
          southWestLat: mapBounds._southWest.lat,
          southWestLng: mapBounds._southWest.lng,
          searchTags: selectedTags,
        };
      } else {
        set(this, 'firstLoad', true);
        queryParams = {
          latitude: null,
          longitude: null,
          address: null,
          northEastLat: mapBounds._northEast.lat,
          northEastLng: mapBounds._northEast.lng,
          southWestLat: mapBounds._southWest.lat,
          southWestLng: mapBounds._southWest.lng,
        };
      }
      // Avoid redirect to map at button route
      if (this.routing.currentRouteName === 'index' || this.routing.currentRouteName === 'index.index' || this.routing.currentRouteName === 'index.index.index') {
        get(this, 'leafletMapConst').target.dragging.enable();
        this.transitionToRoute('index', { queryParams }).then(() => {
          set(this, 'allowMoveMap', true);
        });
      }
  },

  /**
   * Replace and store new transitions, it is only called from beforeModel hook.
   * @method _replaceTransition
   * @param  {Object}           transition Ember Transition Object
   * @return {Void}
   */
  _replaceTransition(transition) {
    const storedTransition = this.storedTransition;
    if(!isEmpty(storedTransition) && !isEmpty(storedTransition.intent) && storedTransition.isActive) {
      storedTransition.abort();
    }
    if (!isEmpty(transition.intent.name)) {
      set(this, 'storedTransition', transition);
    } else {
      set(this, 'storedTransition', transition.targetName);
    }
  },

  iconCreate: computed( function(cluster) {
      return L.icon({ iconUrl: 'svg/icons/place.svg', iconSize: [41,41] });
    }
  ),

  showUserPosition: computed(
    'userPosition',
    'userMarker',
    function() {
      return !isEmpty(this.userPosition) && this.userMarker;
    }
  ),

  minZoom: computed(
    function() {
      return this.isSmallDevice ? 1 : 3;
    }
  ),

  startFitBounds: false,
  resetFitBounds: false,

  isShowingMenuModal: false,

  leafletMapConst: null,

  isShowingModal: false,

  indexController: controller('index'),
  buttonNetController: controller('button-net.index.index'),

  modalButton: null,

  isButtonRoute: computed('routing.currentRouteName', function() {
    return get(this, 'routing.currentRouteName') === 'index.index.button';
  }),

  /**
   * Indicates that the route is in loading state.
   * Derived from `search` controller.
   *
   * @property loading
   * @type {Boolean}
   */
  loading: readOnly('indexController.loading'),

  lastResultCount: 0,

  actions: {
    loadMoreButtons(buttons) {
      this.infinity.infinityLoad(buttons).then((res) => {
        if (!isEmpty(res)) {
          if (get(this, 'lastResultCount') === 0 && res.content.length !== 0 || get(this, 'lastResultCount') === res.content.length) {
            set(this, 'lastResultCount', res.content.length);
            this.send('loadMoreButtons', res);
          } else {
            set(this, 'lastResultCount', res.content.length);
          }
        }
      })
    },
    reduceZoom(e) {
      if(!this.isSmallDevice){
        get(this, 'leafletMapConst').target.zoomOut(2);
      } else {
        get(this, 'leafletMapConst').target.setZoom(5);
      }
    },

    /**
     * Action triggered by Leaflet map when it is loaded.
     * @method loadMap
     * @return {[type]} [description]
     */
    loadMap(e) {
      const buttonsToShow = this.buttonsToShow;
      const map = e.target;
      set(this ,'leafletMapConst', e);
      const btnArray = [];
      if (get(this, 'loadedMap')) {
        if (!isEmpty(this.router._router.targetState.routerJs.oldState.queryParams)) {
          const paramsOld = this.router._router.targetState.routerJs.oldState.queryParams
          if (!isEmpty(this.buttonNetController.indexController.paramsVar)){
            map.fitBounds(L.latLngBounds(L.latLng(this.buttonNetController.indexController.paramsVar.northEastLat, this.buttonNetController.indexController.paramsVar.northEastLng),
              L.latLng(this.buttonNetController.indexController.paramsVar.southWestLat, this.buttonNetController.indexController.paramsVar.southWestLng)));
          }
          set(this, 'loadedMap', true);
        } else {
          if (!isEmpty(buttonsToShow)) {
            if (isEmpty(buttonsToShow.resolve) && !isEmpty(buttonsToShow)) {
              buttonsToShow.forEach((model) => {
                btnArray.push(L.marker([model.location[0], model.location[1]]));
              });
              let group;
              if (btnArray.length > 1) {
                group = L.featureGroup(btnArray);
                map.fitBounds(L.latLng(group.getBounds().getNorthEast(), group.getBounds().getSouthWest()).toBounds(430000));
              } else {
                map.fitBounds(L.latLng(this.bounds.getNorthEast(), this.bounds.getSouthWest()).toBounds(430000));
              }
              set(this, 'resetFitBounds', true);
            } else {
              buttonsToShow.then((buttonsToShowResp) => {
                if (!isEmpty(buttonsToShowResp)) {
                  buttonsToShowResp.forEach((model) => {
                    btnArray.push(L.marker([model.location[0], model.location[1]]));
                  });
                }
                let group;
                if (btnArray.length > 1) {
                  group = L.featureGroup(btnArray);
                  map.fitBounds(L.latLng(group.getBounds().getNorthEast(), group.getBounds().getSouthWest()).toBounds(430000));
                } else {
                  map.fitBounds(L.latLng(this.bounds.getNorthEast(), this.bounds.getSouthWest()).toBounds(430000));
                }
                set(this, 'resetFitBounds', true);
              })
            }
          } else {
            let group;
            if (btnArray.length > 1) {
              group = L.featureGroup(btnArray);
              map.fitBounds(L.latLng(group.getBounds().getNorthEast(), group.getBounds().getSouthWest()).toBounds(430000));
            } else {
              map.fitBounds(L.latLng(this.bounds.getNorthEast(), this.bounds.getSouthWest()).toBounds(430000));
            }
            set(this, 'resetFitBounds', true);
          }
        }
      } else {
        if (!isEmpty(this.router._router.targetState.routerJs.oldState.queryParams)) {
          if (!isEmpty(this.buttonNetController.indexController.paramsVar)){
            map.fitBounds(L.latLngBounds(L.latLng(this.buttonNetController.indexController.paramsVar.northEastLat, this.buttonNetController.indexController.paramsVar.northEastLng),
              L.latLng(this.buttonNetController.indexController.paramsVar.southWestLat, this.buttonNetController.indexController.paramsVar.southWestLng)));
          }
          set(this, 'loadedMap', true);
        }
        if (!isEmpty(this.indexController.northEastLat) && !isEmpty(this.indexController.northEastLng) && !isEmpty(this.indexController.southWestLat) && !isEmpty(this.indexController.southWestLng)) {
          map.fitBounds(L.latLngBounds(L.latLng(this.indexController.northEastLat, this.indexController.northEastLng), L.latLng(this.indexController.southWestLat, this.indexController.southWestLng)));
        }
        set(this, 'loadedMap', true);
      }
    },
    /**
     * Action triggered by map each time that map center changes
     * @method mapMoveEnd
     * @param  {Event Object}   e Event
     * @return {Void}     [description]
     */
    mapMoveEnd(e) {
      const map = e.target;
      if (get(this, 'loadedMap') && get(this, 'allowMoveMap')) {
        set(this, 'mapMoved', true);
        const zoomChanged = this.zoomChanged;
        const mapBounds = map.getBounds();
        // Check if loaded, and if disableQueryParamsSetting flag is activated or coordinates changed.
        if(this.loadedMap &&
          !this.disableQueryParamsSetting &&
          (parseFloat(this.northEastLat) !== mapBounds._northEast.lat ||
          parseFloat(this.northEastLng) !== mapBounds._northEast.lng)
        ) {
          // debounce triggers the passed function after 1000ms
          // whithout execute other debounce with the same method.
          if (!zoomChanged) {
            debounce(this, this._triggerSearchArea, mapBounds, 100);
          }
          setProperties(this, {
            'lastZoom': map.getZoom(),
            'zoomChanged': false,
          });
        }
        set(this, 'disableQueryParamsSetting', false);
      }
    },

    mapZoomEnd(e) {
      const lastZoom = this.lastZoom;
      set(this, 'mapMoved', true);

      //18 is actual max zoom
      if(this.isSmallDevice)
      {
        set(get(this, 'indexController'), 'zoomMax', e.target.getZoom() > 5);
        set(this, 'zoomMax', e.target.getZoom() > 5);
      } else {
        set(get(this, 'indexController'), 'zoomMax', e.target.getZoom() > 3);
        set(this, 'zoomMax', e.target.getZoom() > 3);
      }

      if (e.target.getZoom() > 3) {
        set(get(this, 'indexController'), 'leafletMapConst', e);
        set(this, 'leafletMapConst', e);
      }

      if (!isEmpty(lastZoom)) {
        e.target.getZoom() > lastZoom ? set(this, 'zoomChanged', true) : set(this, 'zoomChanged', false);
      } else {
        set(this, 'lastZoom', e.target.getZoom());
      }
    },
    /**
     * Action triggered by map each time that map center changes
     * @method reduceZoom
     * @param  {Event Object}   e Event
     * @return {Void}     [description]
     */
    markerClick(button, isNet) {
      if (isNet && get(button, 'privacy') && get(button, 'netUrl') && !get(button, 'netUrl').includes("https://www.helpbuttons.org")) {
        set(this, 'showModal', false);
        set(this, 'selectedBtn', button);
      } else if (isNet) {
        this.notifications.success(
          'Cambiaste a la Red '+ get(button, 'name')+' dedicada a: '+get(button, 'description'),
          NOTIFICATION_OPTIONS
        );
        const paramsArr = this.router.currentURL.split("&");
        let lat;
        let lng;
        let nel;
        let nelng;
        let swl;
        let swlng;
        for (var i = 0; i < paramsArr.length; i++) {
          if (paramsArr[i].includes('lat=')) {
            lat = paramsArr[i].split('=')[1]
          }
          if (paramsArr[i].includes('lng=')) {
            lng = paramsArr[i].split('=')[1]
          }
          if (paramsArr[i].includes('nel=')) {
            nel = paramsArr[i].split('=')[1]
          }
          if (paramsArr[i].includes('nelng=')) {
            nelng = paramsArr[i].split('=')[1]
          }
          if (paramsArr[i].includes('swl=')) {
            swl = paramsArr[i].split('=')[1]
          }
          if (paramsArr[i].includes('swlng=')) {
            swlng = paramsArr[i].split('=')[1]
          }
        }
        if (isEmpty(lat)) {
          this.transitionToRoute('button-net.index.index', get(button, 'name'), { queryParams: { nel: nel, nelng: nelng, swl: swl, swlng: swlng } })
        } else {
          this.transitionToRoute('button-net.index.index', get(button, 'name'), { queryParams: { lat: lat, lng: lng } })
        }
      } else {
        set(this, 'showModal', false);
        set(this, 'selectedBtn', null);
        this.transitionToRoute('index.index.button', get(button, 'id'));
      }
    },
    ///togglebuttton trigger route
    toggleAction(checked) {
      var elm = document.getElementById('toggle');

      if (checked != elm.checked) {
        if (Ember.get(this, 'isListView') === 'list') {
            Ember.set(this, 'isListView', 'map');
          } else {
            Ember.set(this, 'isListView', 'list');
          }
        set(this,'isList', !get(this,'isList'));
        this.transitionToRoute('index.index', { queryParams: { buttonsDisplay: this.isListView }});
        // elm.click();
      }
    },

    acceptMarkerClick(button, option) {
      set(this, 'showModal', false);
      set(this, 'selectedBtn', null);
      this.transitionToRoute(get(button, 'netUrl')+'/button', get(button, 'id'));
    },
    closeAction() {
      set(this, 'showModal', false);
      set(this, 'selectedBtn', null);
    },
    create() {
      if(this.isButtonNetRoute) {
        this.transitionToRoute('button-net.index.index.new-button','new');
      } else {
        this.transitionToRoute('index.index.new-button','new');
      }
    },
    toggleModal() {
      this.toggleProperty('isShowingCreateNetModal')
    },
    localizeUser() {
      if(get(this, 'userMarker')) {
        const queryParams = {
          latitude: get(this, 'userPosition.lat'),
          longitude: get(this, 'userPosition.lng'),
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: false,
        };
        this.transitionToRoute('index', { queryParams });
        return null;
      }
      const geolocation = get(this, 'geolocation');
      return geolocation.getCurrentPosition()
        .then((coordinates) => {
          const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);

          set(this, 'userPosition', userPosition);

          const queryParams = {
            latitude: userPosition.lat,
            longitude: userPosition.lng,
            address: null,
            northEastLat: null,
            northEastLng: null,
            southWestLat: null,
            southWestLng: null,
            userMarker: true,
          };
          this.transitionToRoute('index', { queryParams });
        });
    },
  },
});
