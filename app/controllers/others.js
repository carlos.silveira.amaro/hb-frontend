import Controller from '@ember/controller';
import { resolve } from 'rsvp';

export default Controller.extend({
  queryParams: ['section'],
  section: null,

  actions: {
    searchAddress(address, tags) {
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
        searchTags: tags,
      };
      this.transitionToRoute('index', { queryParams });
    }
  }
});
