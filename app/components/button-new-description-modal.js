import Component from '@ember/component';
import { computed, get, set, getProperties} from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import { or, equal, alias } from '@ember/object/computed';
import { hash, resolve } from 'rsvp';
import { htmlSafe } from '@ember/string';
import { notEmpty } from '@ember/object/computed';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  session: service(),

  classNames: ['button-new-description'],

  mediaQueries: service(),
  store: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  screens: service('screen'),
  notifications: service('notification-messages'),
  router: service(),
  isButtonNetRoute: false,

  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));

    if (isEmpty(get(this, 'button.buttonNetId'))) {
      set(this, 'button.buttonNetId', []);
    }
    // get(this, 'button.buttonNetId').pushObject(get(this, 'buttonNetController.model.id'));
  },

  // button:null,

  nextStepAction() {},

  isChangeType: equal('button.buttonType', 'change'),
  isNeedType: equal('button.buttonType', 'need'),
  isOfferType: equal('button.buttonType', 'offer'),

  hasImage: computed('button.imageFile', 'button.image', {
    get() {
      if (!isEmpty(get(this, 'button.imageFile'))) {
        return !isEmpty(get(this, 'button.imageFile'))
      } else if (!isEmpty(get(this, 'button.image'))) {
        get(this, 'button.image').then((image) => {
          set(this, 'hasImage', !isEmpty(image));
        })
      }
    },
    set(key, value) {
      return value;
    }
  }),

  buttonImage: computed('button.imageFile', 'button.image', {
    get() {
      if (get(this, 'hasImage')) {
        if (!isEmpty(get(this, 'button.imageFile'))) {
          let context = this;
          setTimeout(function () {
            set(context, 'buttonImage', get(context, 'button.imageFile.url'));
          }, 50, context);
        } else if (!isEmpty(get(this, 'button.image'))) {
          get(this, 'button.image').then((image) => {
            set(this, 'buttonImage', get(image, 'url'));
          })
        }
      }
    },
    set(key, value) {
      return value;
    }
  }),


///NEXT BUTTON
  isNextButtonDisabled: computed(
    'selectedOfferTags.[]',
    'selectedNeededTags.[]',
    'button.{description,buttonType}',
    function() {
      return (
        isEmpty(get(this, 'button.description')) || !get(this, 'button.hasValidTags')
      );
    }
  ),

  buttonNetController: null,
  tags: null,
  showSpinner: false,
  selectedOfferTags: alias('button.offerTags'),
  selectedNeededTags: alias('button.neededTags'),

  didRenderPage: false,

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'selectedOfferTags',
    'selectedNeededTags',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('button-new-description')[0];
      let contentHeight = document.getElementById('typeContent');
      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        if (bodyHeight - contentHeight > 100) {
          const svgStyles = `height: ${bodyHeight - contentHeight}px;`;
          return htmlSafe(svgStyles);
        }
        const svgStyles = 'height: 100px;';
        return htmlSafe(svgStyles);
      }
      return '';
    }
  ),

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    'svgMaxHeight',
    function() {
      this.svgMaxHeight;
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-description')[0];
      const contentHeight = document.getElementsByClassName('button-new-description__content')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight)) {
        return containerHeight.clientHeight > contentHeight.clientHeight ?
          '' : 'button-new-description__content-has-scroll';
      }
      return '';
    }),

  buttonCardClick: false,


  trendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'trendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  actions: {
    nextStep() {
      get(this, 'nextStepAction')();
    },
    clearImage(file) {
      file=get(this, 'button.imageFile');
      const image=get(this, 'button.image');

      if(file!=null)
      {
        set(this, 'button.imageFile', null);
      }

      if(image!=null) {
        image.then((imageResponse) => {
          imageResponse.destroyRecord();
        });
      }
    },

    togglePicker(param) {
      set(this, 'button.date', null);
      set(this, 'buttonDateType', param);
    },

    searchNeedTags(term, selectClass) {
      term=term.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");
      term=term.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      if (term.includes(' ')) {
        selectClass.actions.close();
        const fixedTerm = term.replace(' ', '');
        if (!(!isEmpty(this.tags) && !isEmpty(this.tags.findBy('name', fixedTerm)))) {
          return this.send('createNewTag', 'selectedNeededTags', fixedTerm, selectClass);
        }
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            return get(this, 'selectedNeededTags').pushObject(tags.get('firstObject'));
          });
      }
      return get(this, 'store').query('tag', { 'filter[name]': term })
        .then((tags) => {
          set(this, 'tags', tags);
          return tags;
        });
    },

    searchOfferTags(termParam, selectClass) {
      let term = termParam;
      term=term.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      term=term.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");
      if (term.includes('#')) {
        term = term.replace('#', '');
        selectClass.searchText = selectClass.searchText.replace('#', '');
      }
      if (term.includes(' ')) {
        selectClass.actions.close();
        const fixedTerm = term.replace(' ', '');
        if (!(!isEmpty(this.tags) && !isEmpty(this.tags.findBy('name', fixedTerm)))) {
          return this.send('createNewTag', 'selectedOfferTags', fixedTerm, selectClass);
        }
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            return get(this, 'selectedOfferTags').pushObject(tags.get('firstObject'));
          });
      }
      return get(this, 'store').query('tag', { 'filter[name]': term })
        .then((tags) => {
          set(this, 'tags', tags);
          return tags;
        });
    },

    createNewTag(targetRelationship, tagName/* , selectClass*/) {
      tagName=tagName.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");

      if(get(this, 'session.isAuthenticated')) {
        set(this, 'showSpinner', true);
        return get(this, 'store').createRecord('tag', {
          name: tagName,
        })
          .save()
          .then((tag) => {
            set(this, 'showSpinner', false);
            get(this, targetRelationship).pushObject(tag);
            this.notifications.success(
              'Tag creado',
              NOTIFICATION_OPTIONS,
            );
          })
          .catch((error)=>{
            set(this, 'showSpinner', false);
            this.notifications.error(error.errors[0].detail, NOTIFICATION_OPTIONS);
          });
      }
      this.notifications.error(
        'Debes iniciar sesión para poder crear tags',
        NOTIFICATION_OPTIONS,
      );
      return resolve(() => {
        this.notifications.error(
          'Error al crear el tag',
          NOTIFICATION_OPTIONS,
        );
      });
    },

    changeSelectedTags(targetRelationship, tags) {
      if (tags.length > 5) {
        this.notifications.error(
          'Limite de tags alcanzado',
          NOTIFICATION_OPTIONS,
        );
      } else {
        this.toggleProperty('button.toggledTags');
        if (isEmpty(get(this, 'button.toggledTags'))) {
          set(this, 'button.toggledTags', false);
        }
        set(this, targetRelationship, tags);
      }
    },
    checkDescriptionLength(param) {
      if (param.length > 250) {
        this.notifications.error(
          'Limite de descripción alcanzado',
          NOTIFICATION_OPTIONS,
        );
        set(this, 'button.description', param.substring(0, 250));  
      }
    },
    removeTag(targetRelationship, tag) {
      const target = get(this, targetRelationship);
      if (isEmpty(get(this, 'button.toggledTags'))) {
        set(this, 'button.toggledTags', false);
      }
      this.toggleProperty('button.toggledTags');
      target.removeObject(tag);
    },

    tagSuggestion(term) {
      if(!get(this,'session.isAuthenticated'))
      return `Tienes que estar registrado para crear temas`;

      return `Pulsa intro para crear ${term}`;
    },

    uploadImage(file) {
      set(this, 'button.imageFile', file);
      file.readAsDataURL().then(function(url) {
        file.set('url', url);
      });
    },

    checkTagAlreadyExists(term) {
      const tags = this.tags;
      return !(!isEmpty(tags) && !isEmpty(tags.findBy('name', term)));
    },
    swapButton() {
      if (isEmpty(this.button.swap)) {
        set(this.button, 'swap', true);
      }
      this.toggleProperty('button.swap');
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },

    closeModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },

  },
});
