import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { get, set, getProperties, computed } from '@ember/object';
import { notEmpty } from '@ember/object/computed';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  classNames: ['container-fluid', 'login-component'],
  classNameBindings: ['loginComponentBindClass'],
  session: service('session'),
  notifications: service('notification-messages'),
  routing: service('-routing'),
  router: service(),

  isModal: null,
  buttonNet: null,

  isMobileButtonCreation: null,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  loginComponentBindClass: computed(
    'isModal',
    'isMobileButtonCreation',
    function() {
      if (this.isModal) {
        return 'login-component__modal';
      }
      if (this.isMobileButtonCreation) {
        return 'login-component--taller';
      }
      return 'login-component__main-page';
    }
  ),

  email: null,

  password: null,
  errorMessages: null,

  modalClasses: computed('isModal', function() {
    return this.isModal ? 'col-10' : 'col-12 col-sm-10 col-md-7 col-lg-9';
  }),

  hasError: notEmpty('errorMessages'),

  isVisiblePassword: false,

  routeName: null,

  passwordInputType: computed(
    'isVisiblePassword',
    function() {
      return this.isVisiblePassword ? 'text' : 'password';
    }
  ),

  actions: {
    authenticate() {
      get(this, 'session').invalidate();
      const { email, password } = getProperties(this, 'email', 'password');
      set(this, 'errorMessage', null);
      return this.session
        .authenticate('authenticator:tiddle', email, password)
        .then(() => {
          if (get(this, 'routeName') == 'button-net.register') {
            this.get('router').transitionTo('button-net.index.index', get(this, 'buttonNet.urlName'));
          }
          this.notifications.success(
            'Bienvenid@ a Helpbuttons!',
            NOTIFICATION_OPTIONS
          );
        })
        .catch(({ payload }) => set(this, 'errorMessages', payload.errors) );
    },
    togglePasswordVisibily() {
      this.toggleProperty('isVisiblePassword');
    },
    removeLocalStorage() {
      window.localStorage.clear();
      window.location.reload();
    },
  },
});
