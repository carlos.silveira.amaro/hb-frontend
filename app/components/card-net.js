import Component from '@ember/component';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';


export default Component.extend({
  classNames: ['col-12', 'card-notification', 'button-file__messages'],
  chat: null,
  user:null,
  userButtonNets: null,
  router: service(),
  isButtonNetRoute: false,

  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },


  buttonNetIcon: computed( 'userButtonNets', function(){
        return get(this, 'userButtonNets.imgUrl');
    }),


  isCreator: computed( 'userButtonNets.creator','userButtonNets','userButtonNets.creator.name',  function(){
        return get(this, 'userButtonNets.creator.id') == get(this, 'user.internalId') ;
    }),

  actions: {
    removeNet() {
      get(this, 'userButtonNets').destroyRecord();
    },
    showRemoveNet(param) {
      set(this, 'showDeleteNet', param);
    }
  },


});
