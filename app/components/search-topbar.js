import Component from '@ember/component';
import { or } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { computed, set, get } from '@ember/object';
import { readOnly } from '@ember/object/computed';


export default Component.extend({
  mediaQueries: service(),
  classNames: ['search-topbar', 'pl-lg-0'],
  classNameBindings: ['useContainer'],
  resultsNumber: null,
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  session: service('session'),
  currentUser: readOnly('session.currentUser'),
  showPulsedButtons: null,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  subtitle: computed(
    'showPulsedButtons',
    'resultsNumber',
    function() {
      if (this.resultsNumber === 0) {
        if (this.isMine) {
          if (this.showPulsedButtons) {
            return 'No has pulsado ningún botón';
          }
          return 'No has creado ningún botón';
        }
        return 'No hay botones'+ this.resultsUserName;
      } else if (this.resultsNumber === 1) {
        if (this.isMine) {
          if (this.showPulsedButtons) {
            return 'Mostrando tu botón pulsado';
          }
          return 'Mostrando tu botón creado';
        }
        return 'Mostrando el botón' + this.resultsUserName;
      }
      if (this.isMine) {
        if (this.showPulsedButtons) {
          return 'Mostrando tus ' + this.resultsNumber + ' botones pulsados';
        }
        return 'Mostrando tus ' + this.resultsNumber + ' botones creados';
      }
      return 'Mostrando los ' + this.resultsNumber + ' botones' + this.resultsUserName;
    }
  ),

  useContainer: computed('isSmallDevice', function() {
    if (this.isSmallDevice) {
      return 'container-fluid';
    }
    return 'container';
  }),

  otherProfile: null,
  userName: null,

  isMine: false,

  resultsUserName: computed(
    'otherProfile',
    'resultsNumber',
    function() {
      if (this.otherProfile) {
        return ' creados por ' + this.userName;
      }
      if (this.resultsNumber === 1) {
        return ' encontrado en la zona';
      }
      if (this.resultsNumber === 0) {
        return ' encontrados en la zona';
      }
      return ' encontrados en la zona';
    }),
});
