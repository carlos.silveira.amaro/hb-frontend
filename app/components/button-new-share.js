import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { htmlSafe } from '@ember/string';
import { isEmpty } from '@ember/utils';
import { get, set, computed } from '@ember/object';
import { readOnly } from '@ember/object/computed';
import { setProperties } from '@ember/object'

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  classNames: ['button-new-share'],
  classNameBindings: ['isButtonFile:button-new-share__button-file'],
  notifications: service('notification-messages'),
  isShowingOptions: null,
  screens: service('screen'),

  button: null,
  finishCreationAction() {},
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  buttonUrl: computed(
    'button.{id,latitude,longitude}',
    function() {
      const origin = window.location.origin;
      const button = this.button;
      const buttonUrl = `${origin}/button/${button.id}?hideWelcome=true&lat=${button.latitude}&lng=${button.longitude}`;
      return {
        string: buttonUrl,
        encoded: encodeURIComponent(buttonUrl),
      };
    }
  ),

  didRenderPage: false,

  facebookLink: computed('buttonUrl', function() {
    return 'location.href=https://www.facebook.com/sharer/sharer.php?u=${this.buttonUrl.encoded};';
  }),

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('button-new-share')[0];
      let contentHeight = document.getElementById('typeContent');
      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        if (bodyHeight - contentHeight > 100) {
          const svgStyles = `height: ${bodyHeight - contentHeight}px;`;
          return htmlSafe(svgStyles);
        }
        const svgStyles = 'height: 60px;';
        return htmlSafe(svgStyles);
      }
      return '';
    }
  ),

  // toggleProperty() {},
  isButtonFile: null,
  socialShareNetwork: null,
  socialShareTransition: null,
  hideShare:true,
  socialShareURL: null,
  closeAction: {},

  actions: {
    showShareOptions() {
      set(this, 'isShowingOptions', true);
    },

    copyRoute() {
      const temporalInput = document.createElement('textarea');
      temporalInput.value = this.buttonUrl.string;
      temporalInput.setAttribute('readonly', '');
      temporalInput.style = {position: 'absolute', left: '-9999px'};
      document.body.appendChild(temporalInput);

      if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
        // save current contentEditable/readOnly status
        const editable = temporalInput.contentEditable;
        const readOnly = temporalInput.readOnly;

        // convert to editable with readonly to stop iOS keyboard opening
        temporalInput.contentEditable = true;
        temporalInput.readOnly = true;

        // create a selectable range
        const range = document.createRange();
        range.selectNodeContents(temporalInput);

        // select the range
        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        temporalInput.setSelectionRange(0, 999999);

        // restore contentEditable/readOnly to original state
        temporalInput.contentEditable = editable;
        temporalInput.readOnly = readOnly;
      } else {
        temporalInput.select();
      }

      if(document.execCommand('copy')) {
        this.notifications.success(
          'Has copiado un enlace a tu botón',
          NOTIFICATION_OPTIONS,
        );
        set(this, 'button.actionEvent', 'button-shared');
        this.button.save().catch((err) => {
          this.notifications.error(
            err,
            NOTIFICATION_OPTIONS,
          );
        });
      } else {
        this.notifications.error(
          'No se pudo copiar el enlace a tu botón',
          NOTIFICATION_OPTIONS,
        );
      }
      document.body.removeChild(temporalInput);
    },

    copyWidgetRoute() {
      const temporalInput = document.createElement('textarea');
      temporalInput.value = '<iframe src="'+this.buttonUrl.string+'" height="600" width="400" title="Iframe Example"></iframe>';
      temporalInput.setAttribute('readonly', '');
      temporalInput.style = {position: 'absolute', left: '-9999px'};
      document.body.appendChild(temporalInput);

      if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
        // save current contentEditable/readOnly status
        const editable = temporalInput.contentEditable;
        const readOnly = temporalInput.readOnly;

        // convert to editable with readonly to stop iOS keyboard opening
        temporalInput.contentEditable = true;
        temporalInput.readOnly = true;

        // create a selectable range
        const range = document.createRange();
        range.selectNodeContents(temporalInput);

        // select the range
        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        temporalInput.setSelectionRange(0, 999999);

        // restore contentEditable/readOnly to original state
        temporalInput.contentEditable = editable;
        temporalInput.readOnly = readOnly;
      } else {
        temporalInput.select();
      }

      if(document.execCommand('copy')) {
        this.notifications.success(
          'Has copiado el código para insertar tu botón',
          NOTIFICATION_OPTIONS,
        );
        set(this, 'button.actionEvent', 'button-shared');
        this.button.save().catch((err) => {
          this.notifications.error(
            err,
            NOTIFICATION_OPTIONS,
          );
        });
      } else {
        this.notifications.error(
          'No se pudo copiar el enlace a tu botón',
          NOTIFICATION_OPTIONS,
        );
      }
      document.body.removeChild(temporalInput);
    },

    finishCreation() {
      this.finishCreationAction();
    },
    toggleShare() {
       if(get(this,'hideShare')==true){
         set(this,'hideShare', false);
       } else {
         set(this,'hideShare', true);
      }
    },
    socialShare(socialNetwork, socialShareURL) {
      set(this, 'socialShareNetwork', socialNetwork);
      set(this, 'socialShareURL', socialShareURL);
      this.socialShareTransition();
    },
    closeModal() {
      this.closeAction();
    }
  },
});
