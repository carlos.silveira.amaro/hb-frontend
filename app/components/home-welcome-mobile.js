import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { get, set, computed } from '@ember/object';
import { A } from '@ember/array';
import { readOnly } from '@ember/object/computed';
import { isEmpty } from '@ember/utils'
import { setProperties } from '@ember/object'


export default Component.extend({
  classNames: 'home-welcome-mobile',

  session: service('session'),
  routing: service('-routing'),
  store: service(),


  isShowingMenuModal: null,
  isShowingMenuTags: null,
  searchTrendingTags: null,
  isSearching: false,
  loading: null,
  togglePropertyMenu: {},

  searchZoneAddress:null,
  buttonNet: null,

  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },


  welcomeTitle: computed('titlesArrays', function() {
    return get(this, 'titlesArrays')[0];
  }),

  welcomeTitle1: computed('titlesArrays', function() {
    return get(this, 'titlesArrays')[1];
  }),

  titlesArrays: A([
    'Colabora en temas cerca de: ',
    'Colabora en temas cerca de: ',
    //'Crea un botón de ayuda',
    // 'De todo salimos juntos.',
    // '"Madre mía cómo está la vida"',
    // '"El ser humano… es extraordinario"',
    // '"La herramienta para la vida colaborativa"',
    // '"La herramienta para la vida colaborativa"',
  ]),

  moreTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length > 5;
  }),

  noTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length == 0;
  }),

  firstTrendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];

    if (!isEmpty(tags)) {
      for (let i = 0; i < tags.length; i++) {
        tagsArr.push({name: get(tags[i], 'name'), activeButtonsCounter: get(tags[i], 'activeButtonsCounter')});
      }
      return tagsArr.slice(0,5);
    }
    return tagsArr;
  }),

  trendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];
    if (!isEmpty(tags)) {
      for (let i = 0; i < Object.keys(tags).length; i++) {
        tagsArr.push({name: Object.keys(tags)[i], activeButtonsCounter: Object.values(tags)[i]});
      }
    }
    return tagsArr;
  }),

  globalTrendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((globalTrendingTags) => {
            set(this, 'globalTrendingTags', globalTrendingTags.slice(0,5));
          });
        // return null;
      },
      set(key, value) { return value; },
    }
  ),

  contextTransition: null,

  actions: {
    toggleModal(param) {
      this.toggleProperty(param);
    },
    create() {
      if(this.isButtonNetRoute) {
        this.send('toggleModal', 'isShowingModal');
        this.transitionToRoute('button-net.index.index.new-button','new');
      } else {
        this.send('toggleModal', 'isShowingModal');
        this.transitionToRoute('index.index.new-button','new');
      }
    },
    hideMenu () {
      if (!isEmpty(get(this, 'contextTransition'))) {
        get(this, 'contextTransition').abort();
      }
    },
    searchByAddress(param) {
      set(this, 'isSearching', true);
      set(this, 'loading', true);
      const address = param.formatted_address;
      return this.searchAction(address, get(this, 'selectedFilterTags'))
        .then(() => {
          setProperties(this, {
            'isSearching': false,
            'inputAddress': null,
            'loading': false,
          });
          this.send('toggleModal', 'isShowingModal');
          // get(this, 'router').transitionTo('index', { queryParams: { buttonsDisplay:"map", hideWelcome:"true", adr: address } })
        });
    },
  },

});
