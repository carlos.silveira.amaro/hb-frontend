import Component from '@ember/component';
import { get, computed, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { notEmpty } from '@ember/object/computed';
import { inject as service } from '@ember/service';

export default Component.extend({
  classNames: ['d-flex mb-2'],
  avatar: null,
  message: null,
  otherUser: null,
  currentUser: null,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },


  profileRoute: computed(
    'isAuthor',
    function() {
      if (get(this, 'isButtonNetRoute')) {
        return get(this, 'isAuthor') ? 'button-net.profile.index' : 'button-net.user.buttons';
      } else {
        return get(this, 'isAuthor') ? 'profile.index' : 'user.buttons';
      }
    }
  ),

  userAvatar: computed(
    'user',
    {
      get() {
        const user = get(this, 'user');
        if (!isEmpty(user)) {
          get(user, 'avatar').then((avatar) => {
            set(this, 'userAvatar', avatar);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  hasAvatar: notEmpty('userAvatar'),


  linkSegmentId: computed(
    'isAuthor',
    function() {
      const isMine = get(this, 'isAuthor');
      return isMine ? null : this.message.belongsTo('user').id();
    }
  ),

  isAuthor: computed('message', 'otherUser', 'currentUser', function() {
    if (isEmpty(get(this, 'message.user.content.id'))) {
      return true;
    }
    if (get(this, 'currentUser.id') === 'current') {
      return get(this, 'currentUser.internalId') === Number(get(this, 'message.user.content.id'));
    }
    return get(this, 'currentUser.id') === get(this, 'message.user.content.id');
  }),

  time: computed('message.createdAt', function() {
    const time = get(this, 'message.createdAt');
    if (!isEmpty(time)) {
      const minutes = time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes();
      return time.getHours() + ':' + minutes;
    }
    return null;
  }),


  didRender() {
    this.send('scrollBottom');
  },

  actions: {
    scrollBottom() {
      const chatConversation = document.getElementById('chatConversation');
      if(!isEmpty((chatConversation))) {
        chatConversation.scrollTop = chatConversation.scrollHeight;
      }
    },
  },
});
