import Component from '@ember/component';
import { get, set, computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import { isEmpty } from '@ember/utils'
import MutableArray from '@ember/array/mutable';


export default Component.extend({
  store: service(),
  routing: service('-routing'),

  classNames: ['info-overlay'],

  init() {
    this._super(...arguments);
    if (this.router._router.url.includes('buttonsDisplay=list')) {
      set(this, 'isListView', 'list');
    }
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  isShowingMenuModal: null,
  searchTrendingTags: null,
  router: service(),
  isButtonNetRoute: false,
  buttonNet: null,
  isListView: null,

  togglePropertyMenu: {},

  globalTrendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'globalTrendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),


  netSelectedTags:  computed('buttonNet', function() {
    return get(this, 'buttonNet.tags');
  }),

  // sortedArray : computed('searchTrendingTags', function(a,b)  {
  //   return b[0] - a[0];
  // }),


  trendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let sortedArray = [];
    if(tags!=null)
    sortedArray = tags.sortBy('activeButtonsCounter').reverse();
    let tagsArr = [];
    if (!isEmpty(tags)) {
      for (let i = 0; i < sortedArray.length && i < 15; i++) {
        tagsArr.push({name: get(sortedArray[i], 'name'), activeButtonsCounter: get(sortedArray[i], 'activeButtonsCounter')});
      }
    }
    return tagsArr;
  }),

  actions: {

    create() {
       this.get('create')();
     },

    hideMenu () {
      this.togglePropertyMenu('isShowingMenuModal');
    },

  }

});
