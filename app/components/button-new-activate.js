import Component from '@ember/component';
import { computed } from '@ember/object';
import { set, get } from '@ember/object';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { readOnly } from '@ember/object/computed'
import { notEmpty } from '@ember/object/computed'

export default Component.extend({
  classNames: ['button-new-activate'],
  screens: service('screen'),
  session: service('session'),
  currentUser: readOnly('session.currentUser'),

  toggleButtonState: null,

  buttonClass: computed('button.active', function() {
    return !this.button.active ?
      'buttons-new-activate-button__inactive-button button-file__yellow-circle' +
      ' button-file__yellow-circle--big-circle button-file__yellow-circle-content' :
      'button-file__yellow-circle' +
      ' button-file__yellow-circle--big-circle button-file__yellow-circle-content';
  }),

  buttonCreator: computed('button.creator', function() {
    return get(this, 'button.creator').then((buttonCreator) => {
      set(this, 'buttonCreator', buttonCreator);
    })
  }),

  imageFile: notEmpty('button.imageFile'),


  nextStep: null,

  didRenderPage: false,

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-activate')[0];
      const contentHeight = document.getElementsByClassName('button-new-activate__content')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight)) {
        return containerHeight.clientHeight > contentHeight.clientHeight ?
          '' : 'button-new-activate__content-has-scroll';
      }
      return '';
    }),

  nextStepAction() {},

  buttonCardClick: false,
  isNewButton: true,

  actions: {
    toggleButtonState() {
      this.toggleProperty('button.active');
      this.nextStepAction();
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
    saveButton() {
      this.nextStepAction();
    },
    closeModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
  },
});
