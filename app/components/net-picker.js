import Component from '@ember/component';
import Model from 'ember-data/model';
import { get, set, computed , getProperties} from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';


export default Component.extend({
  store: service(),
  router: service(),
  agreedNet: null,
  button: null,
  loading: null,
  newNet:null,
  hideCreateNet:null,
  isProfileEdition:null,
  severalNets:false,
  isUserTags:false,
  isCreation: false,
  buttonNetController: null,
  changedInterests: null,


  net: computed(
    'agreedNet',
    function() {
      const agreedNet = get(this, 'agreedNet');
      return agreedNet;
    }
  ),

  defaultNet:  computed(
    function() {
        const newNet = Ember.Object.create({
                  name: 'Todas las redes'
              });
        newNet.set('name','Todas las redes');
        newNet.set('imgUrl','network');
        newNet.set('id',0);
        return newNet;
  }),

  isDefaultSelected: computed('defaultNet', 'selectedNet', function() {
    return get(this, 'defaultNet') === get(this, 'selectedNet') ? 'net-picker-icon--small justify-content-between net-picker__dropdown-trigger col defaultSelected' : 'net-picker-icon--small justify-content-between net-picker__dropdown-trigger col';
  }),

  isTriggerClicked:false,
  TriggerClickedClass: computed('isTriggerClicked', function() {
    let buttonPageClass = get(this, 'isCreation') || get(this, 'isUserTags') ? 'net-picker__white-background' : '';
    buttonPageClass += get(this, 'isFriendNet') ? 'net-picker__friend-net' : '';
    return get(this, 'isTriggerClicked') ? buttonPageClass +' net-picker-icon--small justify-content-between net-picker__dropdown-trigger col net-picker__dropdown-trigger__triggerOpen' : buttonPageClass + ' net-picker-icon--small justify-content-between net-picker__dropdown-trigger col net-picker__dropdown-trigger__triggerClose';
  }),

  nets: computed(
    'store',
    'defaultNet',
    {
      get() {
        this.store.findAll('buttonNet').then((net) => {
          if(net[0]!=this.get('defaultNet') && !get(this, 'isFriendNet')){
            if (isEmpty(get(this, 'buttonNetController'))) {
              const sortedNets = net.sortBy('btnCount').reverse();
              set(this, 'nets', sortedNets.slice().insertAt(0, this.get('defaultNet')));
            } else {
              const newNet = Ember.Object.create({
                        name: 'Todas las redes'
                    });
              newNet.set('name','Todas las redes');
              newNet.set('imgUrl','network');
              newNet.set('id',0);
              const sortedNets = net.sortBy('btnCount').reverse();
              set(this, 'nets', sortedNets.slice().insertAt(0, newNet));
            }
          } else {
            const sortedNets = net.sortBy('btnCount').reverse();
            set(this, 'nets', sortedNets);
          }
        });
      },
      set(key,value){
        return value;
      }
    }
  ),

  selectedNet: computed(
    'agreedNet',
    'isProfileEdition',
    'isFriendNet',
    'isCreation',
    {
      get() {

        if (get(this, 'isProfileEdition')) {
          const arr = [];
          if (isEmpty(get(this, 'userTag.buttonNets'))) {
            arr.push(get(this, 'defaultNet'));
            return arr;
          } else {
            let nets = get(this, 'userTag.buttonNets');
            let count = 0;
            if (!Array.isArray(nets)) {
              const arr1 = [];
              arr1.push(nets);
              nets = arr1;
            }
            for (let i = 0; i < nets.length; i++) {
              if (!isEmpty(nets[i])) {
                if (i <= nets.length - 1) {
                  this.store.findRecord('buttonNet', nets[i]).then((net) => {
                    count++;
                    arr.push(net);
                    if (count === i + 1) {
                      set(this, 'selectedNet', arr);
                      return arr;
                    }
                  });
                } else {
                  return this.store.findRecord('buttonNet', nets[i]).then((net) => {
                    count++;
                    arr.push(net);
                    if (count === i + 1) {
                      return arr;
                    }
                  });
                }
              } else {
                return arr;
              }
            }
          }
        } else if (get(this, 'isFriendNet')) {
          const arr = [];
          if (isEmpty(get(this, 'selectedNetObj.otherNetIds'))) {
            return arr;
          } else {
            let nets = get(this, 'selectedNetObj.otherNetIds');
            let count = 0;
            if (!Array.isArray(nets)) {
              const arr1 = [];
              arr1.push(nets);
              nets = arr1;
            }
            for (let i = 0; i < nets.length; i++) {
              if (i < nets.length - 1) {
                this.store.findRecord('buttonNet', nets[i]).then((net) => {
                  count++;
                  arr.push(net);
                  if (count === i + 1) {
                    return net;
                  }
                });
              } else {
                return this.store.findRecord('buttonNet', nets[i]).then((net) => {
                  count++;
                  arr.push(net);
                  if (count === i + 1) {
                    return arr;
                  }
                });
              }
            }
          }
        } else if (get(this, 'isCreation')) {
          var arr = [];
          if (isEmpty(get(this, 'newButton.buttonNetId')) || isEmpty(get(this, 'newButton.buttonNetId')[0])) {
              if (isEmpty(get(this, 'buttonNetController.model'))) {
                  arr.pushObject(get(this, 'defaultNet'));
              } else {
                arr.pushObject(get(this, 'buttonNetController.model'));
              }
            return arr;
          } else {
            let buttonNets = get(this, 'newButton.buttonNetId');
            if (!Array.isArray(buttonNets) && buttonNets.length) {
              const arr1 = [];
              arr1.push(buttonNets);
              buttonNets = arr1;
            }
            arr=[];
            for (let i = 0; i < buttonNets.length; i++) {
              if (!isEmpty(buttonNets[i])) {

                this.store.findRecord('buttonNet', buttonNets[i]).then((net) => {
                  arr.pushObject(net);
                });
              }
            }
            return arr;
          }
        } else {
          var arr = [];
          const agreedNet = get(this, 'agreedNet');
          if (isEmpty(agreedNet)) {
            arr.pushObject(get(this, 'defaultNet'));
            return arr;
          } else {
            arr.pushObject(agreedNet);
            return arr;
          }
        }
      },
      set(key,value){
        return value;
      }
    }
  ),

  modalClasses: computed('isModal', function() {
    return this.isModal ? 'col-10' : 'col-12 col-sm-10 col-md-7 col-lg-9';
  }),

  isOpened: false,

  netIndicator: null,
  userTag: null,
  isFriendNet: false,
  selectedNetObj: null,

  isShowingCreateNetModal:false,


  actions: {
    toggleModal(param) {
      set(this, 'isOpened', param);
    },

    toggleModalCreate() {
      this.closeDropdown('dropdown');
      // this.toggleProperty('isShowingCreateNetModal');
    },


    // searchNets(term, selectClass) {
    //   if (term.includes(' ')) {
    //     selectClass.actions.close();
    //     const fixedTerm = term.replace(' ', '');
    //     return get(this, 'store').query('buttonNet', { 'filter[name]': fixedTerm })
    //       .then((nets) => {
    //         nets.slice().insertAt(0, this.get('selected'));
    //         set(this, 'nets', nets);
    //         return nets;
    //       });
    //   }
    //   return get(this, 'store').query('buttonNet', { 'filter[name]': term })
    //     .then((nets) => {
    //       nets.slice().insertAt(0, this.get('selected'));
    //       set(this, 'nets', nets);
    //       return nets;
    //     });
    // },
    //
    // changeSelectedNets(nets) {
    //   set(this, 'agreedNet', nets);
    // },

    netSuggestion() {
      return '';
    },
    /**
     * @method closeDropdown
     * @return
     */
    closeDropdown(dropdown) {
      dropdown.actions.close();
    },
    searchButtonNets(param) {
      return this.store.query('buttonNet', { 'name': param, 'active': true, 'filter': true }).then((response) => {
        return response
      })
    },
    fetchOptions() {
      set(this, 'isTriggerClicked', true);
    },
    clearNetAction() {
    },

    closeOptions() {
    set(this, 'isTriggerClicked', false);
    },

    selectNetAction(param) {
      set(this, 'loading', true);
      ////BUTTON creation
      if (get(this, 'isCreation')) {
        const arr = [];
        if (!isEmpty(param)) {
          if(param[0].name=="Todas las redes" && param.length === 1){
            set(this, 'selectedNet', []);
            get(this, 'selectedNet').pushObject(param);
            set(this, 'newButton.buttonNetId', arr);
            return;
          }
          if (param[0].name=="Todas las redes"){
            param.removeObject(param[0]);
          }
          if (param.length > 1) {
            for (let i = 1; i < param.length; i++) {
              if (param[i].name=="Todas las redes")
                  {
                    set(this, 'selectedNet', []);
                    get(this, 'selectedNet').pushObject(param[i]);
                    set(this, 'newButton.buttonNetId', []);
                    return;
                  }
              if (!isEmpty(param[i]) && !isEmpty(param[i].id)) {
                arr.push(param[i].id);
              }
            }
            set(this, 'selectedNet', param);
            set(this, 'newButton.buttonNetId', arr);
          } else {
            set(this, 'selectedNet', param);
            set(this, 'newButton.buttonNetId', []);
            get(this, 'newButton.buttonNetId').push(param[0].id);
          }
        } else {
          set(this, 'selectedNet', []);
          get(this, 'selectedNet').pushObject(get(this, 'defaultNet'));
          set(this, 'newButton.buttonNetId', []);
        }

  /////////////PROFILE
      } else if (get(this, 'isFriendNet')) {
        if (!isEmpty(param)) {
          if(param[0].name=="Todas las redes" && param.length === 1){
            set(this, 'selectedNet', []);
            get(this, 'selectedNet').pushObject(param);
            set(this, 'selectedNetObj.otherNetIds', []);
            return;
          }

          if (param.length > 1) {
            const arr = [];
            if (param[0].name=="Todas las redes") {
              param.removeObject(param[0]);
            }
            for (let i = 0; i < param.length; i++) {
              if (param[i].name=="Todas las redes")
                  {
                    set(this, 'selectedNet', []);
                    get(this, 'selectedNet').pushObject(param[i]);
                    set(this, 'selectedNetObj.otherNetIds', param[0].id);
                    return;
                  }
              if (!isEmpty(param[i]) && !isEmpty(param[i].id)) {
                arr.push(param[i].id);
              }
            }
            set(this, 'selectedNet', param);
            set(this, 'selectedNetObj.otherNetIds', arr);
          } else {
            set(this, 'selectedNet', param);
            const arr = [];
            arr.push(param[0].id);
            set(this, 'selectedNetObj.otherNetIds', arr);
          }
        } else {
          set(this, 'selectedNet', []);
          get(this, 'selectedNet').pushObject(get(this, 'defaultNet'));
          set(this, 'selectedNetObj.otherNetIds',[]);
        }
        set(this, 'selectedNet', param);
      } else if (get(this, 'isProfileEdition')) {
        set(this, 'interestsSaved', false);
        if (!isEmpty(param)) {
          if((param[0].name=="Todas las redes" && param.length === 1) || param.length === 0){
            set(this, 'selectedNet', []);
            // get(this, 'selectedNet').pushObject(param);
            get(this, 'selectedNet').pushObject(get(this, 'defaultNet'));
            set(this, 'userTag.buttonNets', []);
            return;
          }

          if (param.length > 1) {
            const arr = [];
            if (param[0].name=="Todas las redes") {
              param.removeObject(param[0]);
            }
            for (let i = 0; i < param.length; i++) {
              if (param[i].name=="Todas las redes")
                  {
                    set(this, 'selectedNet', []);
                    get(this, 'selectedNet').pushObject(param[i]);
                    set(this, 'userTag.buttonNets', param[0].id);
                    return;
                  }
              if (!isEmpty(param[i]) && !isEmpty(param[i].id)) {
                arr.push(param[i].id);
              }
            }
            set(this, 'selectedNet', param);
            set(this, 'userTag.buttonNets', arr);
            set(this, 'userTag.hasDirtyAttributes', true);
          } else {
            set(this, 'selectedNet', param);
            // if (get(this, 'userTag.buttonNets').includes(Number(param[0].id))) {
            //   for (var i = 0; i < param.length; i++) {
            //     array[i]
            //   }
            //   get(this, 'userTag.buttonNets').splice(get(this, 'userTag.buttonNets').indexOf(Number(param[0].id)), 1);
            // } else {
            //   if (get(this, 'userTag.buttonNets').includes(param[0].id)) {
            //     get(this, 'userTag.buttonNets').splice(get(this, 'userTag.buttonNets').indexOf(param[0].id), 1);
            //   } else {
            //     get(this, 'userTag.buttonNets').push(param[0].id);
            //   }
            // }
            const arr = [];
            if (param[0].name=="Todas las redes")
                {
                  set(this, 'selectedNet', []);
                }
            if (!isEmpty(param[0]) && !isEmpty(param[0].id)) {
              arr.push(param[0].id);
            }
            set(this, 'userTag.buttonNets', arr);
            set(this, 'userTag.hasDirtyAttributes', true);
          }
        } else {
          set(this, 'selectedNet', []);
          get(this, 'selectedNet').pushObject(get(this, 'defaultNet'));
          set(this, 'userTag.buttonNets',[]);
        }
        // set(this, 'selectedNet', param);
  /////////////INDEX
      } else {
        if (!isEmpty(param)) {
          if(param[0].name=="Todas las redes" && param.length === 1){
            set(this, 'selectedNet', []);
            get(this, 'selectedNet').pushObject(param);
            this.get('router').transitionTo('index.index', { queryParams: this.router._router.currentState.routerJsState.queryParams });
            return;
          }
          set(this, 'selectedNet', param);
          if (param.length > 1) {
            const arr = [];
            if (param[0].name=="Todas las redes"){
              param.removeObject(param[0]);
            }
            for (let i = 1; i < param.length; i++) {
              if (param[i].name=="Todas las redes")
                  {
                    set(this, 'selectedNet', []);
                    get(this, 'selectedNet').pushObject(param[i]);
                    this.get('router').transitionTo('index.index', { queryParams: this.router._router.currentState.routerJsState.queryParams });
                    return;
                  }
              if (!isEmpty(param[i]) && !isEmpty(param[i].id)) {
                arr.push(param[i].name);
              }
            }
            const paramsArr = this.router.currentURL.split("&");
            let lat;
            let lng;
            let nel;
            let nelng;
            let swl;
            let swlng;
            if (param.length === 1) {
              for (var i = 0; i < paramsArr.length; i++) {
                if (paramsArr[i].includes('nel=')) {
                  nel = param[0].latitude + 1
                }
                if (paramsArr[i].includes('nelng=')) {
                  nelng = param[0].longitude - 1
                }
                if (paramsArr[i].includes('swl=')) {
                  swl = param[0].latitude - 1
                }
                if (paramsArr[i].includes('swlng=')) {
                  swlng = param[0].longitude + 1
                }
                if (paramsArr[i].includes('lat=')) {
                  lat = param[0].latitude
                }
                if (paramsArr[i].includes('lng=')) {
                  lng = param[0].longitude
                }
              }
            } else {
              for (var i = 0; i < paramsArr.length; i++) {
                if (paramsArr[i].includes('lat=')) {
                  lat = paramsArr[i].split('=')[1]
                }
                if (paramsArr[i].includes('lng=')) {
                  lng = paramsArr[i].split('=')[1]
                }
                if (paramsArr[i].includes('nel=')) {
                  nel = paramsArr[i].split('=')[1]
                }
                if (paramsArr[i].includes('nelng=')) {
                  nelng = paramsArr[i].split('=')[1]
                }
                if (paramsArr[i].includes('swl=')) {
                  swl = paramsArr[i].split('=')[1]
                }
                if (paramsArr[i].includes('swlng=')) {
                  swlng = paramsArr[i].split('=')[1]
                }
              }
            }

            if (isEmpty(lat)) {
              this.get('router').transitionTo('button-net.index.index', param[0].urlName, { queryParams: { otherNets: arr, nel: nel, nelng: nelng, swl: swl, swlng: swlng } });
            } else {
              this.get('router').transitionTo('button-net.index.index', param[0].urlName, { queryParams: { otherNets: arr, lat: lat, lng: lng } });
            }
          } else {
            const paramsArr = this.router.currentURL.split("&");
            let lat;
            let lng;
            let nel;
            let nelng;
            let swl;
            let swlng;
            for (var i = 0; i < paramsArr.length; i++) {
              if (paramsArr[i].includes('nel=')) {
                nel = param[0].latitude + 1
              }
              if (paramsArr[i].includes('nelng=')) {
                nelng = param[0].longitude - 1
              }
              if (paramsArr[i].includes('swl=')) {
                swl = param[0].latitude - 1
              }
              if (paramsArr[i].includes('swlng=')) {
                swlng = param[0].longitude + 1
              }
              if (paramsArr[i].includes('lat=')) {
                lat = param[0].latitude
              }
              if (paramsArr[i].includes('lng=')) {
                lng = param[0].longitude
              }
            }
            set(this, 'selectedNet', param);
            if (isEmpty(lat)) {
              this.get('router').transitionTo('button-net.index.index', param[0].urlName, { queryParams: { nel: nel, nelng: nelng, swl: swl, swlng: swlng } });
            } else {
              this.get('router').transitionTo('button-net.index.index', param[0].urlName, { queryParams: { lat: lat, lng: lng } });
            }
            // this.get('router').transitionTo('button-net.index.index', param[0].name);
          }
        } else {
          set(this, 'selectedNet', []);
          get(this, 'selectedNet').pushObject(get(this, 'defaultNet'));
          this.get('router').transitionTo('index.index', { queryParams: this.router._router.currentState.routerJsState.queryParams });
        }
      }
    }
  },
});
