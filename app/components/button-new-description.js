import Component from '@ember/component';
import { computed, get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';
import { or, equal, alias } from '@ember/object/computed';
import { resolve } from 'rsvp';
import { htmlSafe } from '@ember/string';
import { notEmpty } from '@ember/object/computed';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  session: service(),

  classNames: ['button-new-description'],

  mediaQueries: service(),
  store: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  screens: service('screen'),
  notifications: service('notification-messages'),
  router: service(),
  isButtonNetRoute: false,
  hideCreateNet:null,

  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));

    if (isEmpty(get(this, 'button.buttonNetId'))) {
      set(this, 'button.buttonNetId', []);
    }

    set(this, 'bottomHeightSeparation', null);
    set(this, 'innerHeight', window.innerHeight);
    window.scrollTop = window.scrollHeight;
  },


  nextStepAction() {},

  isChangeType: equal('button.buttonType', 'change'),
  isNeedType: equal('button.buttonType', 'need'),
  isOfferType: equal('button.buttonType', 'offer'),

  hasImage: computed('button.imageFile', 'button.image', {
    get() {
      if (!isEmpty(get(this, 'button.imageFile'))) {
        return !isEmpty(get(this, 'button.imageFile'))
      } else if (!isEmpty(get(this, 'button.image'))) {
        get(this, 'button.image').then((image) => {
          set(this, 'hasImage', !isEmpty(image));
        })
      }
    },
    set(key, value) {
      return value;
    }
  }),

  buttonNetController: null,
  buttonImage: computed('button.imageFile', 'button.image', {
    get() {
      if (get(this, 'hasImage')) {
        if (!isEmpty(get(this, 'button.imageFile'))) {
          let context = this;
          setTimeout(function () {
            set(context, 'buttonImage', get(context, 'button.imageFile.url'));
          }, 50, context);
        } else if (!isEmpty(get(this, 'button.image'))) {
          get(this, 'button.image').then((image) => {
            set(this, 'buttonImage', get(image, 'url'));
          })
        }
      }
    },
    set(key, value) {
      return value;
    }
  }),

  buttonNets: computed('button.buttonNetId', {
    get() {
      if (!isEmpty(button.buttonNetId))
        {
          button.buttonNetId.forEach((id) => {
            let buttonNet = get(this, 'store').query('buttonNet', { 'filter[id]': id })
            buttonNet.push(buttonNet);
          });
        }
      set(this, 'buttonNets', buttonNet);
    },
    set(key, value) {
      return value;
    }
  }),

  isNextButtonDisabled: computed(
    'selectedOfferTags.[]',
    'selectedNeededTags.[]',
    'button.{description,buttonType}',
    function() {
      return (
        isEmpty(get(this, 'button.description')) || !get(this, 'button.hasValidTags')
      );
    }
  ),

  tags: null,
  showSpinner: false,
  selectedOfferTags: alias('button.offerTags'),
  selectedNeededTags: alias('button.neededTags'),

  didRenderPage: false,

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'selectedOfferTags',
    'selectedNeededTags',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;

      let bodyHeight = document.getElementsByClassName('button-new-description')[0];
      let contentHeight = document.getElementById('typeContent');
      bodyHeight.scrollTop = bodyHeight.scrollHeight;

      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.scrollHeight;
        contentHeight = contentHeight.clientHeight;


        if (bodyHeight - contentHeight > 80) {

          const svgStyles = `height: ${bodyHeight - contentHeight}px;`;
          // return htmlSafe('height: 0px !important; ');
          return htmlSafe(svgStyles);
        }
        const svgStyles = 'height: 100px !important;';
        return htmlSafe(svgStyles);
      }
      return htmlSafe('');
    }
  ),

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    'svgMaxHeight',
    function() {
      this.svgMaxHeight;
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-description')[0];
      const contentHeight = document.getElementsByClassName('button-new-description__content')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight)) {
        return containerHeight.clientHeight > contentHeight.clientHeight ?
          '' : 'button-new-description__content-has-scroll';
      }
      return '';
    }),

  buttonCardClick: false,

  trendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'trendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  bottomHeightSeparation: null,
  innerHeight: null,

  actions: {
    nextStep() {
      const des = get(this, 'button.description');
      get(this, 'nextStepAction')();
    },

    clearImage(file) {
      file=get(this, 'button.imageFile');
      const image=get(this, 'button.image');

      if(file!=null)
      {
        set(this, 'button.imageFile', null);
      }

      if(image!=null) {
        image.then((imageResponse) => {
          imageResponse.destroyRecord();
        });
      }
    },

    searchNeedTags(term, selectClass) {
      term=term.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");
      if (term.includes(' ')) {
        selectClass.actions.close();

        const fixedTerm = term.replace(' ','');

        if (!(!isEmpty(this.tags) && !isEmpty(this.tags.findBy('name', fixedTerm)))) {
          return this.send('createNewTag', 'selectedNeededTags', fixedTerm, selectClass);
        }
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            return get(this, 'selectedNeededTags').pushObject(tags.get('firstObject'));
          });
      }
      return get(this, 'store').query('tag', { 'filter[name]': term })
        .then((tags) => {
          set(this, 'tags', tags);
          return tags;
        });
    },

    searchOfferTags(term, selectClass) {
      term=term.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");

      if (term.includes(' ')) {
        selectClass.actions.close();
        const fixedTerm = term.replace(' ', '');
        if (!(!isEmpty(this.tags) && !isEmpty(this.tags.findBy('name', fixedTerm)))) {
          return this.send('createNewTag', 'selectedOfferTags', fixedTerm, selectClass);
        }
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            return get(this, 'selectedOfferTags').pushObject(tags.get('firstObject'));
          });
      }
      return get(this, 'store').query('tag', { 'filter[name]': term })
        .then((tags) => {
          set(this, 'tags', tags);
          return tags;
        });
    },

    createNewTag(targetRelationship, tagName/* , selectClass*/) {
      tagName=tagName.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");
      if(get(this, 'session.isAuthenticated')) {
        set(this, 'showSpinner', true);
        return get(this, 'store').createRecord('tag', {
          name: tagName,
        })
          .save()
          .then((tag) => {
            set(this, 'showSpinner', false);
            get(this, targetRelationship).pushObject(tag);
            this.notifications.success(
              'Tag creado',
              NOTIFICATION_OPTIONS,
            );
          })
          .catch((error)=>{
            set(this, 'showSpinner', false);
            this.notifications.error(error.errors[0].detail, NOTIFICATION_OPTIONS);
          });
      }
      this.notifications.error(
        'Debes iniciar sesión para poder crear tags',
        NOTIFICATION_OPTIONS,
      );
      return resolve(() => {
        this.notifications.error(
          'Error al crear el tag',
          NOTIFICATION_OPTIONS,
        );
      });
    },

    changeSelectedTags(targetRelationship, tags) {
      if (tags.length > 5) {
        this.notifications.error(
          'Limite de tags alcanzado',
          NOTIFICATION_OPTIONS,
        );
      } else {
        if (isEmpty(get(this, 'button.toggledTags'))) {
          set(this, 'button.toggledTags', false);
        }
        this.toggleProperty('button.toggledTags');
        set(this, targetRelationship, tags);
      }
    },
    checkDescriptionLength(param) {
      this.notifications.error(
        'Limite de descripción alcanzado',
        NOTIFICATION_OPTIONS,
      );
      set(this, 'button.description', param.substring(0, 250));
    },
    removeTag(targetRelationship, tag) {
      if (isEmpty(get(this, 'button.toggledTags'))) {
        set(this, 'button.toggledTags', false);
      }
      this.toggleProperty('button.toggledTags');
      const target = get(this, targetRelationship);
      target.removeObject(tag);
    },

    tagSuggestion(term) {
      return `Pulsa intro o aquí para crear ${term}`;
    },


    uploadImage(file) {
      set(this, 'button.imageFile', file);
      file.readAsDataURL().then(function(url) {
        file.set('url', url);
      });
    },


    checkTagAlreadyExists(term) {
      const tags = this.tags;
      return !(!isEmpty(tags) && !isEmpty(tags.findBy('name', term)));
    },
    swapButton() {
      if (isEmpty(this.button.swap)) {
        set(this.button, 'swap', true);
      }
      this.toggleProperty('button.swap');
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
    closeModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
    openSelect(param) {
      const inner = get(this, 'innerHeight');
      setTimeout(function() {
        const bodyHeight = document.getElementsByClassName('button-new-description__select')[0];
        if (!isEmpty(bodyHeight)) {
          const scrollTop = $(window).scrollTop();
          let elementOffset = 0;
          if (param === 1) {
            elementOffset = $(document.getElementsByClassName('ember-power-select-trigger')[0]).offset().top;
            const elementOffset2 = $(document.getElementsByClassName('ember-power-select-trigger')[1]).offset().top;
            elementOffset = elementOffset2 - elementOffset;
          }
          const distance = document.getElementById('typeContent').clientHeight;
          const distanceFull = document.getElementsByClassName('buttons-new')[0].clientHeight;
          const distanceTop = document.getElementsByClassName('button-new-description__body-svg-container')[0].clientHeight;
          const distanceText = document.getElementsByClassName('button-new-description__body-content--text-class')[0].clientHeight
          const totalHeight = distanceTop + distanceText + elementOffset;
          const style = document.createElement('style');
          style.type = 'text/css';
          // if (isEmpty(get(this, 'bottomHeightSeparation')) || window.innerHeight !== inner) {
            set(this, 'bottomHeightSeparation', (distance - distanceText - (distanceFull - inner)));
          // }
          style.innerHTML = '.auto-power-select-position { max-height: ' +
          totalHeight + 'px !important; top: auto !important; bottom: ' + (get(this, 'bottomHeightSeparation') - elementOffset) + 'px !important; display: block; }';
          document.getElementsByTagName('head')[0].appendChild(style);

          const styleReplace = document.createElement('style');
          styleReplace.type = 'text/css';
          styleReplace.innerHTML = '.ember-power-select-options { max-height: ' +
          totalHeight + 'px !important; top: auto !important; bottom: ' + (get(this, 'bottomHeightSeparation') - elementOffset) + 'px }';
          document.getElementsByTagName('head')[0].appendChild(styleReplace);
          $(bodyHeight.firstElementChild).scrollTop(bodyHeight.firstElementChild.scrollHeight);
        }
      }, 3);
    },
  },
});
