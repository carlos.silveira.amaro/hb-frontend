import Component from '@ember/component';
import { set, get, computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { or, readOnly } from '@ember/object/computed';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  session: service(),
  notifications: service('notification-messages'),
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  routing: service('-routing'),


  buttonIsMine: computed('button.isMine', function() {
    return get(this, 'button.isMine');
  }),

  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  showNormalChat: true,


  button: null,
  closeAction: null,

  unreadMessagesbool: computed(
    'button',
    'button.chats',
    'buttonIsMine',
    {
      get() {
        if (get(this, 'button.isMine')) {
          get(this, 'button.chats').then((chats) => {
            chats.forEach((chat) => {
              if(chat.unreadMessages>0)
              {
              return  true;
              }
            });

          });
        }
      },
    }
  ),


  chatsComp: computed(
    'button',
    'button.chats',
    'buttonIsMine',
    {
      get() {
        if (get(this, 'button.isMine')) {
          get(this, 'button.chats').then((chats) => {
            set(this, 'chatsComp', chats);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  routeType: null,
  showProfile: null,
  showModalButton: false,
  currentUser: readOnly('session.currentUser'),

  bigButtonAction: computed('button', 'button.isMine', function() {
    return get(this, 'button.isMine') ? 'toggleButtonState' : 'openConversation';
  }),



  showConversations: false,

  svgIcon: computed('button.{isMine,active}', function() {
    if (get(this, 'button.isMine')) {
      return 'switch_black';
    }
    return 'chat';
  }),



  buttonClass: computed('button.{isMine,active}', function() {
    const button = this.button;

    return button.isMine && !button.active ?
      'button-file__yellow-circle--inactive-button' : '';
  }),


  creatorName: computed(
    'button',
    {
      get() {
        const button = get(this, 'button');
        get(button, 'creator')
          .then((creator) => {
            set(this, 'creatorName', creator.nickname);
          });
      },
      set(key, value) {
        return value;
      },
    }
  ),

  allowEdit: computed('routing.currentRouteName', function() {
    return this.routing.currentRouteName === 'profile.index';
  }),

  openConversation: null,
  removeButtonTransition: null,
  showButtonFile: true,
  buttonCardClick: false,
  //show  whatasapp telegram linkSegment
  showConversationLinks: null,
  conversationURL:null,
  conversationTransition:null,
  conversationNetwork:null,

  socialShareTransition: null,
  socialShareURL: null,

  useTelegram: computed( 'button.creator.useTelegram', function(){
    return  get(this, 'button.creator.useTelegram');
  }),
  useWhatsapp: computed( 'button.creator.useWhatsapp', function(){
    return  get(this, 'button.creator.useWhatsapp');
  }),

  accessDenied: null,

  events: {
        goToLink: function(item, anchor) {
            var $elem = $(anchor);
            var $scrollTo = $('body').scrollTop($elem.offset().top);
            this.transitionToRoute(item.route).then($scrollTo);  //.transitionTo is depricated
        }
    },

  actions: {
    markerClick() {
      const button = get(this, 'button');
      set(button, 'showConversationLinks', false);
      this.closeAction();
    },

    conversationOptions(socialNetwork) {
      set(this, 'conversationNetwork', socialNetwork);
      const buttonT = get(this, 'button.creator.userTelegram');
      const buttonW = get(this, 'button.creator.phone');
      const button = get(this, 'button');
      switch (this.conversationNetwork) {
        case 'telegram':
          window.open('https://t.me/'+buttonT.toString(),'_blank');
          break;
        case 'whatsapp':
          window.open('https://wa.me/'+buttonW,'_blank');
          break;
          case 'helpbuttons':
          this.openConversation();
            break;
        default:
        break;
      }
    },

    scrollToBottom(){
      $('.button-file__container--scrollable').animate({ scrollTop: $('#buttonChats').offset().top }, 1000);
    },

    openConversation() {
      const use = get(this, 'button.creator.useExternalConv');
      const button = get(this, 'button');
      if(use){
        set(button, 'showConversationLinks', true);
      }
      else{
      this.openConversation();
      }
    },


    toggleButtonState() {
      const button = get(this, 'button');
      this.toggleProperty('button.active');
      return button.save().then((btn) => {
        if (btn.active) {
          this.notifications.success(
            'Has activado el botón',
            NOTIFICATION_OPTIONS,
          );
        } else {
          this.notifications.info(
            'Has desactivado el botón, ya no será visible para otros usuarios.',
            NOTIFICATION_OPTIONS,
          );
        }
      });
    },


    toggleProperty(param) {
      this.toggleProperty(param);
    },
    removeButtonTransition() {
      this.removeButtonTransition();
    },
    closeButtonFile() {
      const button = get(this, 'button');
      if (!this.buttonCardClick) {
        set(button, 'showConversationLinks', false);
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
  },
});
