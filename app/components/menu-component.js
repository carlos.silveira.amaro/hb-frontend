import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils'


export default Component.extend({
  session: service('session'),
  routing: service('-routing'),
  isAuthenticated: readOnly('session.isAuthenticated'),
  searchTrendingTags: null,
  indexController:null,

  isShowingMenuModal: null,
  isSmallDevice: null,
  hideWelcome: null,
  isShowingMoreOptions: false,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  newButtonRoute: computed('routing.currentRouteName', function() {
    if (get(this, 'isButtonNetRoute')) {
      if (!this.isSmallDevice) {
        return 'button-net.index.index.new-button';
      }
      return 'button-net.buttons.new';
    }
    if (!this.isSmallDevice) {
      return 'index.index.new-button';
    }
    return 'buttons.new';
  }),

  showSettings: false,

  currentUser: readOnly('session.currentUser'),

  currentUserAvatar: readOnly('currentUser.avatar'),

  isShowingMoreOptions: false,


  actions: {
    toggleProperty(param, hideWelcome) {
      this.toggleProperty(param);
      if (hideWelcome) {
        // set(this, 'hideWelcome', false);
      }
    },

    closeSession() {
      this.session.invalidate();
    },
    removeProfile() {
      window.alert('Not finished');
    },

    toggleShowMoreOptions() {
      this.toggleProperty('isShowingMoreOptions');
    },

    rrssAuthenticate(providerName) {
      const context = this;
      this.session.authenticate('authenticator:torii', providerName).then(function() {
        if (context.routing.currentRouteName === 'index.index') {
          context.loginTransition();
        }
      }, context);
    },
  },
});
