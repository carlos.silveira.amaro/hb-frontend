import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { htmlSafe } from '@ember/string';
import { computed, set, get, setProperties } from '@ember/object';
import { alias, notEmpty } from '@ember/object/computed';
import { resolve, hash } from 'rsvp';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 20000,
  cssClasses: 'notifications-index',
};
const NOTIFICATION_OPTIONS2 = {
  autoClear: true,
  clearDuration: 10000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  classNames: ['container-fluid', 'create-net-component'],
  classNameBindings: ['createNetComponentBindClass'],
  notifications: service('notification-messages'),
  routing: service('-routing'),
  geolocation: service('geolocation'),
  session: service('session'),
  screens: service('screen'),
  router: service(),
  store: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
    const buttonNet = get(this, 'store').createRecord('buttonNet');
    set(this, 'buttonNet', buttonNet);
    set(this, 'buttonNet.active', true);
    set(this, 'buttonNetHeader', buttonNet);
  },

  currentUser: alias('model.currentUserLet'),

  buttonNet: null,
  isPrivateNet: null,
  isExternalNet: null,

  isPublicNet: computed('buttonNet.privacy', function() {
    const b = get(this, 'isPrivateNet');
    return !b;
  }),


  isModal: null,

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },


  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('create-net-component__modal-content')[0];
      let contentHeight = document.getElementById('typeContent');
      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        let svgStyles;
        if (this.isModal && this.screens.width < 992) {
          svgStyles = bodyHeight - contentHeight + 54;
        } else if (this.isModal && this.screens.width > 992) {
          svgStyles = bodyHeight - contentHeight - 32;
        } else {
          svgStyles = this.screens.height - contentHeight - 16 - 75;
        }
        if (svgStyles < 100) {
          svgStyles = 100;
        } else if (svgStyles > 350) {
          svgStyles = 300;
        }
        svgStyles = `height: ${svgStyles}px;`;
        return htmlSafe(svgStyles);
      }
      return htmlSafe(`height: ${100}px;`);
    }
  ),

  isModal: null,
  isShowingModal: false,

  isMobileButtonCreation: null,

  hasBounds: notEmpty('mapBounds'),

  searchAddressBounds: null,



  deleteModal: false,
  showMap: false,
  draggableMarkerLocation: null,

  showIconSelector: false,
  icons: ['1_terrestre_one', '2_tren_Artboard','3_maritim', '4_air', '5_non_motor', '6_tics_electronic_tech_&software', '7_games_table_all','8_home_and_garden_help', '9_objetos','10_cook2', '11_plastic_art', '12_compras', '13_crafts', '14_peluc_estectic','15_party', '16_meetings','17_drink3', '18_teach_ADVICE', '19_investigacion', '20_religion5', '21_general','22_chat78', '23_elder8','24_emergency', '25_addiction', '37_police_jail', '26_politics', '27_sustain_recicle', '28_cooperacion', '29_human_rights','30_enfado_denuncia', '31_nature90', '32_woman', '33_man', '34_minorias_inmigrac','35_paral', '36_shelter', '38_animal34', '39_plants','40_love3', '41_sanidad_farmacia','42_Babe_child', '43_construccion_bricolage_reformas', '44_agric_industria', '45_cleaning', '47_energy','46_peligro_danger','48_guide2', '50_storage', '37_police_jail', '38_animal34', '51_traffic2','52_depresion', '53_problemas_economicos','54_defunciones', '55_otras_causas', '56_security', '57_lies_fraud', '39_plants', ],


  createNetComponentBindClass: computed(
    'isModal',
    'isMobileButtonCreation',
    'screens.height',
    'svgMaxHeight',
    function() {
      if (this.isModal) {
        return 'create-net-component__modal';
      }
      if (this.isMobileButtonCreation) {
        return 'create-net-component--taller';
      }
      const imageHeight = document.getElementsByClassName('create-net-component__image')[0];
      if (!isEmpty(imageHeight) && imageHeight.clientHeight > 100) {
        return 'create-net-component__main-page create-net-component__main-page--high';
      }
      return 'create-net-component__main-page';
    }
  ),

  mapBounds: computed(
    'geolocation',
    'searchAddressBounds',
    {
      get() {
        if (isEmpty(this.searchAddressBounds)) {
          // if (this.showSecondMarker && !isEmpty(this.button.toLatitude) && !isEmpty(this.button.toLongitude)) {
          //   return L.latLng(this.button.toLatitude, this.button.toLongitude).toBounds(500);
          // }
          // if (!isEmpty(this.button.latitude) && !isEmpty(this.button.longitude)) {
          //   return L.latLng(this.button.latitude, this.button.longitude).toBounds(500);
          // } else if (!isEmpty(this.button.location.latitude) && !isEmpty(this.button.location.longitude)) {
          //   return L.latLng(this.button.location.latitude, this.button.location.longitude).toBounds(500);
          // }
          get(this, 'geolocation')
            .getCurrentPosition()
            .then((coordinates) => {
              set(this, 'mapBounds', L.latLng(coordinates.latitude, coordinates.longitude).toBounds(500));
              set(this, 'draggableMarkerLocation', L.latLng(coordinates.latitude, coordinates.longitude));
            });
        } else {
          return this.searchAddressBounds;
        }
      },
      set(key, value) {
        return value;
      },
  }),

  newUser: null,

  modalClasses: computed('isModal', function() {
    return this.isModal ? 'col-10' : 'col-12 col-sm-10 col-md-7 col-lg-9';
  }),

  routeName: null,

  isShowingCreateNetModal: null,

  isShowingMoreOptions: false,

  loginTransition: null,

  closeRouteName: computed('routing.currentRouteName', function() {
    return get(this, 'routing.currentRouteName').includes('index.list') ? 'index.list' : 'index.index';
  }),


  actions: {
    searchByAddress() {
      set(this, 'isSearching', true);
      const address = this.inputAddress;
      return this.searchAction(address, get(this, 'selectedFilterTags'))
        .then(() => {
          setProperties(this, {
            'isSearching': false,
            'inputAddress': null,
          });
        });
    },

    setAdressToButtonNet() {

    },

    createNewTag(targetRelationship, tagName) {
      if (tagName.includes('##')) {
        tagName = tagName.replace('#', '');

      }
      if (tagName.includes('#')) {
        tagName = tagName.replace('#', '');
      }
      tagName=tagName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      tagName=tagName.toLowerCase();

      return get(this, 'store').createRecord('tag', {
        name: tagName,
      })
        .save()
        .then((tag) => {
          get(this, targetRelationship).pushObject(tag);
        });
    },

    submitNewNet() {
      this.send('setNetUrl');
      get(this, 'buttonNet').save()
      .then((buttonNet) => {
        if (buttonNet.privacy) {
          this.notifications.success(
            'Formulario de red privada enviado con éxito, una vez revisado te llegará un mail confirmando la creación de la red. Puedes editar tu red en tu perfil.',
            NOTIFICATION_OPTIONS,
          );
          this.toggleProperty('isShowingCreateNetModal')
        }
        else if (!buttonNet.privacy) {
          this.notifications.success(
            'Formulario de red pública enviado con éxito, tu red se confirmará en unos minutos. Puedes editar tu red en tu Perfil->Editar perfil->Moderación y Redes.',
            NOTIFICATION_OPTIONS,
          );
          this.toggleProperty('isShowingCreateNetModal')
        }
         else {
          this.notifications.info(
            'No ha podido enviarse tu red.',
            NOTIFICATION_OPTIONS,
          );
        }
      })
      .catch(() => {
        this.notifications.error(
          'Error al crear red, el nombre ya existe o faltan campos por completar.',
          NOTIFICATION_OPTIONS,
        );
      });
    },

    setNetUrl () {
      set(this, 'buttonNet.netUrl', "https://www.helpbuttons.org/"+ get(this,'buttonNet.urlName'));
    },

    onMapLoad() {
      const center = this.mapBounds.getCenter();
      setProperties(this, {
        'currentUser.latitude': center.lat,
        'currentUser.longitude': center.lng,
        'draggableMarkerLocation': L.latLng(center.lat, center.lng),
      });
    },
    onDragEnd(marker) {
      setProperties(this, {
        'currentUser.latitude': marker.target._latlng.lat,
        'currentUser.longitude': marker.target._latlng.lng,
        'draggableMarkerLocation': L.latLng(marker.target._latlng.lat, marker.target._latlng.lng),
      });
    },
    placeChangedCallback(params) {
      if (!isEmpty(params.geometry)) {
        const viewport = params.geometry.viewport;
        const northEastCorner = L.latLng(viewport.getNorthEast().lat(), viewport.getNorthEast().lng());
        const southWestCorner = L.latLng(viewport.getSouthWest().lat(), viewport.getSouthWest().lng());
        setProperties(this, {
          'searchAddressBounds': L.latLngBounds(northEastCorner, southWestCorner),
          'searchZoom': true,
          'isSearchingAddress': true,
          'currentUser.tagsLatitude': params.geometry.location.lat(),
          'currentUser.tagsLongitude': params.geometry.location.lng(),
          'draggableMarkerLocation': L.latLng(params.geometry.location.lat(), params.geometry.location.lng()),
        });
      }
    },
    acceptLocation(address) {
      set(this, 'buttonNet.latitude', address.geometry.location.lat());
      set(this, 'buttonNet.longitude', address.geometry.location.lng());
      set(this, 'buttonNet.locationName', address.formatted_address);
      set(this, 'showMap', false);
    },

    showMap() {
      this.toggleProperty('showMap');
    },

    checkTagAlreadyExists(term) {
      const tags = this.tags;
      return !(!isEmpty(tags) && !isEmpty(tags.findBy('name', term)));
    },
    tagSuggestion(term) {
      return `Pulsa intro o aquí para crear ${term}`;
    },
    removeTag(targetRelationship, tag) {
      const target = get(this, targetRelationship);
      target.removeObject(tag);
    },

    togglePrivateNet(isPrivateNet) {
      this.toggleProperty(isPrivateNet);
      if (this.isPrivateNet) {
        set(this, 'buttonNet.privacy', true);
        setTimeout(function () {
          paypal.Buttons({
              style: {
                  shape: 'rect',
                  color: 'gold',
                  layout: 'vertical',
                  label: 'subscribe'
              },
              createSubscription: function(data, actions) {
                return actions.subscription.create({
                  'plan_id': 'P-13296696KM483111NL6UYOII'
                });
              },
              onApprove: function(data, actions) {
                alert(data.subscriptionID);
              }
          }).render('#paypal-button-container');
        }, 50);
      } else {
        set(this, 'buttonNet.privacy', false);
      }
    },

    toggleExternalNet(isExternalNet) {
      this.toggleProperty(isExternalNet);
    },

    // searchTags(term) {
    //   return this.store.query('tag', { 'filter[name]': term })
    //     .then((tags) => {
    //       return tags.filter((tag) => !this.selectedInterests.includes(tag));
    //     });
    // },

    searchTags(term, selectClass) {
      if (term.includes(' ')) {
        selectClass.actions.close();
        const fixedTerm = term.replace(' ', '');
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            return get(this, 'selectedTags').pushObject(tags.get('firstObject'));
          });
      }
      return get(this, 'store').query('tag', { 'filter[name]': term })
        .then((tags) => {
          set(this, 'tags', tags);
          return tags;
        });
    },

    toggleModal() {
      this.toggleProperty('isShowingCreateNetModal')
      get(this, 'buttonNet').destroyRecord();
    },

    toggleShowMoreOptions() {
      this.toggleProperty('isShowingMoreOptions');
    },

    toggleShowIcons() {
      this.toggleProperty('showIconSelector');
    },

    rrssAuthenticate(providerName) {
      const context = this;
      this.session.authenticate('authenticator:torii', providerName).then(function() {
        if (context.routing.currentRouteName === 'index.index') {
          context.loginTransition();
        }
      }, context);
    },

    addIcon(param) {
      set(this, 'buttonNet.imgUrl', param);
    },

    nameKeyUp(param) {
      set(this, 'buttonNet.urlName', param.replace(/ /g,"_"));
    }

  },
});
