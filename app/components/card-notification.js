import Component from '@ember/component';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  classNames: ['col-12', 'card-notification', 'button-file__messages'],
  chat: null,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  userAvatar: computed(
    'user',
    {
      get() {
        const user = get(this, 'user');
        if (!isEmpty(user)) {
          get(user, 'avatar').then((avatar) => {
            set(this, 'userAvatar', avatar);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  user: computed(
    'lastMessage',
    {
      get() {
        const lastMessage = get(this, 'lastMessage');
        if (!isEmpty(lastMessage)) {
          get(lastMessage, 'user').then((user) => {
            set(this, 'user', user);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  lastMessage: computed(
    'chat',
    {
      get() {
        const chat = get(this, 'chat');
        if (!isEmpty(chat)) {
          get(chat, 'messages').then((messages) => {
            set(this, 'lastMessage', get(messages, 'lastObject'));
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  daysSinceLastMesssage: computed('lastMessage', function() {
    const lastMessage = get(this, 'lastMessage');
    if (!isEmpty(lastMessage)) {
      const timeDiff = Date.now() - Date.parse(lastMessage.createdAt);
      const daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
      if (daysDiff === 0) {
        return 'hoy';
      }
      return (daysDiff === 1) ?  'hace ' + daysDiff + ' día' : 'hace ' + daysDiff + ' días';
    }
    return '';
  }),

  button: computed(
    'chat',
    {
      get() {
        const chat = get(this, 'chat');
        get(chat, 'button').then((button) => {
          set(this, 'button', button);
        });
      },
      set(key, value) {
        return value;
      },
    }
  ),

});
